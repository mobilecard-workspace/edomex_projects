package org.addcel.session;

import java.util.HashMap;

import org.addcel.prosa.view.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	SharedPreferences pref;
	Editor editor;
	Context _context;
	
	int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "Data";
	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String USR_ID = "usr_id";
	public static final String USR_LOGIN = "usr_login";
	public static final String USR_PWD = "usr_pwd";
	
	public static final String ID_SUPERVISOR = "idSupervisor";
	public static final String ID_TIENDA = "id_tienda";
	public static final String DESC_TIENDA = "desc_tienda";
	public static final String ID_PROVEEDOR = "id_proveedor";
	public static final String COMISION = "comision";
	public static final String COMISION_PCT = "comision_pct";
	public static final String MIN_COM_PCT = "min_com_pct";
	public static final String MIN_COM = "min_com";
	public static final String TIPO_CAMBIO = "tipo_cambio";
	public static final String ROL = "rol";
	public static final String NOMBRES = "nombres";
	
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	public void createLoginSession(String id, String login, String pwd, String idTienda, String descTienda, String idProveedor, 
			String comision, String comisionPct, String minComPct, String minCom, String tipoCambio, String rol, String idSupervisor, String nombres) {
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(USR_ID, id);
		editor.putString(USR_LOGIN, login);
		editor.putString(USR_PWD, pwd);
		editor.putString(ID_TIENDA, idTienda);
		editor.putString(DESC_TIENDA, descTienda);
		editor.putString(ID_PROVEEDOR, idProveedor);
		editor.putString(COMISION, comision);
		editor.putString(COMISION_PCT, comisionPct);
		editor.putString(MIN_COM_PCT, minComPct);
		editor.putString(MIN_COM, minCom);
		editor.putString(TIPO_CAMBIO, tipoCambio);
		editor.putString(ROL, rol);
		editor.putString(ID_SUPERVISOR, idSupervisor);
		editor.putString(NOMBRES, nombres);
		editor.commit();
	}
	
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		
		user.put(USR_ID, pref.getString(USR_ID, null));
		user.put(USR_LOGIN, pref.getString(USR_LOGIN, null));
		user.put(USR_PWD, pref.getString(USR_PWD, null));
		user.put(ID_TIENDA, pref.getString(ID_TIENDA, null));
		user.put(DESC_TIENDA, pref.getString(DESC_TIENDA, null));
		user.put(ID_PROVEEDOR, pref.getString(ID_PROVEEDOR, null));
		user.put(COMISION, pref.getString(COMISION, null));
		user.put(COMISION_PCT, pref.getString(COMISION_PCT, null));
		user.put(MIN_COM_PCT, pref.getString(MIN_COM_PCT, null));
		user.put(MIN_COM, pref.getString(MIN_COM, null));
		user.put(TIPO_CAMBIO, pref.getString(TIPO_CAMBIO, null));
		user.put(ROL, pref.getString(ROL, null));
		user.put(ID_SUPERVISOR, pref.getString(ID_SUPERVISOR, null));
		user.put(NOMBRES, pref.getString(NOMBRES, null));
		
		return user;
	}
	
	public void checkLogin() {
		if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             
            // Staring Login Activity
            _context.startActivity(i);
		}
	}
	
	public void logoutUser() {
        editor.clear();
        editor.commit();                 
        // Staring Login Activity
    }
	
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
	
}
