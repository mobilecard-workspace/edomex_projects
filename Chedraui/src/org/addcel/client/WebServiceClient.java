package org.addcel.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.addcel.interfaces.WSResponseListener;
import org.addcel.util.Dialogos;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.NameValuePair;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class WebServiceClient extends AsyncTask<String, Void, String> {

	private InputStream is;
	private String response;
	private Context con;
	private boolean hasLoader;
	private WSResponseListener listener;
	private String url;
	private static final String LOG = "WEB_SERVICE_CLIENT";
	
	public WebServiceClient(WSResponseListener listener, Context con, boolean hasLoader, String url) {
		this.listener = listener;
		this.con = con;
		this.hasLoader = hasLoader;
		this.url = url;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		
		if (hasLoader) {
			Dialogos.makeDialog(con, "Cargando...", "Espere por favor...");
		}
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
	        nameValuePairs.add(new BasicNameValuePair("json", params[0]));
	        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
	        Log.i(LOG, EntityUtils.toString(httpPost.getEntity()));
			
	        HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch(UnsupportedEncodingException e) {
			Log.e(LOG, "UnsupportedEncodingException - " + e.getLocalizedMessage());
			return "";
		} catch (ClientProtocolException e) {
			Log.e(LOG, "ClientProtocolExceptione - " + e.getLocalizedMessage());
			return "";
		} catch (IOException e) {
			Log.e(LOG, "IOException - " + e.getLocalizedMessage());
			return "";
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			response = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result" + e.toString());
			return "";
		}
		
		return response;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		
		if (hasLoader) {
			Dialogos.closeDialog();
		}
		
		listener.StringResponse(result);
	}

}
