package org.addcel.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

public class BluetoothHelper {
	
	private BluetoothAdapter adapter;
	private Activity act;
	
	public final static int REQUEST_ENABLE_BT = 1;
	
	public BluetoothHelper(Activity act) {
		adapter = BluetoothAdapter.getDefaultAdapter();
		this.act = act;
	}
	
	public BluetoothAdapter getAdapter() {
		if (adapter == null) {
			adapter = BluetoothAdapter.getDefaultAdapter();
		}
		
		return adapter;
	}
	
	public boolean hasAdapter() {
		if (null == adapter) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isEnabled() {
		if (adapter.isEnabled()) {
			return true;
		} else {
			return false;
		}
	}
	
	public Set<BluetoothDevice> getPairedDevices() {
		return adapter.getBondedDevices();
	}
	
	public List<BluetoothDevice> getPairedDevicesAsList() {
		return new ArrayList<BluetoothDevice>(getPairedDevices());
	}
	
	public void init() {
		ConnectThread c = new ConnectThread(getPairedDevicesAsList().get(0));
		c.run();
	}
	
	private class ConnectThread extends Thread {

		private BluetoothSocket mmSocket;
		private BluetoothDevice mmDevice;
		
		public ConnectThread(BluetoothDevice device) {
			// TODO Auto-generated constructor stub
			BluetoothSocket tmp = null;
			mmDevice = device;
			
			try {
				tmp = device.createRfcommSocketToServiceRecord(UUID.randomUUID());
			} catch (IOException e) {
				mmSocket = tmp;
			}
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			getAdapter().cancelDiscovery();
			
			try {
				mmSocket.connect();
			} catch (IOException connectException) {
				// TODO Auto-generated catch block
				try {
					mmSocket.close();
				} catch (IOException closeException) { }
				
				return;
			}
			
			ConnectedThread conectado = new ConnectedThread(mmSocket);
			conectado.write(null);
			conectado.run();
			
		}
		
		public void cancel() {
			
			try {
				mmSocket.close();
			} catch (IOException e) { }
		}
	}
	
	private class ConnectedThread extends Thread {

	    private final BluetoothSocket mmSocket;
	    private final InputStream mmInStream;
	    private final OutputStream mmOutStream;
	    
	    public ConnectedThread(BluetoothSocket socket) {
			// TODO Auto-generated constructor stub
	    	mmSocket = socket;
	    	InputStream tmpIn = null;
	    	OutputStream tmpOut = null;
	    	
	    	try {
				tmpIn = socket.getInputStream();
		    	tmpOut = socket.getOutputStream();
			} catch (IOException e) { }
	    	
	    	mmInStream = tmpIn;
	    	mmOutStream = tmpOut;
	    }
	    
	    @Override
	    public void run() {
	    	// TODO Auto-generated method stub
	    	byte[] buffer = new byte[1024];
	    	int bytes;
	    	
	    	while (true) {
				try {
					bytes = mmInStream.read(buffer);
				} catch (IOException e) {
					break;
				}
			}
	    }
	    
	    public void write(byte[] bytes) {
	    	try {
	    		mmOutStream.write(bytes);
	    	} catch (IOException e) { }
	    }
	    
	    public void cancel() {
	    	try {
	    		mmSocket.close();
	    	} catch (IOException e) { }
	    }
	}
	
}
