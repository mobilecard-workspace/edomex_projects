/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.addcel.miuraapp.vo.TLVItem;

/**
 *
 * @author Carlos Corona
 */
public class TagComparitor {

    private HashMap<List<Tag>, Integer> tagPaths = new HashMap<List<Tag>, Integer>();

    public TagComparitor(Object... args) {
        List<Tag> tp = new ArrayList<Tag>();
        if (args.length == 1 && args[0] instanceof TLV) {
            buildFromTLV((TLV) args[0], new ArrayList());
        } else {
            for (Object tagPath : args) {
                tp=new ArrayList<Tag>();
                for (int i = 0; i < ((int[]) tagPath).length; i++) {
                    tp.add(new Tag(((int[]) tagPath)[i]));
                }
                System.out.println("Los tags de TagComparitor : "+tp);
                if (tagPaths.containsKey(tp)) {
                    tagPaths.put(tp, new Integer(((Integer) tagPaths.get(tp)) + 1));
                } else {
                    tagPaths.put(tp, new Integer(1));
                }
            }
            for (Map.Entry<List<Tag>, Integer> entry : tagPaths.entrySet()) {
                List<Tag> tag = entry.getKey();
                Integer valor = entry.getValue();
                System.out.println(tag + " : " + valor);
            }

        }
    }

    public void buildFromTLV(TLV tlv, List<Tag> path) {
        for (TLVItem tag : tlv.getObjects()) {
            if (tag.getTag().constructed() && tag.getValue() instanceof TLV) {
                path.add(tag.getTag());
                buildFromTLV((TLV) tag.getValue(), path);
            } else if (!tag.getTag().constructed() && tag.getValue() instanceof String) {
                path.add(tag.getTag());
                if (tagPaths.containsKey(path)) {
                    tagPaths.put(path, new Integer(((Integer) tagPaths.get(path)) + 1));
                } else {
                    tagPaths.put(path, new Integer(1));
                }

            }
        }
    }

    public boolean empty() {
        return tagPaths.isEmpty();
    }

    public boolean match(TLV tlv) {
        boolean result;
        /*
         Returns True if TLV object 'tlv' contains at least one of each tag
         matching the tag sets in 'x'.  Otherwise returns False.  Note: As a
         special case, if 'x' is empty, this will only match if 'tlv' is also
         empty.
         */
        result = true;
        if (tlv instanceof TLV) {
            result = result && (this.empty() == tlv.empty());
            for (Map.Entry<List<Tag>, Integer> entry : tagPaths.entrySet()) {
                List<Tag> tag = entry.getKey();
                Integer valor = entry.getValue();
                System.out.println("en match : "+tag);
                result = result && (tlv.search(tag).size() > 0);
            }
        } else {
            System.out.println("tlv parameter is not a TLV object.");
        }
        return result;
    }
/*
    public Object[] String2TagPath(String s) {
        String tempS1[] = s.split("-");
        String tempS2[];
        List<Integer> datos=new ArrayList<Integer>();
        List<Tag> tags = new ArrayList<Tag>();
        System.out.println("String2TagPath : "+s);
        for (int i = 0; i < tempS1.length; i++) {
            tempS2 = tempS1[i].split(" ");
            datos=new ArrayList<Integer>();
            for (int j = 0; j < tempS2.length; j++) {
                datos.add(Integer.parseInt(tempS2[j], 16));
            }
            tags.add(new Tag(datos));
        }
        return tags.toArray();
    }

    public String TagPath2String(List<Tag> tp) {
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i < tp.size(); i++) {
            tmp.append(tp.get(i).toString()).append("-");
        }
        System.out.println("TagPath2String : "+tmp);
        return tmp.toString();
    }*/
    
}
