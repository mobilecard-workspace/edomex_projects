/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp.vo;

import org.addcel.miuraapp.Tag;

/**
 *
 * @author Carlos Corona
 */
public class TagItem {
    private Tag tag;
    private String descripcion;
    
    public TagItem(){
    }
    public TagItem(Tag tag, String descripcion){
        this.tag=tag;
        this.descripcion=descripcion;
    }
    /**
     * @return the tag
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(Tag tag) {
        this.tag = tag;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
