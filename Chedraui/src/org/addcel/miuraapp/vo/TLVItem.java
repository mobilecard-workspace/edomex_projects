/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp.vo;

import org.addcel.miuraapp.Tag;

/**
 *
 * @author Carlos Corona
 */
public class TLVItem {
    private Tag tag;
    private Object value;
    public TLVItem(){
        
    }
    public TLVItem(Tag tag, Object value){
        this.tag=tag;
        this.value=value;
    }

    /**
     * @return the tag
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(Tag tag) {
        this.tag = tag;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }
}
