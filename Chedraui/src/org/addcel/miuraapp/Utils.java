/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;

/**
 *
 * @author Carlos Corona
 */
public class Utils {

    public static Integer unicodeEscaped(char ch) {
        if (ch < 0x10) {
            return Integer.parseInt("000" + Integer.toHexString(ch), 16);
        } else if (ch < 0x100) {
            return Integer.parseInt("00" + Integer.toHexString(ch), 16);
        } else if (ch < 0x1000) {
            return Integer.parseInt("0" + Integer.toHexString(ch), 16);
        }
        return ch&0xFF;
    }

    public ArrayList tuple(Object o) {
        return null;
    }

    public static String concat(char... chars) {
        if (chars.length == 0) {
            return "";
        }
        StringBuilder s = new StringBuilder(chars.length);
        for (char c : chars) {
            s.append(c);
        }
        return s.toString();
    }
}
