/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Carlos Corona
 */
public class Response {

    private boolean valid;
    private String timestamp;
    private int pcb;
    private Comando command;
    private String data;
    private int sw1sw2;
    private TLV tlv;
    private List<Integer> lista1 = Arrays.asList(0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6);

    public Response(Comando command, String data) {
        SimpleDateFormat df= new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        this.valid = true;
        this.timestamp = df.format(Calendar.getInstance().getTime());
        this.pcb = 0;
        this.command = command;
        this.data = data;
        this.sw1sw2 = 0;
        this.tlv = new TLV();
        System.out.println("command : " + command);
        System.out.println("data : " + data);
        if (data != null) {
            pcb = Utils.unicodeEscaped(data.charAt(0));
        } else {
            valid = false;
        }
        if (data != null && data.length() >= 3) {
            this.data = data.substring(1, data.length() - 2);
            sw1sw2 = (Utils.unicodeEscaped((char) data.charAt(data.length() - 2)) << 8) + Utils.unicodeEscaped((char) data.charAt(data.length() - 1));
            if (command==null||(command != null && command.getNombre() != null && !command.getNombre().equals("READ_BINARY"))) {
                if ((command!=null&&command.getResponseComparitors() != null) || (this.data != null && lista1.contains(new Integer(this.data.charAt(0))))) {
                    tlv = tlv.unserialise(this.data);
                    if (!tlv.serialise().equals(this.data)) {
                        System.out.println("TLV.serialise() != original data");
                    }
                } else {
                    if (this.data.length() > 0) {
                        //#print repr(self._data)
                        //pass
                    }
                }
            }
        } else {
            valid = false;
        }
        valid &= this.validateTLV();
        valid &= (sw1sw2 == 0x9000);
    }

    /**
     * @return the valid
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the pcb
     */
    public int getPcb() {
        return pcb;
    }

    /**
     * @param pcb the pcb to set
     */
    public void setPcb(int pcb) {
        this.pcb = pcb;
    }

    /**
     * @return the command
     */
    public Comando getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(Comando command) {
        this.command = command;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the sw1sw2
     */
    public int getSw1sw2() {
        return sw1sw2;
    }

    /**
     * @param sw1sw2 the sw1sw2 to set
     */
    public void setSw1sw2(int sw1sw2) {
        this.sw1sw2 = sw1sw2;
    }

    public boolean solicited() {
        return (pcb & 0x40) == 0x00;
    }

    public boolean validateTLV() {
        /*Uses the Command's expected responses to validate the received TLV.
         */
        boolean result;
        if (command == null || command.getResponseComparitors() == null || command.getResponseComparitors().length == 0) {
            return true;
        }
        result = false;
        System.out.println("valido tlv");
        for (TagComparitor comparitor : command.getResponseComparitors()) {
            result = result || comparitor.match(this.getTlv());
        }
        System.out.println("fin valido tlv");
        if (!result) {
            System.out.println("***!!!*** Warning: TLV validate failed! ***!!!***");
        }
        return result;
    }

    public String toString() {
        String cmdName = "UNSOLICITED";
        StringBuffer dataTmp = new StringBuffer();
        if (solicited()) {
            if (command != null) {
                cmdName = command.getNombre();
            } else {
                cmdName = "UNKOWN_COMMAND";
            }
        }
        dataTmp.append(String.format("%s : ", timestamp));
        for (int j = 0; j < data.length(); j++) {
            dataTmp.append(String.format("%s ", data.charAt(j)));
        }
        dataTmp.append("\n");
        dataTmp.append(String.format("Valid: %s, For: %s, PCB: %02X, SW1SW2: %04X\n", valid, cmdName, pcb, sw1sw2));
        return dataTmp.toString();
    }

    /**
     * @return the tlv
     */
    public TLV getTlv() {
        return tlv;
    }

    /**
     * @param tlv the tlv to set
     */
    public void setTlv(TLV tlv) {
        this.tlv = tlv;
    }
}
