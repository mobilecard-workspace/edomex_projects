/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Carlos Corona
 */
public class Comando {
    private String nombre;
    private int cla;
    private int ins;
    private int p1;
    private int p2;
    private String data;
    private int le;
    private TagComparitor responseComparitors[];
    Comando(String nombre,int cla, int ins, int p1, int p2, String data, int le,  TagComparitor[] responseComparitors) {
        this.nombre=nombre;
        this.cla = cla;
        this.ins = ins;
        this.p1 = p1;
        this.p2 = p2;
        this.data = data;
        this.le = le;
        this.responseComparitors=responseComparitors;
    }

    /**
     * @return the cla
     */
    public int getCla() {
        return cla;
    }

    /**
     * @param cla the cla to set
     */
    public void setCla(int cla) {
        this.cla = cla;
    }

    /**
     * @return the ins
     */
    public int getIns() {
        return ins;
    }

    /**
     * @param ins the ins to set
     */
    public void setIns(int ins) {
        this.ins = ins;
    }

    /**
     * @return the p1
     */
    public int getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(int p1) {
        this.p1 = p1;
    }

    /**
     * @return the p2
     */
    public int getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(int p2) {
        this.p2 = p2;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the le
     */
    public int getLe() {
        return le;
    }

    /**
     * @param le the le to set
     */
    public void setLe(int le) {
        this.le = le;
    }

    /**
     * @return the responseComparitors
     */
    public TagComparitor[] getResponseComparitors() {
        return responseComparitors;
    }

    /**
     * @param responseComparitors the responseComparitors to set
     */
    public void setResponseComparitors(TagComparitor[] responseComparitors) {
        this.responseComparitors = responseComparitors;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString(){
        StringBuilder st=new StringBuilder();
        st.append("\nnombre : "+this.nombre);
        st.append("\ncla : "+Integer.toHexString(this.cla) );
        st.append("\nins : "+Integer.toHexString(this.ins) );
        st.append("\np1 : "+Integer.toHexString(this.p1 ));
        st.append("\np2 : "+Integer.toHexString(this.p2 ));
        st.append("\ndata : "+this.data );
        st.append("\nle : "+Integer.toHexString(this.le ));
        return st.toString();
    }
}
