/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Carlos Corona
 */
public class Command {

    private static HashMap<String, Comando> commandMap = new HashMap<String, Comando>();
    public static HashMap<String, Comando> comandos = new HashMap<String, Comando>();
    
    public static Comando RESET_DEVICE=new Comando("RESET_DEVICE",0xD0, 0x00, 0, 0, "", 0, new TagComparitor[]{new TagComparitor(
                      new int[]{0xE1, 0x9F1E},
                      new int[]{0xE1, 0xEF, 0xDF0D},
                      new int[]{0xE1, 0xEF, 0xDF7F})});
    public static Comando GET_CONFIGURATION=new Comando("GET_CONFIGURATION",0xD0, 0x01, 0, 0, "", 0,new TagComparitor[]{new TagComparitor(
                      new int[]{0xE1, 0xED, 0xDF0D},
                      new int[]{0xE1, 0xED, 0xDF7F})});
    public static Comando DISPLAY_TEXT=new Comando("DISPLAY_TEXT",0xD2, 0x01, 0, 1, "", 0, new TagComparitor[]{});
    public static Comando CARD_STATUS=new Comando("CARD_STATUS",0xD0, 0x60, 0, 0, "", 0,new TagComparitor[]{new TagComparitor(
                      new int[]{0xE1, 0x48})});
    public static Comando START_TRANSACTION=new Comando("START_TRANSACTION",0xDE, 0xD1, 0, 0, "", 0, new TagComparitor[]{});
    public static Comando P2PE_STATUS=new Comando("P2PE_STATUS",0xEE, 0xE0, 0, 0, "", 0, new TagComparitor[]{});


    static {
        comandos.put("RESET_DEVICE", RESET_DEVICE);
        comandos.put("GET_CONFIGURATION", GET_CONFIGURATION);
        comandos.put("DISPLAY_TEXT", DISPLAY_TEXT);
        comandos.put("CARD_STATUS", CARD_STATUS);
        comandos.put("START_TRANSACTION", START_TRANSACTION);
        comandos.put("P2PE_STATUS", P2PE_STATUS);
    }

    static {
        for (Entry<String, Comando> entry : comandos.entrySet()) {
            String key = entry.getKey();
            Comando com = entry.getValue();
            commandMap.put(Utils.concat((char) com.getCla(), (char) com.getIns()), com);
        }
    }
    public static Comando get(String comand){
        if (comandos.containsKey(comand)) {
            return comandos.get(comand);
        }else{
            return null;
        }
            
    }

    public static Comando getCommandType(int cla, int ins) {
        String key = Utils.concat((char) cla, (char) ins);
        if (commandMap.containsKey(key)) {
            return commandMap.get(key);
        } else {
            return null;
        }
    }
}
