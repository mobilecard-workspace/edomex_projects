/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

/**
 *
 * @author Carlos Corona
 */
/*
 * Created on May 21, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Horacio Sanchez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TarjetaVO {

    /* Track tal y como viene del lector */
    private String track;
    /* Track uno de la tarjeta utilizado cuando la tarjeta es tecleada*/
    private String trackUno;
    /* track dos utilizado cuando la tarjeta es deslizada*/
    private String trackDos;
    /* código de seguridad de la tarjeta */
    private String securityCode;
    /* monto de la transaccion */
    private double amount;
    /* Cuenta */
    private String cuenta;
    /* Titular */
    private String nombre_titular;
    /* Bin Son los 6 primeros digitos de la tarjeta */
    private String bin;
    /* Fecha de Expiracion MMYY*/
    private String fecha_expiracion;
    /* bandera que determina si es American Express a partir de los bines */
    private boolean isAmerican;
    private String message;


    /**
     * @return
     */
    public String getTrack() {
        return track;
    }

    /**
     * @return
     */
    public String getTrackDos() {
        return trackDos;
    }

    /**
     * @return
     */
    public String getTrackUno() {
        return trackUno;
    }

    /**
     * @param string
     */
    public void setTrack(String string) {
        track = string;
    }

    /**
     * @param string
     */
    public void setTrackDos(String string) {
        trackDos = string;
    }

    /**
     * @param string
     */
    public void setTrackUno(String string) {
        trackUno = string;
    }

    /**
     * @return
     */
    public String getSecurityCode() {
        return securityCode;
    }

    /**
     * @param string
     */
    public void setSecurityCode(String string) {
        securityCode = string;
    }

    /**
     * @return
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param d
     */
    public void setAmount(double d) {
        amount = d;
    }

    /**
     * @return
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @return
     */
    public String getNombre_titular() {

        if (nombre_titular == null || nombre_titular.length() == 0 || nombre_titular.equals("")) {
            return "S/N";
        } else {
        	nombre_titular = nombre_titular.trim();
            return this.replaceAllAcentos(nombre_titular);
        }
    }

    /**
     * @param string
     */
    public void setCuenta(String string) {
        cuenta = string;
    }

    /**
     * @param string
     */
    public void setNombre_titular(String string) {
        nombre_titular = string;
    }

    /**
     * @return
     */
    public String getBin() {
        return bin;
    }

    /**
     * @param string
     */
    public void setBin(String _bin) {
        bin = _bin;
    }

    /**
     * @return
     */
    public String getFecha_expiracion() {
        return fecha_expiracion;
    }

    /**
     * @param string
     */
    public void setFecha_expiracion(String string) {
        fecha_expiracion = string;

    }

    public String getMes() {
        try {
            return this.fecha_expiracion.substring(2, 4);
        } catch (Exception e) {
            return "00";
        }

    }

    public String getAno() {
        try {
            return this.fecha_expiracion.substring(0, 2);
        } catch (Exception e) {
            return "00";
        }
    }

    /**
     * @return
     */
    public boolean isAmerican() {
        return isAmerican;
    }

    /**
     * @param b
     */
    public void setAmerican(boolean b) {
        isAmerican = b;
    }

    public String replaceAllAcentos(String strAcentos) {
        if (strAcentos != null && !strAcentos.equals("")) {
        	char[] acentuados = new char[] { '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�' };
            char[] naoAcentuados = new char[]{'c', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u'};
            for (int i = 0; i < acentuados.length; i++) {
                strAcentos = strAcentos.replace(acentuados[i], naoAcentuados[i]);

            }
            return strAcentos;
        }
        return null;
    }

    private String dateFormat(Date date) {
        Locale es_MX = new Locale("es", "MX");
        String patron = "dd MMMMM yyyy hh:mm aaa";
        SimpleDateFormat format = new SimpleDateFormat(patron, es_MX);
        return (format.format(date));
    }

    /*
     * %B4101812069050845^LIRA TERAN/VICTOR FERNANDO^1604201000000000000000228000000?
     */

    public void obtenTracks(String track) {
        int consonantes = 0;
        String trackDos = null;
        String trackUno = null;
        if (track != null && track.length() > 0) {
            char[] arreglo = track.toCharArray();
            for (int i = arreglo.length - 1; i > 0; i--) {
                if (!Character.isDigit(arreglo[i])) {
                    consonantes++;
                }
                if (consonantes == 3) {
                    trackDos = track.substring(i + 1, arreglo.length - 1);
                }
                if (consonantes == 4) {
                    consonantes++;
                    trackUno = track.substring(1, i);
                }
            }
            this.setTrackUno(trackUno);
            System.out.println(this.getTrackUno());
            try {
                /* Obtenemos el track2 */
                this.setTrackDos(trackDos.replaceAll("[^0-9]", "="));
            } catch (Exception e) {
                System.out.println("Error al obtener los datos de la tarjeta");
            }
            try {
                /* Obtenemos el numero de la tarjeta */
                //tarjetaVO.setCuenta(trackDos.substring(0, tarjetaVO.getTrackDos().indexOf("=")));
                this.setCuenta(this.getCuenta(this.getTrackUno()));
            } catch (Exception e) {
                System.out.println("Error al obtener el numero de la tarjeta");
            }
            try {
                /* Obtenemos el Bin que son los 6 primeros numero de la tarjeta */
                System.out.println("Calculando en BIN para el TRACK" + this.getTrackDos());
                this.setBin(this.calculaBin(this.getCuenta()));
            } catch (Exception e) {
                System.out.println("Error al obtener el BIN de la tarjeta");
            }
            try {
                /* Obtenemos el nombre del titular */
                this.setNombre_titular(this.getNombreTitular(track, this.getCuenta()));
            } catch (Exception e) {
                System.out.println("Error al obtener el titular de la tarjeta");
            }

            try {
                /* Obtenemos la fecha de expiracion de la tarjeta TC */
                this.setFecha_expiracion(this.getExpirationDate(track, this.getCuenta(), this.getNombre_titular()));
            } catch (Exception e) {
                System.out.println("Error al obtener la fecha de expiracion de la tarjeta");
            }
        }
        this.setTrack(track);
    }

    public String getCuenta(String trackUno) {
        if (trackUno != null) {
            return trackUno.substring(1, trackUno.indexOf("^"));
        } else {
            return trackUno;
        }
    }

    public String calculaBin(String cuenta) {
        if (cuenta != null && cuenta.length() > 5) {
            System.out.println("Cuenta:" + cuenta + " Longitud:" + cuenta.trim().length());
            if (cuenta.trim().length() == 15) {
                return cuenta.substring(0, 4);
            } else {
                return cuenta.substring(0, 6);
            }
        } else {
            return null;
        }
    }

    public String getNombreTitular(String track, String cuenta) {
        String nombre_titular = null;
        String apellidos=null;
        String nombres=null;
        if (track != null && cuenta != null) {
            int long_cuenta = cuenta.length();
            int inicio_nombre = track.indexOf("^");//long_cuenta + 3; // +3 caracteres de separación
            int fin_nombre = 0;//track.indexOf("^", inicio_nombre);
            if (inicio_nombre == -1) {
                inicio_nombre = track.indexOf("&");
                fin_nombre = track.indexOf("&", inicio_nombre + 1);
            } else {
                fin_nombre = track.indexOf("^", inicio_nombre + 1);
            }
            nombre_titular = track.substring(inicio_nombre + 1, fin_nombre);
            apellidos = nombre_titular.substring(0,nombre_titular.indexOf("/"));
            nombres = nombre_titular.substring(nombre_titular.indexOf("/")+1);
        }
        return nombres+" "+apellidos;
    }

    public String getExpirationDate(String track, String cuenta, String nombre_titular) {
        String fecha_expiracion = null;
        if (track != null && cuenta != null && nombre_titular != null) {
            /*int long_cuenta = cuenta.length();
             int long_nombre = nombre_titular.length();
             int fin_nombre = long_cuenta + 4 + long_nombre;
		
             fecha_expiracion = track.substring(fin_nombre,fin_nombre+4);
             */
            int inicio_nombre = track.indexOf("^");//long_cuenta + 3; // +3 caracteres de separación
            if (inicio_nombre == -1) {
                inicio_nombre = track.indexOf("&");
                inicio_nombre = track.indexOf("&", inicio_nombre + 1);
            } else {
                inicio_nombre = track.indexOf("^", inicio_nombre + 1);
            }
            inicio_nombre += 1;
            //fin_nombre = inicio_nombre+4; 
            fecha_expiracion = track.substring(inicio_nombre, inicio_nombre + 4);
        }
        
        return fecha_expiracion;
    }
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("track : ").append(this.track).append("\n");
        sb.append("trackUno : ").append(this.trackUno).append("\n");
        sb.append("trackDos : ").append(this.trackDos).append("\n");
        sb.append("securityCode : ").append(this.securityCode).append("\n");
        sb.append("amount : ").append(this.amount).append("\n");
        sb.append("cuenta : ").append(this.cuenta).append("\n");
        sb.append("nombre_titular : ").append(this.nombre_titular).append("\n");
        sb.append("bin : ").append(this.bin).append("\n");
        sb.append("fecha_expiracion : ").append(this.fecha_expiracion).append("\n");
        sb.append("isAmerican : ").append(this.isAmerican).append("\n");
        return sb.toString();
    }
    public static void main(String args[]){
        TarjetaVO tarjeta=new TarjetaVO();
        tarjeta.obtenTracks("%B4101812069050845^LIRA TERAN/VICTOR FERNANDO^1604201000000000000000228000000?");
        System.out.println(tarjeta);
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
