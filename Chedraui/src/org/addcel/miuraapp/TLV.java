/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.List;

import org.addcel.miuraapp.vo.TLVItem;

/**
 *
 * @author Carlos Corona
 */
public class TLV {

    private ArrayList prefix;
    private ArrayList<TLVItem> objects = new ArrayList<TLVItem>();

    public TLV() {
    }

    public TLV(Tag t, Object value) {
        this.append(t, value);
    }

    public void append(Tag tag, Object value) {
        if (value instanceof String || value instanceof TLV) {
            System.out.println(tag + "nuevo dato tlv " + value);
            objects.add(new TLVItem(new Tag(tag), value));
        }
    }

    /**
     * @return the prefix
     */
    public ArrayList getPrefix() {
        return prefix;
    }

    /**
     * @param prefix the prefix to set
     */
    public void setPrefix(ArrayList prefix) {
        this.prefix = prefix;
    }

    /**
     * @return the objects
     */
    public ArrayList<TLVItem> getObjects() {
        return objects;
    }

    /**
     * @param objects the objects to set
     */
    public void setObjects(ArrayList<TLVItem> objects) {
        this.objects = objects;
    }

    public TLV unserialise(String data) {
        TLV result = new TLV();
        int length = 0;
        Object tmp1[];
        Object tmp2[];

        while (length >= 0 && data.length() > 0) {
            System.out.println("Unserialise");
            tmp1 = getTag(data);

            tmp2 = getLength((String) tmp1[1]);
            System.out.println("------EL TAG----------->" + tmp1[0] + " : " + tmp1[1]);
            System.out.println("------EL LENGTH-------->" + tmp2[0] + " : " + tmp2[1]);
            length = (Integer) tmp2[0];
            data = (String) tmp2[1];
            if (tmp1[1] != null) {
                if (((Tag) tmp1[0]).constructed()) {
                    result.append((Tag) tmp1[0], unserialise(data.substring(0, length)));
                } else {
                    result.append((Tag) tmp1[0], data.substring(0, length));
                }
                data = data.substring(length, data.length());
            }
        }
        return result;
    }

    public Object[] getLength(String data) {
        Object[] response = new Object[2];
        int length = 0;
        int lenBytes = 1;
        /*

         Gets a TLV "length" from data and returns it as an integer along with the
         data remaining after the length element is removed.
         */
        if (data.length() < 1) {
            response[0] = -1;
            response[1] = null;
            return response;
        }
        if ((Utils.unicodeEscaped(data.charAt(0)) & 0x80) == 0x80) {
            lenBytes += Utils.unicodeEscaped(data.charAt(0)) & 0x7f;
            System.out.println("***********>"+lenBytes);
            for (char i : data.substring(1, lenBytes).toCharArray()) {
                length = (length << 8) + Utils.unicodeEscaped(i);
            }

        } else {
            length = Utils.unicodeEscaped(data.charAt(0)) & 0x7f;
        }
        response[0] = length;
        response[1] = data.substring(lenBytes, data.length());
        return response;
    }

    public Object[] getTag(String data) {
        Object[] resp = new Object[2];
        /*
         Gets a TLV "tag" from data and returns it as a Tag object along with the
         data remaining after the tag element is removed.
         */
        int tagOrd = 0;
        ArrayList<Integer> tag = new ArrayList<Integer>();
        if (data.length() < 1) {
            return null;
        }
        tagOrd = Utils.unicodeEscaped(data.charAt(0));
        tag.add(tagOrd);
        if ((tagOrd & 0x1f) == 0x1f) {
            for (int i = 1; i < data.length(); i++) {
                int tmp = Utils.unicodeEscaped(data.charAt(i));
                tag.add(tmp);
                if ((tmp & 0x80) != 0x80) {
                    break;
                }
            }
        }
        //Collections.reverse(tag);
        resp[0] = new Tag(tag);
        resp[1] = data.substring(tag.size(), data.length());
        return resp;
    }

    public void append(Tag tag, List<Integer> value) {
        /*

         Adds a new tag/value pair to 'x'.

         'tag' - any type supported by Tag.__init__().
         'value' - can be either a string for a primitive tag, or another TLV
         object for a constructed tag.

         Note: A TLV object passed as 'value' is used by reference.  Modifying
         the original object later also affects 'x'.
         */
        objects.add(new TLVItem(new Tag(tag), value));
    }

    public String serialise() {
        /*
         Returns formatted TLV data as a string.
         */
        StringBuffer data = new StringBuffer("");
        StringBuffer tmpData = new StringBuffer("");
        int lenBytes = 0;
        for (TLVItem item : objects) {
            data.append(item.getTag().serialise());
            tmpData = new StringBuffer("");
            if (item.getValue() instanceof String) {
                tmpData = new StringBuffer((String) item.getValue());
            } else if (item.getValue() instanceof TLV) {
                tmpData.append(((TLV) item.getValue()).serialise());
            } else {
            }
            lenBytes = 0;
            if (tmpData.length() >= 128) {
                lenBytes += 1;
            }
            if (tmpData.length() >= 256) {
                lenBytes += 1;
            }
            if (lenBytes > 0) {
                data.append((char) (0x80 + lenBytes));
                for (int index = lenBytes; index > 0; index--) {
                    data.append((char) ((tmpData.length() >> (8 * index)) & 0xff));
                }
            } else {
                data.append((char) (tmpData.length()));
            }
            data.append(tmpData);
        }
        return data.toString();
    }

    public int len() {
        return objects.size();
    }

    public TLVItem getitem(int index) {
        if (len() > index) {
            return objects.get(index);
        }
        return null;
    }

    public TLV copy() {
        return unserialise(this.serialise());
    }

    public List<Object> search(List<Tag> tagPath) {

        ArrayList<Object> values = new ArrayList<Object>();
        /*
         x.search(tagPath) -> list

         Returns a list of values matching the specified tagPath.  tagPath is a
         list of Tag objects, or values which can be converted Tag objects via the
         Tag constructor.  The return list is empty [] if no matches are found.
         */
        List<Tag> tail = new ArrayList<Tag>();
        List<Object> tmp = null;
        for (Object tmptmp : tagPath) {
            System.out.println("____>" + tmptmp);
        }
        System.out.println();
        Tag head = new Tag(tagPath.get(0));
        System.out.println("head : " + head);
        tail = tagPath.subList(1, tagPath.size());
        for (TLVItem t : objects) {
            System.out.println("t.getTag() : " + t.getTag());
            if (t.getTag().toString().equals(head.toString())) {
                if (tail.size() > 0 && t.getValue() instanceof TLV) {
                    System.out.println("Es un TLV");
                    tmp = ((TLV) t.getValue()).search(tail);
                    for (Object tlvtmp : tmp) {
                        values.add(tlvtmp);
                    }
                } else if (tail.size() == 0) {
                    System.out.println("no es un TLV");
                    values.add(t.getValue());
                }
            }
        }
        System.out.println("tamaño de los values : " + values.size());
        return values;
    }

    public Object firstMatch(List<Tag> tagPath) {
        List matches;
        /*
         *  
         x.firstMatch(tagPath) -> str (if matching tag is primitive)
         x.firstMatch(tagPath) -> TLV (if matching tag is constructed)
         x.firstMatch(tagPath) -> None (if no matching tag)

         Returns the first value that matches the tagPath.  Returns None if
         there is no match.  Note that a value return of "" (empty string)
         means a match *is* found (albeit 0 length).
         */
        matches = search(tagPath);
        System.out.println(matches);
        if (matches != null && !matches.isEmpty()) {
            return matches.get(0);
        }
        return null;
    }

    public boolean empty() {
        return this.len() == 0;
    }

    public Tag firstTag() {
        TLVItem item;
        /*
         x.firstTag(asInt) -> Tag
         x.firstTag(asInt) -> None

         Returns the first Tag in this TLV.  If asInt is True the value
         returned is an integer converted from the corresponding Tag
         object.  Returns None if there are no items.
         */
        if (objects != null && !objects.isEmpty()) {
            item = objects.get(0);
            return item.getTag();
        } else {
            return null;
        }
    }

    public List<Tag> tags() {
        List<Tag> result = new ArrayList<Tag>();
        /*
         x.tags(asInts) -> list

         Returns a list of all the Tag items in this TLV.  Note: tags from
         inner constructed items are not included.  If asInts is True then
         the returned list contains integers converted from the Tag
         objects.
         */
        for (TLVItem item : objects) {
            result.add(item.getTag());
        }
        return result;
    }
}
