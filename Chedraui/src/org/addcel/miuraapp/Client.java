/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.io.DataInputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 *
 * @author Carlos Corona
 */
public class Client {

    private static ArrayList<Comando> sentCommands = new ArrayList<Comando>();
    private static ArrayList<Response> receivedResponses = new ArrayList<Response>();
    private DataInputStream din;
    private OutputStream ost;
    
    public Client(DataInputStream din, OutputStream ost){
        this.din=din;
        this.ost=ost;
    }
    public void sendCommand( Comando comando) {
        sendAPPDU(comando.getCla(), comando.getIns(), comando.getP1(), comando.getP2(), comando.getData(), comando.getLe());
    }

    public void sendAPPDU(int cla, int ins, int p1, int p2, String dataField, int le) {
        ArrayList<Integer> packet = null;
        StringBuilder mensaje = new StringBuilder("");
        Comando cmdType=null;
        String cmdName="";
        byte mensajeArr[] = null;
        int lrc;
        try {
            System.out.println("Entro a envio comando");
            if (getOst() != null) {
                packet = new ArrayList<Integer>();
                packet.add(0x01);
                packet.add(0x00);
                packet.add(0x04);
                packet.add(cla);
                packet.add(ins);
                packet.add(p1);
                packet.add(p2);
                if (dataField != null && dataField.length() > 0) {
                    packet.set(2, packet.get(2) + dataField.length() + 1);
                    packet.add(dataField.length());
                    for (int i = 0; i < dataField.length(); i++) {
                        packet.add(Utils.unicodeEscaped(dataField.charAt(i)));
                    }
                }
                if (le > 0) {
                    packet.set(2, packet.get(2) + 1);// # Adjust LEN (Le)
                    packet.add(le);
                }
                lrc = 0;
                for (Integer b : packet) {
                    lrc ^= b;
                }
                packet.add(lrc);
                mensajeArr = new byte[packet.size()];
                for (int i = 0; i < packet.size(); i++) {
                    mensajeArr[i] = (byte) packet.get(i).intValue();
                }
                StringBuffer tmpStr = new StringBuffer();
                for (int j = 0; j < mensajeArr.length; j++) {
                    tmpStr.append(String.format("%02X ", mensajeArr[j]));
                }
                System.out.println("Envio : " + tmpStr.toString());
                cmdType = Command.getCommandType(cla, ins);
                cmdName = cmdType!=null?cmdType.getNombre(): "UNKNOWN_COMMAND";
                System.out.println(cmdName);
                sentCommands.add(cmdType);
                getOst().write(mensajeArr);
                getOst().flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Response internalReceiveResponse() {
        ArrayList<Integer> buff;
        StringBuffer response = new StringBuffer();
        Comando comando = null;
        while (true) {
            buff = receivePacket();
            if (buff.size() >= 5) {
                if (response.length()==0) {
                    response.append((char)buff.get(1).intValue());
                }
                for (int i = 3; i < buff.size() - 1; i++) {
                    response.append((char)buff.get(i).intValue());
                }
                if (Utils.unicodeEscaped((char) buff.get(1).intValue()) != 0x01 && Utils.unicodeEscaped((char) buff.get(1).intValue()) != 0x41) {
                    break;
                }
            } else {
                break;
            }
        }
        if (response.length()>= 3) {
            //StringBuffer tmpStr = new StringBuffer();
            //for (int j = 0; j < response.size(); j++) {
            //    tmpStr.append(String.format("%02X ", response.get(j)));
            //}
            System.out.println("RX:\n" + response.toString());
            if (!sentCommands.isEmpty() && ((Utils.unicodeEscaped((char) response.charAt(0)) & 0x40) == 0 || sentCommands.get(0).getNombre().equals("CARD_STATUS"))) {
                comando = sentCommands.remove(0);
            }
            return new Response(comando, response.toString());
        } else {
            return new Response(null, null);
        }
    }
    /*
     * internalReceiveResponse(self, timeout):
     """
     Internal function, do not call this.
     """
     response = ""
     while True:
     buff = self._receivePacket(timeout)
     if len(buff) >= 5:
     if len(response) == 0:
     response = buff[1] # Response format is PCB/Data
     response += buff[3:-1]
     if ord(buff[1]) != 0x01 and ord(buff[1]) != 0x41:
     break
     else:
     break
     if len(response) >= 3:
     self._log.writeLine("RX:\n" + hexDump(response, 2))
     return Response(self._sentCommands.pop(0) if self._sentCommands and ((ord(response[0]) & 0x40 == 0) or self._sentCommands[0] == CARD_STATUS) else None, response)
     else:
     return Response()
     */

    public ArrayList<Integer> receivePacket() {
        ArrayList<Integer> packet = new ArrayList<Integer>();
        Integer rx_byte = new Integer(0);
        int count = 0;
        int lrc = 0;
        int iter=0;
        if (getDin() != null) {
            try {
                while (rx_byte >= 0) {
                    rx_byte = (int) getDin().readByte();
                    rx_byte = Utils.unicodeEscaped((char) rx_byte.intValue());
                    System.out.println("Recibo 1 " +(iter++)+" : " + Integer.toHexString(rx_byte));
                    packet.add(rx_byte);
                    if (packet.size() == 1) {
                        if (rx_byte != 1) {
                            System.out.println("BAD NAD : " + Integer.toHexString(rx_byte));
                            packet = new ArrayList<Integer>();
                        }
                    } else if (packet.size() == 2) {
                        if (rx_byte != 0 && rx_byte != 0x01 && rx_byte != 0x40 && rx_byte != 0x41) {
                            System.out.println("BAD PCB : " + Integer.toHexString(rx_byte));
                            packet = new ArrayList<Integer>();
                        }
                    } else {
                        if (rx_byte == 0 || rx_byte == 0xFF) {
                            System.out.println("BAD LEN : " + Integer.toHexString(rx_byte));
                            packet = new ArrayList<Integer>();
                            continue;
                        }
                        rx_byte = 0;
                        System.out.println("Recibire: " + (packet.get(2)+1));
                        count=(packet.get(2)+1);
                        while (count>0) {
                            rx_byte = (int) getDin().readByte();
                            System.out.println("Recibo ------->  "+ Integer.toHexString(rx_byte));
                            packet.add(Utils.unicodeEscaped((char) rx_byte.intValue()));
                            System.out.println("Recibo 2  "+count+ " : " + Integer.toHexString(rx_byte));
                            count--;
                        }
                        lrc = 0;
                        for (Integer i : packet) {
                            lrc ^= i;
                        }
                        if (lrc == 0) { // got a valid packet
                            StringBuffer tmpStr = new StringBuffer();
                            for (int j = 0; j < packet.size(); j++) {
                                tmpStr.append(String.format("%02X ", packet.get(j)));
                            }
                            System.out.println("Recibi : " + tmpStr.toString());
                            break;
                        } else {
                            System.out.println("BAD LRC : " + Integer.toHexString(lrc));
                            packet = new ArrayList<Integer>();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return packet;
    }
    /*
     * receivePacket(self, timeout):
     """
     Internal function, do not call this.
     """
     packet = []
     if not self._connector.connected():
     self._log.writeLine("Not connected")
     return ""

     while timeout > 0:
     try:
     rx_byte = self._connector.recv(1)
     except Error.Timeout:
     time.sleep(0.01)
     timeout -= 10
     continue
     except Error.NotConnected:
     self._log.writeLine("_receivePacket Not connected")
     raise
     except Exception, e:
     self._log.writeLine("_receivePacket Unknown error")
     raise

     timeout = 100 # started receiving packet, drop timeout to 100ms
     rx_byte = ord(rx_byte[0])
     packet.append(rx_byte)
     if len(packet) == 1:
     if  rx_byte != 1:
     self._log.writeLine("BAD NAD %02X" % rx_byte)
     packet = []
     elif len(packet) == 2:
     if rx_byte != 0 and rx_byte != 0x01 and rx_byte != 0x40 and rx_byte != 0x41:
     self._log.writeLine("BAD PCB %02X" % rx_byte)
     packet = []
     else:
     if rx_byte == 0 or rx_byte == 0xFF:
     self._log.writeLine("BAD LEN %02X" % rx_byte)
     packet = []
     continue
     try:
     rx_block = self._connector.recv(packet[2]+1)
     if len(rx_block) == packet[2]+1:
     packet += map(ord, rx_block)
     lrc = 0
     for byte in packet:
     lrc ^= byte
     if lrc == 0: # got a valid packet
     break

     self._log.writeLine("BAD LRC %02X" % rx_byte)
     packet = []
     else:
     self._log.writeLine("Receive error, failed to grab entire information_field and lrc")
     self._log.writeLine("expected %i bytes, got %i" % (packet[2]+1, len(rx_block)))

     except Exception, e:
     self._log.writeLine("Receive error, failed to grab inf/lrc due to " + str(e))
     packet = []
     time.sleep(0.01)
     timeout -= 10
     continue
     # end of while timeout:

     if timeout <= 0:
     raise Error.Timeout()
     return ''.join('%c' % b for b in packet)
     */

    public Response receiveResponse(Boolean solicited, Boolean valid) {
        Response response = null;
        while (true) {
            for (int i = 0; i < receivedResponses.size(); i++) {
                response = receivedResponses.get(i);
                System.out.println(response);
                if ((solicited == null || solicited == response.solicited()) && (valid == null || valid == response.isValid())) {
                    response=receivedResponses.remove(i);
                    return response;
                }
            }
            receivedResponses.add(internalReceiveResponse());
        }
    }

    /**
     * @return the din
     */
    public DataInputStream getDin() {
        return din;
    }

    /**
     * @param din the din to set
     */
    public void setDin(DataInputStream din) {
        this.din = din;
    }

    /**
     * @return the ost
     */
    public OutputStream getOst() {
        return ost;
    }

    /**
     * @param ost the ost to set
     */
    public void setOst(OutputStream ost) {
        this.ost = ost;
    }
}
