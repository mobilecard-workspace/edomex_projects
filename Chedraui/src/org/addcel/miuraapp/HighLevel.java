/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Corona
 */
public class HighLevel {

    public Response getSwipe(Client mpic, String mesage, int status) {
        /*
         getSwipe(mpic, message, status, timeout) -> Response
         getSwipe(mpic, message, status, timeout) -> None

         Waits for a magnetic card swipe provided the CARD STATUS doesn't match
         the given "status".  Returns a Response object containing the swipe data
         if read, or None for a timeout.

         mpic - A connected MPI.Client.Client object.
         message - string to display on terminal.
         status - value that CARD STATUS is expected not to match.
         timeout - optional timeout in milliseconds.  Default:30000 (30 sec).
         */
        Comando c = Command.CARD_STATUS;
        Response response;
        Response cardData=null;
        c.setP1(0x0F);
        mpic.sendCommand(c);
        response = mpic.receiveResponse(null, null);
        List<Tag> tmpLst = new ArrayList<Tag>();
        tmpLst.add(new Tag(0xe1));
        tmpLst.add(new Tag(0x48));
        String cardstatus = (String) response.getTlv().firstMatch(tmpLst);
        for (int j = 0; j < cardstatus.getBytes().length; j++) {
            System.out.format("%02X ", cardstatus.getBytes()[j]);
        }

        if (cardstatus != null && !cardstatus.equals(Integer.toString(status, 16))) {
            c = Command.DISPLAY_TEXT;
            c.setData(mesage);
            c.setP1(1);
            mpic.sendCommand(c);
            response = mpic.receiveResponse(null, null);

            cardData = mpic.receiveResponse(null, null);


            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xe1));
            tmpLst.add(new Tag(0x48));
            String tlvdata = (String) cardData.getTlv().firstMatch(tmpLst);
            System.out.println("**********>" + tlvdata);
            if (tlvdata != null) {
                cardstatus = tlvdata;
            }
        }
        mpic.sendCommand(Command.CARD_STATUS);
        response = mpic.receiveResponse(null, null);
        return cardData;

    }
}
