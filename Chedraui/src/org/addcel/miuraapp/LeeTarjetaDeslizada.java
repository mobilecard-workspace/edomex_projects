/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Corona
 */
public class LeeTarjetaDeslizada {

    public TarjetaVO transaccion(Client mpic) {
        String PED_TerminalID = "";
        String PED_MPIname = "";
        String PED_MPIver = "";
        String PED_OSname = "";
        String PED_OSver = "";
        String PED_OStype = "";
        String P2PEstatus = "";
        TarjetaVO tarjeta=new TarjetaVO();
        Response response;
        HighLevel highLevel;
        mpic.sendCommand(Command.RESET_DEVICE);
        response = mpic.receiveResponse(null, null);
        List<Tag> tmpLst = new ArrayList<Tag>();
        tmpLst.add(new Tag(0xE1));
        tmpLst.add(new Tag(0x9F1E));
        PED_TerminalID = (String) response.getTlv().firstMatch(tmpLst);
        System.out.println("PED_TerminalID : " + PED_TerminalID);
        if (PED_TerminalID == null || PED_TerminalID.length() == 0) {
            System.out.println("tlv error on Reset Device response.");
            tarjeta.setMessage("tlv error on Reset Device response.");
            return tarjeta;
        }
        tmpLst = new ArrayList<Tag>();
        tmpLst.add(new Tag(0xE1));
        tmpLst.add(new Tag(0xEF));
        List tlvEF = response.getTlv().search(tmpLst);
        if (tlvEF != null && !tlvEF.isEmpty()) {//this should not 'assume' MPIname then OSname order of elements...
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xDF0D));
            PED_MPIname = (String) ((TLV) tlvEF.get(0)).search(tmpLst).get(0);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xDF7F));
            PED_MPIver = (String) ((TLV) tlvEF.get(0)).search(tmpLst).get(0);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xDF0D));
            PED_OSname = (String) ((TLV) tlvEF.get(1)).search(tmpLst).get(0);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xDF7F));
            PED_OSver = (String) ((TLV) tlvEF.get(1)).search(tmpLst).get(0);
            System.out.println("PED_MPIname : " + PED_MPIname);
            System.out.println("PED_MPIver  : " + PED_MPIver);
            System.out.println("PED_OSname  : " + PED_OSname);
            System.out.println("PED_OSver   : " + PED_OSver);
        } else {
            System.out.println("tlv error on Reset Device response.");
            tarjeta.setMessage("tlv error on Reset Device response.");
            return tarjeta;
        }
        if (PED_OSname.indexOf("TESTOS") >= 0 || PED_OSname.indexOf("DEVOS") >= 0) {
            PED_OStype = "TEST";
        } else {
            PED_OStype = "LIVE";
        }
        mpic.sendCommand(Command.P2PE_STATUS);
        response = mpic.receiveResponse(null, null);
        if (!response.isValid()) {
            System.out.println(String.format("Error on P2PE STATUS, sw1sw2: 0x%02X ", response.getSw1sw2()));
            tarjeta.setMessage(String.format("Error on P2PE STATUS, sw1sw2: 0x%02X ", response.getSw1sw2()));
            return tarjeta;
        }
        tmpLst = new ArrayList<Tag>();
        tmpLst.add(new Tag(0xe1));
        tmpLst.add(new Tag(0xDFAE01));
        Object tlvdata = (Object) response.getTlv().firstMatch(tmpLst);
        System.out.println("--->" + tlvdata);
        if (tlvdata != null) {
            //P2PEstatus = tlvdata;
            System.out.println(P2PEstatus);
        }
        highLevel = new HighLevel();
        Response carddata = highLevel.getSwipe(mpic, "\n     SWIPE CARD", 0x05);
        Comando c = Command.DISPLAY_TEXT;
        c.setData("\n     Processing");
        c.setP1(1);
        mpic.sendCommand(c);
        response = mpic.receiveResponse(null, null);
        if (carddata != null) {
            TLV tlv = carddata.getTlv();
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xe1));
            tmpLst.add(new Tag(0xDFAE02));
            Object tlvDFAE02 = tlv.firstMatch(tmpLst);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xe1));
            tmpLst.add(new Tag(0xDFAE03));
            Object tlvDFAE03 = tlv.firstMatch(tmpLst);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xe1));
            tmpLst.add(new Tag(0xDFAE22));
            Object tlvDFAE22 = tlv.firstMatch(tmpLst);
            tmpLst = new ArrayList<Tag>();
            tmpLst.add(new Tag(0xe1));
            tmpLst.add(new Tag(0x5F21));
            Object tlv5f21 = tlv.firstMatch(tmpLst);
            System.out.println(tlvDFAE02);
            System.out.println(tlvDFAE03);
            System.out.println(tlvDFAE22);
            System.out.println(tlv5f21);
            tarjeta.obtenTracks((String)tlv5f21);
        }
        System.out.println(tarjeta);
        return tarjeta;
    }
}
