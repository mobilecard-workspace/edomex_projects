/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.addcel.miuraapp;

import java.util.ArrayList;
import java.util.List;

import org.addcel.miuraapp.vo.TagItem;

/**
 *
 * @author Carlos Corona
 */
public class Tag {

    private ArrayList<Integer> value = new ArrayList<Integer>();


    public Tag(Object data) {
        if (data instanceof Integer) {
            while (((Integer) data).intValue() > 0) {
                value.add(0,new Integer(((Integer) data).intValue() & 0xff));
                data = ((Integer) data).intValue() >> 8;
            }
        } else if (data instanceof String) {
            for (int i = 0; i < ((String) data).length(); i++) {
                value.add(i, Utils.unicodeEscaped(((String) data).charAt(i)));
            }
        } else if (data instanceof int[]) {
            for (int i = 0; i < ((int[]) data).length; i++) {
                value.add(i, (Integer) ((int[]) data)[i]);
            }
        } else if (data instanceof List) {
            for (int i = 0; i < ((List) data).size(); i++) {
                value.add(i,(Integer) ((List) data).get(i));
            }
        }else if (data instanceof Tag) {
            this.value = ((Tag) data).getValue();
        }else{
            System.out.println("No entro");
        }
        System.out.println("Soy un nuevo tag : "+this);
    }

    /**
     * @return the value
     */
    public ArrayList<Integer> getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(ArrayList<Integer> value) {
        this.value = value;
    }

    public boolean constructed() {
        /*
         Returns True if tag coding indicates its data is "constructed",
         i.e. the associated data is more TLV items.
         */
        return (value.get(0).intValue() & 0x20) == 0x20;
    }

    public String serialise() {
        StringBuilder result = new StringBuilder("");
        for (Integer i : value) {
            result.append((char) i.intValue());
        }
        return result.toString();
    }

    public String toString() {
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i<value.size(); i++) {
             tmp.append(String.format("%02X ", value.get(i).intValue()));
             //tmp.append(value.get(i).intValue()).append(" ");
        }
        return tmp.toString();
    }

    public void description() {
    }

    public TagItem tagLookup(String description) {
        for (TagItem t:Tags.descriptions) {
            if(t.getDescripcion().equalsIgnoreCase(description)){
                return t;
            }
        }
        return null;
    }
}