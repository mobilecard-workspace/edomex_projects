package org.addcel.util;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.addcel.dto.ItemCatalogoDTO;
import org.addcel.prosa.dto.Rol;
import org.addcel.prosa.dto.Tienda;
import org.addcel.prosa.dto.Usuario;
import org.addcel.prosa.dto.Venta;

import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.widget.ArrayAdapter;

public class AppUtils {
	public static String getVersionName(Context context, Class<?> cls) 
    {
      try {
        ComponentName comp = new ComponentName(context, cls);
        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
        return pinfo.versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
        return null;
      }
    }
	
	public static ArrayAdapter<ItemCatalogoDTO> createCatalogAdapter(Context c, List<ItemCatalogoDTO> list) {
		
		ArrayAdapter<ItemCatalogoDTO> adapter = new ArrayAdapter<ItemCatalogoDTO>(c, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	public static ArrayAdapter<Tienda> createTiendaAdapter(Context c, List<Tienda> list) {
		ArrayAdapter<Tienda> adapter = new ArrayAdapter<Tienda>(c, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	public static ArrayAdapter<Rol> createRolAdapter(Context c, List<Rol> list) {
		ArrayAdapter<Rol> adapter = new ArrayAdapter<Rol>(c, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	public static ArrayAdapter<Usuario> createUsuarioAadapter(Context c, List<Usuario> list) {
		
		ArrayAdapter<Usuario> adapter = new ArrayAdapter<Usuario>(c, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	public static ArrayAdapter<Venta> createVentaAadapter(Context c, List<Venta> list) {
			
			ArrayAdapter<Venta> adapter = new ArrayAdapter<Venta>(c, android.R.layout.simple_spinner_item, list);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			
			return adapter;
	}
	
	public static LinkedList<String> getAniosVigencia() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		LinkedList<String> anios = new LinkedList<String>();
		
		int amount = anio + 7;
		
		for (int i = anio; i < amount; i++) {
			anios.add("" + i);
		}
		
		return anios;
	}
}
