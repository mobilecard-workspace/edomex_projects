/**
 * 
 */
package org.addcel.prosa.adapter;

import java.util.List;

import org.addcel.chedraui.R;
import org.addcel.prosa.dto.Modulo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author ADDCEL14
 *
 */
public class ButtonListAdapter extends BaseAdapter {
	List<Modulo> data;
	Activity act;
	LayoutInflater inflater;
	View v;
	
	public ButtonListAdapter(Activity act, List<Modulo> data){
		this.act = act;
		this.data = data;
		inflater = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		Modulo modulo = data.get(position);
		
		View v = null;		
		v = inflater.inflate(R.layout.list_item, null);
		TextView tv = (TextView) v.findViewById(R.id.nombre_tramite);
		tv.setText(modulo.getDescripcion());
		
		return v;
	}

}
