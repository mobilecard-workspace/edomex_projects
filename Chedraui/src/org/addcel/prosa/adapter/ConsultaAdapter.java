package org.addcel.prosa.adapter;

import java.util.List;

import org.addcel.chedraui.R;
import org.addcel.prosa.adapter.ResultadosAdapter.ViewHolder;
import org.addcel.prosa.constants.Moneda;
import org.addcel.prosa.dto.Venta;
import org.addcel.util.Text;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ConsultaAdapter extends BaseAdapter {
	
	private Context c;
	private List<Venta> data;
	private LayoutInflater inflater;
	
	public ConsultaAdapter(Context c, List<Venta> data) {
		this.c = c;
		this.data = data;
		inflater = (LayoutInflater) this.c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		
		if (null == arg1) {
			arg1 = inflater.inflate(R.layout.item_consulta, arg2, false);
			
			viewHolder = new ViewHolder();
			viewHolder.descripcion = (TextView) arg1.findViewById(R.id.text_desc);
			viewHolder.total = (TextView) arg1.findViewById(R.id.text_total);
			viewHolder.moneda = (TextView) arg1.findViewById(R.id.text_moneda);
			viewHolder.fecha = (TextView) arg1.findViewById(R.id.text_fecha);
			
			arg1.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		Venta venta = data.get(arg0);
		
		if (null != venta) {
			
			double total = Double.parseDouble(venta.getTotal());
			
			String moneda = "";
			
			if (Moneda.PESO.equals(venta.getMoneda()))
				moneda = "MXN";
			else if (Moneda.DOLAR.equals(venta.getMoneda()))
				moneda = "DLL";
			
			viewHolder.descripcion.setText(venta.getDescProducto());
			viewHolder.total.setText(Text.formatCurrency(total, true));
			viewHolder.moneda.setText(moneda);
			viewHolder.fecha.setText(venta.getFecha());
		}
		
		return arg1;
	}
	
	static class ViewHolder {
		TextView descripcion;
		TextView total;
		TextView moneda;
		TextView fecha;
	}

}
