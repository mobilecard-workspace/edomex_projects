package org.addcel.prosa.adapter;

import java.util.List;

import org.addcel.chedraui.R;
import org.addcel.prosa.dto.Usuario;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ResultadosAdapter extends BaseAdapter {

	private List<Usuario> data;
	private Activity act;
	private LayoutInflater inflater;
	
	public ResultadosAdapter(Activity act, List<Usuario> data) {
		this.act = act;
		this.data = data;
		inflater = (LayoutInflater) this.act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.item_usuario, arg2, false);
			
			viewHolder = new ViewHolder();
			viewHolder.nombre = (TextView) arg1.findViewById(R.id.text_nombre);
			viewHolder.login = (TextView) arg1.findViewById(R.id.text_login);
			viewHolder.role = (TextView) arg1.findViewById(R.id.text_status);
			
			arg1.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		Usuario usuario = data.get(arg0);
		
		if (null != usuario) {
			viewHolder.nombre.setText(usuario.getNombres() + " " + usuario.getApellidoP() + " " + usuario.getApellidoM());
			viewHolder.login.setText(usuario.getLogin());
			viewHolder.role.setText(usuario.getDescStatus());
		}
		
		return arg1;
	}
	
	static class ViewHolder {
		TextView nombre;
		TextView login;
		TextView role;
	}

}
