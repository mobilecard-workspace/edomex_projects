package org.addcel.prosa.constants;

public class IdModulo {
	
	public static final int REGISTRO= 1;
	public static final int BUSQUEDA = 2;
	public static final int CONSULTAS = 3;
	public static final int CAMBIO_PASS = 4;
	public static final int PAGO_VISA = 5;
	public static final int PAGO_AMEX = 6;
	public static final int CONSULTA_VENTAS = 7; 
}
