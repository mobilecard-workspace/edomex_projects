package org.addcel.prosa.constants;

public class Error {
	public static final String ERROR_CONEXION = "Error de conexi�n. Int�ntelo de nuevo m�s tarde.";
	public static final String ERROR_BUSQUEDA = "Ingrese al menos un criterio de b�squeda.";
	public static final String ERROR_RESULTADOS = "No existen resultados para esta b�squeda.";
	public static final String ERROR_BUSQUEDA_USERS = "No existen usuarios para este supervisor";
}
