package org.addcel.prosa.constants;

public class Url {

	public static final String BASE_URL = "http://www.mobilecard.mx:8080/ProsaServicios/";
	
	public static final String LOGIN = BASE_URL + "login";
	public static final String GET_TIENDAS = BASE_URL + "getTiendas";
	public static final String GET_ROLES = BASE_URL + "getRoles";
	public static final String GET_VENDEDOR = BASE_URL + "getVendedor";
	public static final String ADD_USER = BASE_URL + "addUser";
	public static final String DELETE_USER = BASE_URL + "deleteUser";
	public static final String UPDATE_PASS = BASE_URL + "updatePass";
	public static final String UPDATE_PASS_STATUS = BASE_URL + "updatePassStatus";
	public static final String FIND_USER = BASE_URL + "findUser";
	public static final String UPDATE_USER = BASE_URL + "updateUser";
	public static final String GET_TOKEN = BASE_URL + "getToken";
	public static final String PAGO_VISA = BASE_URL +"pago-visa";
	public static final String PAGO_AMEX = BASE_URL + "pago-amex";
	public static final String BUSQUEDA_PAGOS = BASE_URL + "busquedaPagos";
}
