package org.addcel.prosa.constants;

import org.apache.http.message.BasicNameValuePair;

public class Status {

	public static final BasicNameValuePair ACTIVO = new BasicNameValuePair("Activo", "1");
	public static final BasicNameValuePair CAMBIO = new BasicNameValuePair("Cambio Contraseľa", "2");
	public static final BasicNameValuePair BLOQUEADO = new BasicNameValuePair("Bloqueado", "3");
	public static final BasicNameValuePair BORRADO = new BasicNameValuePair("Borrado", "4");
}
