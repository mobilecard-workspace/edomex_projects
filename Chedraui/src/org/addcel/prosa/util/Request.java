package org.addcel.prosa.util;

import java.util.Date;

import org.addcel.crypto.AddcelCrypto;
import org.addcel.prosa.dto.Pago;
import org.addcel.prosa.dto.Registro;
import org.addcel.prosa.dto.PasswordUpdater;
import org.addcel.prosa.dto.Usuario;
import org.addcel.session.SessionManager;
import org.addcel.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class Request {
	
	private static final String LOG = "Request";
	
	public static String LoginRequest(String login, String password, int proveedor) {
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("password", password);
			json.put("idProveedor", proveedor);
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String getTiendasRequest(String proveedor) {
		JSONObject json = new JSONObject();
		
		try {
			json.put("proveedor", proveedor);
			
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String getRolesRequest(String proveedor) {
		JSONObject json = new JSONObject();
		
		try {
			json.put("id_aplicacion", "1");
			json.put("id_proveedor", proveedor);
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String getVendedorRequest(int idSupervisor, int idTienda, int idProveedor) {
		JSONObject json = new JSONObject();
		
		try {
			json.put("idTienda", idTienda);
			json.put("idSupervisor", idSupervisor);
			json.put("idProveedor", idProveedor);
//			{"idTienda":2,"idProveedor":20,"idSupervisor":1}
		} catch (JSONException e) {
			return "";
		}
		
		Log.d(LOG, "getVendedor Request: " + json.toString());
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String addUserRequest(Registro registro) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("nombres", registro.getNombre());
			json.put("apellidoP", registro.getApellidoP());
			json.put("apellidoM", registro.getApellidoM());
			json.put("email", registro.getEmail());
			json.put("telefono", registro.getTelefono());
			json.put("idTienda", registro.getIdTienda());
			json.put("idProveedor", registro.getIdProveedor());
			json.put("idSupervisor", registro.getIdSupervisor());
			json.put("login", registro.getLogin());
			json.put("idRole", registro.getIdRole());
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String updatePassRequest(PasswordUpdater updater) {

		return AddcelCrypto.encryptHard(updater.toJSON().toString());
	}
	
	public static String findUserRequest(String nombres, String paterno, String materno, String login, int idProveedor) {
		JSONObject json = new JSONObject();
		
		try {
			
			json.put("idProveedor", idProveedor);
			
			if (nombres != "")
				json.put("nombres", nombres);
			
			if (paterno != "")
				json.put("paterno", paterno);
			
			if (materno != "")
				json.put("materno", materno);
			
			if (login != "") 
				json.put("login", login);
			
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String updateUserRequest(Usuario usuario) {
		
		/*{
		 * 	"idUser":1,
		 * 	"nombre":"nombre completo actualizado",
		 * 	"apellidoP":"apellido paterno act",
		 * 	"apellidoM":"apellido materno act",
		 * 	"email":null,"telefono":"1234567899",
		 * 	"idProveedor":1,
		 * 	"idTienda":1,
		 * 	"password":null,
		 * 	"lastLogin":null,
		 * 	"fechaAlta":1378332417030,
		 * 	"idSupervisor":1,
		 * 	"login":"login user",
		 * 	"loginCount":null,
		 * 	"idStatus":0,
		 * 	"idRole":1}*/
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("idUser", usuario.getIdUser());
			json.put("nombres", usuario.getNombres());
			json.put("apellidoP", usuario.getApellidoP());
			json.put("apellidoM", usuario.getApellidoM());
			json.put("email", usuario.getEmail());
			json.put("telefono", usuario.getTelefono());
			json.put("idProveedor", usuario.getIdProveedor());
			json.put("idTienda",usuario.getIdTienda());
			json.put("password", usuario.getPassword());
			json.put("lastLogin", usuario.getLastLogin());
			json.put("fechaAlta", usuario.getFechaAlta());
			json.put("idSupervisor", usuario.getIdSupervisor());
			json.put("login", usuario.getLogin());
			json.put("loginCount", usuario.getLoginCount());
			json.put("idStatus", usuario.getIdStatus());
			json.put("idRole", usuario.getIdRole());
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String getTokenRequest(Context _context) {
		SessionManager session = new SessionManager(_context);
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("idProveedor", session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
			json.put("usuario", "userPrueba");
			json.put("password", "passwordPrueba");
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String pagoRequest(Context c, Pago pago) {
		JSONObject json = pago.buildJSON();
		
		try {	
			json.put("imei", DeviceUtils.getIMEI(c));
			json.put("cx", 0);
			json.put("cy", 0);
			json.put("wkey",DeviceUtils.getIMEI(c));
			json.put("modelo", DeviceUtils.getModel());
			json.put("software", DeviceUtils.getSWVersion());
			
			Log.d(LOG, "Request pagoVisa: " + json.toString());
			
		} catch (JSONException e) {
			return "";
		}
			
		String key = Long.toString((new Date()).getTime()).substring(0, 8);
		
		Log.d(LOG, "KEY pago: " + key);
		
		return json.toString();
//				AddcelCrypto.encryptSensitive(key, json.toString());
	}
	
	public static String busquedaPagosRequest(String inicio, String fin, int idSupervisor, int idUser, int idTienda, int proveedor) {
//		{"id_supervisor":1,"id_user":"1","id_tienda":1,"idProveedor":1,"fi ":"2013-10-01","ff":"203-10-07"}

		
		JSONObject json = new JSONObject();
		
		try {
			json.put("fi", inicio);
			json.put("ff", fin);
			json.put("idTienda", idTienda);
			json.put("idProveedor", proveedor);
			json.put("idSupervisor", idSupervisor);
			
			if (0 < idUser) {
				json.put("idUser", idUser);
			}

		} catch (JSONException e) {
			return "";
		}
		
		Log.i(LOG, "Request busquedaPagos: " + json.toString());
		
		return AddcelCrypto.encryptHard(json.toString());
		
	}
	
	public static String deleteUserRequest(int idUser, int idProveedor, int idSupervisor, String login, int idRole) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("idUser", idUser);
			json.put("idProveedor", idProveedor);
			json.put("idSupervisor", idSupervisor);
			json.put("login", login);
			json.put("idRole", idRole);
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
}
