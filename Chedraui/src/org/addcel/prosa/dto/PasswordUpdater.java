package org.addcel.prosa.dto;

import org.json.JSONException;
import org.json.JSONObject;

public class PasswordUpdater {
	
	private int idUser;
	private int idProveedor;
	private int idSupervisor;
	private String login;
	private String password;
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public int getIdSupervisor() {
		return idSupervisor;
	}
	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public JSONObject toJSON() {
		
		JSONObject json = new JSONObject();
		try { 
			json.put("login", login);
			json.put("idUser", idUser);
			json.put("password", password);
			json.put("idProveedor", idProveedor);
			json.put("idSupervisor", idSupervisor);
		} catch (JSONException e) {
			return null;
		}
		
		return json;
	}
	
	
}
