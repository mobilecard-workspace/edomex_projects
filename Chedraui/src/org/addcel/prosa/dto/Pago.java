package org.addcel.prosa.dto;

import org.json.JSONException;
import org.json.JSONObject;

public class Pago {
	
	private String email;
	private String nombres;
	private String descProducto;
	private String tarjeta;
	private String cvv2;
	private String vigencia;
	private double subTotal;
	private double total;
	private int moneda;
	private int tipo;
	private int idUser;
	private int idProveedor;
	private String token;
	
//	Campos amex
	private boolean amex;
	private String direccion;
	private String cp;
	
	/*
	 * {
	 * "email":"prueba@addcel.com",
	 * "nombre":"NombreCOMPLETO",
	 * "descProducto":"pruebamex",
	 * "subTotal":100000,
	 * "total":100012,
	 * "moneda":1,
	 * "tipo":1,
	 * "idUser":54,
	 * "vigencia":"01/15",
	 * "tarjeta":"123455566666666",
	 * "cvv2":"5555",
	 * "idProveedor":"1",
	 * "token":"Je1L2NY2mNHC/aJFleOpIB4V+18r9d3mbo1PK6MTiz8=",
	 * }
	 * */
	
	public Pago() {
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDescProducto() {
		return descProducto;
	}

	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getMoneda() {
		return moneda;
	}

	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public boolean isAmex() {
		return amex;
	}

	public void setAmex(boolean amex) {
		this.amex = amex;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public JSONObject buildJSON() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("email", email);
			json.put("nombres", nombres);
			json.put("descProducto", descProducto);
			json.put("tarjeta", tarjeta);
			json.put("cvv2", cvv2);
			json.put("vigencia", vigencia);
			json.put("subTotal", subTotal);
			json.put("total", total);
			json.put("moneda", moneda);
			json.put("tipo", tipo);
			json.put("idUser", idUser);
			json.put("idProveedor", idProveedor);
//			json.put("token", token);
			
			if (isAmex()) {
				json.put("direccion", direccion);
				json.put("cp",cp);
			}
			
		} catch (JSONException e) {
			return null;
		}
		
		return json;
	}
}
