package org.addcel.prosa.dto;

public class Modulo {
	
	private int idModule;
	private String descripcion;
	
	public Modulo() {
		
	}
	
	public Modulo(int idModule, String descripcion) {
		this.idModule = idModule;
		this.descripcion = descripcion;
	}

	public int getIdModule() {
		return idModule;
	}

	public void setIdModule(int idModule) {
		this.idModule = idModule;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}
	
	
}
