package org.addcel.prosa.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class Venta implements Parcelable{
	
	private String referencia;
	private String nombre;
	private String fecha;
	private String descProducto;
	private String total;
	private String moneda;
	
	public Venta() {
		
	}
	
	public Venta(Parcel in) {
		readFromParcel(in);
	}
	
	public String getReferencia() {
		return referencia;
	}
	
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getDescProducto() {
		return descProducto;
	}
	
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	
	public String getTotal() {
		return total;
	}
	
	public void setTotal(String total) {
		this.total = total;
	}
	
	public String getMoneda() {
		return moneda;
	}
	
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getDescProducto() + " - " + getTotal();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(total);
		dest.writeString(descProducto);
		dest.writeString(fecha);
		dest.writeString(moneda);
		dest.writeString(nombre);
		dest.writeString(referencia);
	}
	
	public void readFromParcel(Parcel in) {
		total = in.readString();
		descProducto = in.readString();
		fecha = in.readString();
		moneda = in.readString();
		nombre = in.readString();
		referencia = in.readString();
	}
	
	public static final Parcelable.Creator<Venta> CREATOR = new Creator<Venta>() {
		
		@Override
		public Venta[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Venta[size];
		}
		
		@Override
		public Venta createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Venta(source);
		}
	};
	
	
}
