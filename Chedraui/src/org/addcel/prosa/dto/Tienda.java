package org.addcel.prosa.dto;

public class Tienda {
	
	private int idTienda;
	private String descripcion;
	private String idProveedor;
	
	public Tienda() {
		
	}
	
	public Tienda(int idTienda, String descripcion, String idProveedor) {
		this.idTienda = idTienda;
		this.descripcion = descripcion;
		this.idProveedor = idProveedor;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}
	
	
}
