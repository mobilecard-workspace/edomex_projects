package org.addcel.prosa.dto;

public class Rol {
	
	private int idRol;
	private String maxLogin;
	private int idAplicacion;
	private String descripcion;
	
	public static int ADMIN = 1;
	public static int VENDEDOR = 2;
	
	public Rol() {
		
	}
	
	public Rol(int idRol, String maxLogin, int idAplication, String descripcion) {
		this.idRol = idRol;
		this.maxLogin = maxLogin;
		this.idAplicacion = idAplication;
		this.descripcion = descripcion;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getMaxLogin() {
		return maxLogin;
	}

	public void setMaxLogin(String maxLogin) {
		this.maxLogin = maxLogin;
	}

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}
}
