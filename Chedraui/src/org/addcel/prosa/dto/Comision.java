package org.addcel.prosa.dto;

public class Comision {
	
	private double minCom;
	private double comisionPorcentaje;
	private double minComPorcentaje;
	private double comision;
	
	public Comision() {
		
	}

	public double getMinCom() {
		return minCom;
	}

	public void setMinCom(double minCom) {
		this.minCom = minCom;
	}

	public double getComisionPorcentaje() {
		return comisionPorcentaje;
	}

	public void setComisionPorcentaje(double comisionPorcentaje) {
		this.comisionPorcentaje = comisionPorcentaje;
	}

	public double getMinComPorcentaje() {
		return minComPorcentaje;
	}

	public void setMinComPorcentaje(double minComPorcentaje) {
		this.minComPorcentaje = minComPorcentaje;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}
	
	
}
