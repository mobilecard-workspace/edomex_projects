package org.addcel.prosa.dto;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Usuario implements Parcelable {
	
	private int idUser;
	private String nombres;
	private String apellidoP;
	private String apellidoM;
	private String email;
	private String telefono;
	private int idProveedor;
	private int idTienda;
	private String password;
	private long lastLogin;
	private long fechaAlta;
	private int idSupervisor;
	private String login;
	private int loginCount;
	private int idStatus;
	private int idRole;
	private String descTienda;
	private String descStatus;
	private String descRole;
	
	public Usuario() {
		
	}
	
	public Usuario(Parcel in) {
		readFromParcel(in);
	}
	
	

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoP() {
		return apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}

	public String getApellidoM() {
		return apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public long getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(long fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public int getIdSupervisor() {
		return idSupervisor;
	}

	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public int getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}

	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public String getDescTienda() {
		return descTienda;
	}

	public void setDescTienda(String descTienda) {
		this.descTienda = descTienda;
	}

	public String getDescStatus() {
		return descStatus;
	}

	public void setDescStatus(String descStatus) {
		this.descStatus = descStatus;
	}

	public String getDescRole() {
		return descRole;
	}

	public void setDescRole(String descRole) {
		this.descRole = descRole;
	}
	
	public static Usuario fromJson(JSONObject j) {
		
		Usuario u = new Usuario();
			
			u.setDescRole(j.optString("desc_role"));
			u.setIdStatus(j.optInt("idStatus"));
			u.setApellidoP(j.optString("apellidoP"));
			u.setTelefono(j.optString("telefono"));
			u.setIdRole(j.optInt("idRole"));
			u.setLoginCount(j.optInt("loginCount"));
			u.setIdTienda(j.optInt("idTienda"));
			u.setPassword(j.optString("password"));
			u.setIdSupervisor(j.optInt("idSupervisor"));
			u.setLastLogin(j.optLong("lastLogin"));
			u.setNombres(j.optString("nombres"));
			u.setEmail(j.optString("email"));
			u.setDescTienda(j.optString("desc_tienda"));
			u.setIdProveedor(j.optInt("idProveedor"));
			u.setDescStatus(j.optString("desc_status"));
			u.setApellidoM(j.optString("apellidoM"));
			u.setIdUser(j.optInt("idUser"));
			u.setLogin(j.optString("login"));
			u.setFechaAlta(j.optLong("fechaAlta"));
		
		return u;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getNombres() + " " + getApellidoP() + " " + getApellidoM();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		arg0.writeInt(idUser);
		arg0.writeString(nombres);
		arg0.writeString(apellidoP);
		arg0.writeString(apellidoM);
		arg0.writeString(email);
		arg0.writeString(telefono);
		arg0.writeInt(idProveedor);
		arg0.writeInt(idTienda);
		arg0.writeString(password);
		arg0.writeLong(lastLogin);
		arg0.writeLong(fechaAlta);
		arg0.writeInt(idSupervisor);
		arg0.writeString(login);
		arg0.writeInt(loginCount);
		arg0.writeInt(idStatus);
		arg0.writeInt(idRole);
		arg0.writeString(descTienda);
		arg0.writeString(descStatus);
		arg0.writeString(descRole);
	}
	
	public void readFromParcel(Parcel in) {
		idUser = in.readInt();
		nombres = in.readString();
		apellidoP = in.readString();
		apellidoM = in.readString();
		email = in.readString();
		telefono = in.readString();
		idProveedor = in.readInt();
		idTienda = in.readInt();
		password = in.readString();
		lastLogin = in.readLong();
		fechaAlta = in.readLong();
		idSupervisor = in.readInt();
		login = in.readString();
		loginCount = in.readInt();
		idStatus = in.readInt();
		idRole = in.readInt();
		descTienda = in.readString();
		descStatus = in.readString();
		descRole = in.readString();
	}
	
	public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {

		@Override
		public Usuario createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Usuario(source);
		}

		@Override
		public Usuario[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Usuario[size];
		}
	};

}
