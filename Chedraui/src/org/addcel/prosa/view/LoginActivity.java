package org.addcel.prosa.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Proveedor;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	private static final String LOG = "LoginActivity";
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		session = new SessionManager(LoginActivity.this);
		
		findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String login = ((EditText) findViewById(R.id.edit_login)).getText().toString().trim();
				String pass = ((EditText) findViewById(R.id.edit_pass)).getText().toString().trim();
				
				String request = Request.LoginRequest(login, pass, Proveedor.PROSA);
					
				new WebServiceClient(new LoginListener(), LoginActivity.this, true, Url.LOGIN).execute(request);
				
				
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		((EditText) findViewById(R.id.edit_login)).setText("");
		((EditText) findViewById(R.id.edit_pass)).setText("");
		
	}
	
	private class LoginListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {	
					JSONObject json = new JSONObject(response);
					Log.i(LOG, json.toString(1));
					
					switch (json.getInt("idError")) {
						case 0:
							JSONObject tienda = json.getJSONObject("tienda");
							JSONObject comisiones = json.getJSONObject("comisiones");
							
							String idTienda = String.valueOf(tienda.getInt("idTienda"));
							String descripcion = tienda.getString("descripcion");
							String idProveedor = tienda.getString("idProveedor");
							
							String comision = String.valueOf(comisiones.getDouble("comision"));
							String comisionPorcentaje = String.valueOf(comisiones.getDouble("comisionPorcentaje"));
							String minComPorcentaje = String.valueOf(comisiones.getDouble("minComPorcentaje"));
							String minCom = String.valueOf(comisiones.getDouble("minCom"));
							
							String tipoCambio = String.valueOf(json.getDouble("tipoCambio"));
							String idUser = String.valueOf(json.getInt("idUser"));
							String rol = String.valueOf(json.getInt("idRol"));
							
							String login = ((EditText) findViewById(R.id.edit_login)).getText().toString().trim();
							String pass = ((EditText) findViewById(R.id.edit_pass)).getText().toString().trim();
							String idSupervisor = String.valueOf(json.getInt("idSupervisor"));
							String nombres = json.getString("nombres");
							
							session.createLoginSession(idUser, login, pass, idTienda, descripcion, idProveedor, comision, comisionPorcentaje, minComPorcentaje, minCom, 
									tipoCambio, rol, idSupervisor, nombres);
							

							JSONArray modulos = json.getJSONArray("modulos");
							
							String modulosExtra = modulos.toString();
							
							Intent i = new Intent(LoginActivity.this, MenuActivity.class);
							i.putExtra("modulos", modulosExtra);
							startActivity(i);
														
							break;
							
						case 2:
							Toast.makeText(LoginActivity.this, json.optString("mensajeError"), Toast.LENGTH_SHORT);
							int id = json.optInt("idUser");
							
							Intent intent = new Intent(LoginActivity.this, CambioPasswordActivity.class);
							intent.putExtra("login", true);
							intent.putExtra("idUser", id);
							intent.putExtra("nombre_usuario", ((EditText) findViewById(R.id.edit_login)).getText().toString().trim());
							startActivity(intent);
							
	
						default:
							Toast.makeText(getApplicationContext(), json.getString("mensajeError"), Toast.LENGTH_LONG).show();
							break;
					}
					
				} catch (JSONException e) {
					Toast.makeText(getApplicationContext(), "Error de conexi�n. Intente de nuevo m�s tarde.",  Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(getApplicationContext(), "Error de conexi�n. Intente de nuevo m�s tarde.",  Toast.LENGTH_SHORT).show();
			}
		}
		
	}

}
