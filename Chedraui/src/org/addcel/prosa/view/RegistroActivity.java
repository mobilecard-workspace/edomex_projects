package org.addcel.prosa.view;

import java.util.LinkedList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.Registro;
import org.addcel.prosa.dto.Rol;
import org.addcel.prosa.dto.Tienda;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegistroActivity extends Activity {
	
	SessionManager session;
	private static final String LOG = "RegistroActivity";
	private List<Tienda> tiendas;
	private List<Rol> roles;
	private Spinner tiendaSpinner;
	private Spinner rolesSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		
		session = new SessionManager(RegistroActivity.this);
		
		((TextView) findViewById(R.id.text_header)).setText("Registro");
		
		final EditText login = (EditText) findViewById(R.id.edit_login);
		final EditText nombre = (EditText) findViewById(R.id.edit_nombre);
		final EditText paterno = (EditText) findViewById(R.id.edit_paterno);
		final EditText materno = (EditText) findViewById(R.id.edit_materno);
		final EditText celular = (EditText) findViewById(R.id.edit_celular);
		final EditText correo = (EditText) findViewById(R.id.edit_correo);
		
		String param = Request.getTiendasRequest(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
		
		findViewById(R.id.button_registrar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (validate()) {
					
					Registro r = new Registro();
					r.setLogin(login.getText().toString().trim());
					r.setNombre(nombre.getText().toString().trim());
					r.setApellidoP(paterno.getText().toString().trim());
					r.setApellidoM(materno.getText().toString().trim());
					r.setTelefono(celular.getText().toString().trim());
					r.setEmail(correo.getText().toString().trim());
					r.setIdProveedor(Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR)));
					r.setIdRole(((Rol) getRolesSpinner().getSelectedItem()).getIdRol());
					r.setIdSupervisor(Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID)));
					r.setIdTienda(((Tienda) getTiendaSpinner().getSelectedItem()).getIdTienda());
					
					String request = Request.addUserRequest(r);
					
					new WebServiceClient(new AddUserListener() , RegistroActivity.this, true, Url.ADD_USER).execute(request);
				}
			}
		});
		
		if (!"".equals(param)) {
			new WebServiceClient(new TiendaListener(), RegistroActivity.this, false, Url.GET_TIENDAS).execute(param);
		} else {
			Toast.makeText(RegistroActivity.this, "Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
		}
		
	}
		
	public List<Tienda> getTiendas() {
		if (null == tiendas) {
			tiendas = new LinkedList<Tienda>();
		}
		
		return tiendas;
	}
	
	public Spinner getTiendaSpinner() {
		if (null == tiendaSpinner) {
			tiendaSpinner = (Spinner) findViewById(R.id.spinner_tienda);
		}
		
		return tiendaSpinner;
	}
	
	public List<Rol> getRoles() {
		if (null == roles) {
			roles = new LinkedList<Rol>();
		}
		
		return roles;
	}
	
	public Spinner getRolesSpinner() {
		if (null == rolesSpinner) {
			rolesSpinner = (Spinner) findViewById(R.id.spinner_rol);
		}
		
		return rolesSpinner;
	}
	
	private boolean validate() {
		return true;
	}
	
	private class TiendaListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						for (int i = 0; i < array.length(); i++) {
							JSONObject json = array.getJSONObject(i);
							Tienda t = new Tienda(json.optInt("idTienda"), json.optString("descripcion"), json.optString("proveedor"));
							getTiendas().add(t);
						}
						getTiendaSpinner().setAdapter(AppUtils.createTiendaAdapter(RegistroActivity.this, getTiendas()));
						
						String param = Request.getRolesRequest(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
						new WebServiceClient(new RolListener(), RegistroActivity.this, true, Url.GET_ROLES).execute(param);
						
					} else {
						Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
					}
					
				} catch (JSONException e) {
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class RolListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						for (int i = 0; i < array.length(); i++) {
							JSONObject json = array.getJSONObject(i);
							Rol r = new Rol(json.optInt("idRol"), json.optString("maxLogin"), json.optInt("idAplication"), json.optString("descripcion"));
							getRoles().add(r);
						}
						
						getRolesSpinner().setAdapter(AppUtils.createRolAdapter(RegistroActivity.this, getRoles()));
						
					} else {
						Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
					}
					
				} catch (JSONException e) {
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class AddUserListener implements WSResponseListener {
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				Log.d(LOG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					switch (json.optInt("idError", -1)) {
						case 0:
							Toast.makeText(RegistroActivity.this, json.optString("mensajeError"), Toast.LENGTH_SHORT).show();
							finish();
							break;
	
						default:
							Toast.makeText(RegistroActivity.this, json.optString("mensajeError", "Error de conexi�n. Int�ntelo de nuevo m�s tarde."), Toast.LENGTH_SHORT).show();
							break;
						}
				} catch (JSONException e) {
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Int�ntelo de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				}
				
				/*
				 * {"idError":0,"mensajeError":"Usuario guardado correctamente"}
			 	*/
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Int�ntelo de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
			}
		}
	}

}
