package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.prosa.adapter.ButtonListAdapter;
import org.addcel.prosa.constants.IdModulo;
import org.addcel.prosa.dto.Modulo;
import org.addcel.chedraui.R;
import org.addcel.session.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends Activity {
	
	private String modulosString;
	private ListView botonesList;
	private List<Modulo> modulos;
	SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		session = new SessionManager(MenuActivity.this);
		
		((TextView) findViewById(R.id.text_nombre)).setText(session.getUserDetails().get(SessionManager.NOMBRES));
		
		getBotonesList().setAdapter(new ButtonListAdapter(MenuActivity.this, getModulos()));
		getBotonesList().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Modulo  mod = (Modulo) ((ButtonListAdapter) arg0.getAdapter()).getItem(arg2);
				
				switch (mod.getIdModule()) {
					case IdModulo.REGISTRO:
						startActivity(new Intent(MenuActivity.this, RegistroActivity.class));
						break;
					
					case IdModulo.BUSQUEDA:
						startActivity(new Intent(MenuActivity.this, BusquedaActivity.class));
						break;
					
					case IdModulo.CONSULTAS:
						Intent consultaAdmin = new Intent(MenuActivity.this, ConsultaActivity.class);
						startActivity(consultaAdmin);
						break;
					
					case IdModulo.CAMBIO_PASS:
						startActivity(new Intent(MenuActivity.this, CambioPasswordActivity.class));
						break;
					
					case IdModulo.PAGO_VISA:
						startActivity(new Intent(MenuActivity.this, PagoVISAActivity.class));
						break;
					
					case IdModulo.PAGO_AMEX:
						startActivity(new Intent(MenuActivity.this, PagoAmexActivity.class));
						break;
					
					case IdModulo.CONSULTA_VENTAS:
						Intent consultaVendedor = new Intent(MenuActivity.this, ConsultaActivity.class);
						startActivity(consultaVendedor);
						break;
					
					default:
						Toast.makeText(getApplicationContext(), "M�dulo no disponible", Toast.LENGTH_SHORT).show();
						break;
				}
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_logout:
			session.logoutUser();
			finish();
			break;

		default:
			break;
		}
		
		return true;
	}
	
	public String getModulosString() {
		if (null == modulosString) {
			modulosString = getIntent().getStringExtra("modulos");
		}
		
		return modulosString;
	}
	
	public List<Modulo> getModulos() {
		if (null == modulos) {
			try {
				JSONArray modulosArray = new JSONArray(getModulosString());
				
				if (0 < modulosArray.length()) {
					
					modulos = new ArrayList<Modulo>(modulosArray.length());
					
					for (int i = 0; i < modulosArray.length(); i++) {
						JSONObject moduloJSON = modulosArray.getJSONObject(i);
						modulos.add(new Modulo(moduloJSON.getInt("idModule"), moduloJSON.getString("descripcion")));
					}
					
				} else {
					modulos = new ArrayList<Modulo>();
				}
								
			} catch (JSONException e) {
				modulos = new ArrayList<Modulo>();
			}
		}
		
		return modulos;
	}
	
	public ListView getBotonesList() {
		if (null == botonesList) {
			botonesList = (ListView) findViewById(R.id.list_botones);
		}
		
		return botonesList;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		session.logoutUser();
		finish();
	}
}
