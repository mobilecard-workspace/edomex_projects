package org.addcel.prosa.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.Pago;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class PagoAmexActivity extends Activity {
	
	SessionManager session;
	
	private Pago pago;
	
	private EditText nombreEdit,correoEdit, descEdit, tarjetaEdit, cvvEdit, montoEdit, direccionEdit, cpEdit;
	private Spinner mesSpinner, anioSpinner;

	private double monto;
	private double comision;
	private double total;
	
	private boolean porcentaje;
	
	private static final String LOG = "PagoAmexActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pago_amex);
		
		session = new SessionManager(PagoAmexActivity.this);
		
		nombreEdit = (EditText) findViewById(R.id.edit_nombre);
		correoEdit = (EditText) findViewById(R.id.edit_mail);
		descEdit = (EditText) findViewById(R.id.edit_desc);
		tarjetaEdit = (EditText) findViewById(R.id.edit_tarjeta);
		cvvEdit =(EditText) findViewById(R.id.edit_cvv);
		montoEdit = (EditText) findViewById(R.id.edit_monto);
		direccionEdit = (EditText) findViewById(R.id.edit_direccion);
		cpEdit = (EditText) findViewById(R.id.edit_cp);
		
		montoEdit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
				if (s.toString().length() > 0) {
					monto = Double.parseDouble(s.toString());
				} else {
					monto = 0;
				}
				
				if (monto > 1000) {
					porcentaje = true;
					comision = Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION_PCT)) * monto;
					((TextView) findViewById(R.id.text_comision)).setText(Text.formatCurrency(comision, true));
					
				} else {
					porcentaje = false;
					comision = Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION));
					((TextView) findViewById(R.id.text_comision)).setText(Text.formatCurrency(comision, true));
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			} 
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().length() > 0) {
					total = monto + comision;
					((TextView) findViewById(R.id.text_total)).setText(Text.formatCurrency(total, true));
				}
			}
		});
		
		findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		findViewById(R.id.button_pago).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String request = Request.getTokenRequest(PagoAmexActivity.this);
				new WebServiceClient(new TokenListener(), PagoAmexActivity.this, true, Url.GET_TOKEN).execute(request);	
			}
		});
		
		comision = Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION));
		((TextView) findViewById(R.id.text_comision)).setText(Text.formatCurrency(comision, true));
		
		getMesSpinner();
		getAnioSpinner();
	}
	

	
	public Spinner getMesSpinner() {
		if (null == mesSpinner) {
			mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.mes, android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mesSpinner.setAdapter(adapter);
		}
		return mesSpinner;
	}
	
	public Spinner getAnioSpinner() {
		if (null == anioSpinner) {
			anioSpinner = (Spinner) findViewById(R.id.spinner_anio);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, AppUtils.getAniosVigencia());
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			anioSpinner.setAdapter(adapter);
		}
		return anioSpinner;
	}
	
	private boolean validate() {
		
		String nombre = nombreEdit.getText().toString().trim();
		String correo = correoEdit.getText().toString().trim();
		String desc = descEdit.getText().toString().trim();
		String tarjeta = tarjetaEdit.getText().toString().trim();
		String cvv = cvvEdit.getText().toString().trim();
		String monto = montoEdit.getText().toString().trim();
		String direccion = direccionEdit.getText().toString().trim();
		String cp = cpEdit.getText().toString().trim();
		
		if ("".equals(nombre)) {
			nombreEdit.requestFocus();
			nombreEdit.setError("El campo nombre no puede ir vac�o.");
			return false;
		}
		
	
		if ("".equals(correo)) {
			correoEdit.requestFocus();
			correoEdit.setError("El campo correo no puede ir vac�o.");
			return false;
		}
		
		if (!Validador.esCorreo(correo)) {
			correoEdit.requestFocus();
			correoEdit.setError("El correo introducido no es v�lido.");
			return false;
		}
		
		if ("".equals(desc)) {
			descEdit.requestFocus();
			descEdit.setError("El campo descripci�n no puede ir vac�o.");
			return false;
		}
		
		if ("".equals(tarjeta)) {
			tarjetaEdit.requestFocus();
			tarjetaEdit.setError("El campo tarjeta no puede ir vac�o.");
			return false;
		}
		
		if (!Validador.esTarjetaDeCreditoAmericanExpress(tarjeta)) {
			tarjetaEdit.requestFocus();
			tarjetaEdit.setError("La tarjeta introducida no es v�lida.");
			return false;
		}
		
		if ("".equals(cvv)) {
			cvvEdit.requestFocus();
			cvvEdit.setError("El campo tarjeta no puede ir vac�o.");
			return false;
		}
		
		if (3 > cvv.length()) {
			cvvEdit.requestFocus();
			cvvEdit.setError("El campo cvv debe tener 3 o 4 caracteres.");
			return false;
		}
		
		if ("".equals(cp)) {
			cpEdit.requestFocus();
			cpEdit.setError("El campo c�digo postal no puede ir vac�o.");
			return false;
		}
		
		if ("".equals(direccion)) {
			direccionEdit.requestFocus();
			direccionEdit.setError("El campo direcci�n no puede ir vac�o.");
			return false;
		}
		
		if ("".equals(monto)) {
			montoEdit.requestFocus();
			montoEdit.setError("El campo monto no puede ir vac�o");
			return false;
		}
		
		return true;
	}
	
	private class TokenListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					String token = json.getString("token");
					String mes = (String) mesSpinner.getSelectedItem();
					String anio = (String) anioSpinner.getSelectedItem();
					
					Pago pago = new Pago();
					pago.setDescProducto(descEdit.getText().toString().trim());
					pago.setEmail(correoEdit.getText().toString().trim());
					pago.setNombres(nombreEdit.getText().toString().trim());
					pago.setTarjeta(tarjetaEdit.getText().toString().trim());
					pago.setCvv2(cvvEdit.getText().toString().trim());
					pago.setVigencia(mes + "/" + anio.substring(2, 4));
					pago.setIdProveedor(Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR)));
					
					Log.d(LOG, "ID Proveedor: " + pago.getIdProveedor());
					pago.setIdUser(Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID)));
					
					
					pago.setMoneda(1);
					pago.setSubTotal(monto);
					pago.setTipo(1);
					pago.setToken(token);
					pago.setTotal(total);
					
					pago.setAmex(true);
					pago.setDireccion(direccionEdit.getText().toString().trim());
					pago.setCp(cpEdit.getText().toString().trim());
					
					String request = Request.pagoRequest(PagoAmexActivity.this, pago);
					
					new WebServiceClient(new PagoListener(), PagoAmexActivity.this, true, Url.PAGO_AMEX).execute(request);

					
				} catch (JSONException e) {
					Toast.makeText(PagoAmexActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(PagoAmexActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}
	
	private class PagoListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			
			if (null != response && !response.equals("")) {
				response = AddcelCrypto.decryptSensitive(response);
				try {
					JSONObject json = new JSONObject(response);
					

					Intent i = new Intent(PagoAmexActivity.this, DetalleActivity.class);
					i.putExtra("pago_response", json.toString());
					
					if (0 == json.optInt("idError", -1)) {
						i.putExtra("subtotal", monto);
						i.putExtra("total", total);
						startActivity(i);
						finish();
					} else {
						startActivity(i);
					}
					
				} catch (JSONException e) {
					Toast.makeText(PagoAmexActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(PagoAmexActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
			
		}
	}
}
