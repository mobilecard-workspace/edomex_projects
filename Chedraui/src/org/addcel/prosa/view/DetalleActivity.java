package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.addcel.print.MiniPrinterFunctions;
import org.addcel.print.PrinterFunctions;
import org.addcel.chedraui.R;
import org.addcel.prosa.dto.Venta;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleActivity extends Activity {
	
	private JSONObject pagoJSON;
	private double subtotal, total;
	private TextView header, autorizacion, folio, monto, totalText, mensaje;
	private Venta venta;
	
	private final static int REQUEST_ENABLE_BT = 1;
	private static final String LOG = "DetalleActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle);
		
		header = (TextView) findViewById(R.id.header);
		autorizacion = (TextView) findViewById(R.id.autorizacion);
		folio = (TextView) findViewById(R.id.folio);
		monto = (TextView) findViewById(R.id.monto);
		totalText = (TextView) findViewById(R.id.total);
		mensaje = (TextView) findViewById(R.id.mensaje);
		
		header.setText(getPagoJSON().optString("mensajeError"));
		folio.setText(String.valueOf(getPagoJSON().optInt("referencia")));
		
		findViewById(R.id.button_imprimir).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setBluetooth();
				}
		});
		findViewById(R.id.button_finalizar).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		if (getPagoJSON().optInt("idError") == 0) {
			
			subtotal = getIntent().getDoubleExtra("subtotal", 0);
			total = getIntent().getDoubleExtra("total", 0);
			
			autorizacion.setText(String.valueOf(getPagoJSON().optLong("autorizacion")));
			monto.setText(Text.formatCurrency(subtotal, true));
			totalText.setText(Text.formatCurrency(total, true));
			mensaje.setText("Gracias por tu compra.");
			
			venta = new Venta();
			venta.setDescProducto("Prueba");
			venta.setReferencia(String.valueOf(getPagoJSON().optLong("autorizacion")));
			venta.setTotal(Double.toString(total));
			venta.setFecha(Text.formatDateForShowing(new Date()));
			
			findViewById(R.id.layout_autorizacion).setVisibility(View.VISIBLE);
			findViewById(R.id.layout_monto).setVisibility(View.VISIBLE);
			findViewById(R.id.layout_total).setVisibility(View.VISIBLE);
			
		} else {
			((Button) findViewById(R.id.button_imprimir)).setVisibility(View.GONE);
			((Button) findViewById(R.id.button_finalizar)).setText("Regresar");
			mensaje.setText("Presiona atr�s para intentar efectuar el pago.");
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (getPagoJSON().optInt("idError") == 0) {
			finish();
		} else {
			super.onBackPressed();
		}
	}
	
	public JSONObject getPagoJSON() {
		
		if (null == pagoJSON) {

			String pagoString = getIntent().getStringExtra("pago_response");
			
			if (null != pagoString && !"".equals(pagoString)) {
				try {
					pagoJSON = new JSONObject(pagoString);
				} catch (JSONException e) {
					return pagoJSON;
				}
			}
		}
		
		return pagoJSON;
	}
	
	public void setBluetooth() {

		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
			Toast.makeText(DetalleActivity.this, "Bluetooth no disponible en este dispositivo", Toast.LENGTH_SHORT).show();
		} else {
			
			if (!bluetoothAdapter.isEnabled()) {
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBluetooth, REQUEST_ENABLE_BT);
			}
			
			Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
			
			BluetoothDevice device = null;
			if (0 < pairedDevices.size()) {
				for (BluetoothDevice bluetoothDevice : pairedDevices) {
					Log.i(LOG, "Device: " + bluetoothDevice.getName());
					if (bluetoothDevice.getName().contains("Star")||bluetoothDevice.getName().contains("STAR")||bluetoothDevice.getName().contains("star")) {
						device = bluetoothDevice;
					}
				}
				
				if(null != device)
				
					try {
						MiniPrinterFunctions.PrintSampleReceipt(DetalleActivity.this, "BT:"+device.getName(), "mini", "2inch (58mm)", venta);
					} catch (Exception e) {
						PrinterFunctions.PrintSampleReceipt(DetalleActivity.this, "BT:"+device.getName(), "", "line", getResources(), "3inch (78mm)", venta);
					}
			}
		}
	}
	

}
