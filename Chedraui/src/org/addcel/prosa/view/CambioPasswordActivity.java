package org.addcel.prosa.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.PasswordUpdater;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CambioPasswordActivity extends Activity {
	
	private int idUsuario;
	private String nombreUsuario;
	private int idSupervisor;
	private int proveedor;
	private boolean login;
	private boolean supervisor;
	private EditText password, confPassword;
	
	private static final String LOG = "CambioPasswordActivity";
	
	SessionManager session;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cambio_password);
		
		session = new SessionManager(CambioPasswordActivity.this);
		login = getIntent().getBooleanExtra("login", false);
		Log.d(LOG, String.valueOf(login));
		supervisor = getIntent().getBooleanExtra("supervisor", false);
		
		if (login || supervisor) {
			idUsuario = getIntent().getIntExtra("idUser", 0);
			nombreUsuario = getIntent().getStringExtra("nombre_usuario");
			proveedor = 20;
		} else {
			idUsuario = Integer.valueOf(session.getUserDetails().get(SessionManager.USR_ID));
			idSupervisor = Integer.valueOf(session.getUserDetails().get(SessionManager.ID_SUPERVISOR));
			nombreUsuario = session.getUserDetails().get(SessionManager.USR_LOGIN);
			proveedor = Integer.valueOf(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
			
		} 
		
		
		Log.d(LOG, "ID USUARIO: " + idUsuario);
		
		password = (EditText) findViewById(R.id.edit_pass);
		confPassword = (EditText) findViewById(R.id.edit_pass_nuevo);
		
		findViewById(R.id.button_cambio).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validate()) {
					
					PasswordUpdater updater = new PasswordUpdater();
					
					updater.setIdProveedor(proveedor);
					
					if (supervisor) {
						updater.setIdSupervisor(Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID)));
					} else if (login) {
						updater.setIdSupervisor(idUsuario);
					} else {
						updater.setIdSupervisor(Integer.parseInt(session.getUserDetails().get(SessionManager.ID_SUPERVISOR)));
					}
					
					updater.setIdUser(idUsuario);
					updater.setLogin(nombreUsuario);
					updater.setPassword(password.getText().toString().trim());
					
					String request = Request.updatePassRequest(updater);
					
					if (login) {
						Log.d(LOG, "Url cambio password /status: " +  Url.UPDATE_PASS_STATUS);
						new WebServiceClient(new UpdatePassListener(), CambioPasswordActivity.this, true, Url.UPDATE_PASS_STATUS).execute(request);
					} else {
						new WebServiceClient(new UpdatePassListener(), CambioPasswordActivity.this, true, Url.UPDATE_PASS).execute(request);
					}
				}
			}
		});
	}
	
	private boolean validate() {
		
		String passwordString = password.getText().toString().trim();
		String passwordConfString = confPassword.getText().toString().trim();
		
		if ("".equals(passwordString)) {
			password.requestFocus();
			password.setError("Introduzca password.");
			return false;
		}
		
		if (!passwordString.equals(passwordConfString)) {
			confPassword.requestFocus();
			confPassword.setError("Confirmaci�n de password no coincide.");
			return false;
		}
		
		return true;
	}
	
	private class UpdatePassListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					int idError = json.optInt("idError");
					String mensajeError = json.optString("mensajeError", Error.ERROR_CONEXION);
					
					switch (idError) {
						case 0:
							Toast.makeText(CambioPasswordActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							finish();
							break;
	
						default:
							Toast.makeText(CambioPasswordActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							break;
					}
					
				} catch (JSONException e) {
					Toast.makeText(CambioPasswordActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(CambioPasswordActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
