package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.Rol;
import org.addcel.prosa.dto.Usuario;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

public class ConsultaActivity extends Activity {
	
	private Spinner usuarioSpinner;
	private List<Usuario> usuarios;
	private Date inicioDate;
	private Date terminoDate;
	
	SessionManager session;
	private static final String LOG = "ConsultaActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consulta);
		
		session = new SessionManager(ConsultaActivity.this);
		int rol = Integer.parseInt(session.getUserDetails().get(SessionManager.ROL));
		
		findViewById(R.id.button_inicio).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Calendar c = Calendar.getInstance();
				
				DatePickerDialog d = new DatePickerDialog(ConsultaActivity.this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						// TODO Auto-generated method stub
						
						c.set(year, monthOfYear, dayOfMonth);
						
						setInicioDate(c.getTime());
						((Button) findViewById(R.id.button_inicio)).setText(Text.formatDateForShowing(getInicioDate()));
						
						Calendar termino = Calendar.getInstance();
						termino.setTime(c.getTime());
						termino.add(Calendar.DAY_OF_MONTH, 7);
						setTerminoDate(termino.getTime());
						((Button) findViewById(R.id.button_corte)).setText(Text.formatDateForShowing(getTerminoDate()));
						
					}
				}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				
				d.show();
			}
		});
		
		findViewById(R.id.button_corte).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Calendar c = Calendar.getInstance();
				
				DatePickerDialog d = new DatePickerDialog(ConsultaActivity.this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						// TODO Auto-generated method stub
						
						c.set(year, monthOfYear, dayOfMonth);
						
						setTerminoDate(c.getTime());
						((Button) findViewById(R.id.button_corte)).setText(Text.formatDateForShowing(getTerminoDate()));
						
						Calendar inicio = Calendar.getInstance();
						inicio.setTime(c.getTime());
						inicio.add(Calendar.DAY_OF_MONTH, -7);
						setInicioDate(inicio.getTime());
						((Button) findViewById(R.id.button_inicio)).setText(Text.formatDateForShowing(getInicioDate()));
						
					}
				}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				
				d.show();
			}
		});
		
		findViewById(R.id.button_consultar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				int rol = Integer.parseInt(session.getUserDetails().get(SessionManager.ROL));
				int idSupervisor = 0;
				int userId = 0;
				int tienda = 0;
				int proveedor = 0;
				
				if (Rol.ADMIN == rol) {
					
					Usuario user = (Usuario) getUsuarioSpinner().getSelectedItem();
					idSupervisor = Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID));
					userId = user.getIdUser();
					tienda = user.getIdTienda();
					proveedor = 24;
//							user.getIdProveedor();
					
					Log.d(LOG, "idUserEmpresa: " + user.getIdProveedor());
				} else {
					idSupervisor = Integer.parseInt(session.getUserDetails().get(SessionManager.ID_SUPERVISOR));
					userId = Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID));
					tienda = Integer.parseInt(session.getUserDetails().get(SessionManager.ID_TIENDA));
					proveedor = 24;
//							Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
				}
				String request = Request.busquedaPagosRequest(Text.formatDate(getInicioDate()), Text.formatDate(getTerminoDate()),idSupervisor, userId, tienda, proveedor);
				new WebServiceClient(new ConsultaListener(), ConsultaActivity.this, true, Url.BUSQUEDA_PAGOS).execute(request);
				
			}
		});
		
		if (Rol.VENDEDOR == rol) {
			findViewById(R.id.label_usuario).setVisibility(View.GONE);
			findViewById(R.id.spinner_usuario).setVisibility(View.GONE);
		} else {
			
			int idSupervisor = Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID));
			int idProveedor = 24;
//					Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
			int idTienda = Integer.parseInt(session.getUserDetails().get(SessionManager.ID_TIENDA));
			
			
			String param = Request.getVendedorRequest(idSupervisor, idTienda, idProveedor);
			new WebServiceClient(new VendedorListener(), ConsultaActivity.this, true, Url.GET_VENDEDOR).execute(param);
		}	
		
	}
	
	public Date getInicioDate() {
		return this.inicioDate;
	}
	
	public void setInicioDate(Date inicioDate) {
		this.inicioDate = inicioDate;
	}
	
	public Date getTerminoDate() {
		return this.terminoDate;
	}
	
	public void setTerminoDate(Date terminoDate) {
		this.terminoDate = terminoDate;
	}
	
	public Spinner getUsuarioSpinner() {
		if (null == usuarioSpinner) {
			usuarioSpinner = (Spinner) findViewById(R.id.spinner_usuario);
		}
		
		return usuarioSpinner;
	}
	
	public List<Usuario> getUsuarios() {
		if (null == usuarios) {
			usuarios = new ArrayList<Usuario>();
		}
		
		return usuarios;
	}
	
	private class VendedorListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						
						for (int i = 0; i < array.length(); i++) {
							getUsuarios().add(Usuario.fromJson(array.getJSONObject(i)));
						}
						
						getUsuarioSpinner().setAdapter(AppUtils.createUsuarioAadapter(ConsultaActivity.this, getUsuarios()));
						
					} else {
						Toast.makeText(ConsultaActivity.this, Error.ERROR_BUSQUEDA_USERS, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					Toast.makeText(ConsultaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(ConsultaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class ConsultaListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);

				Log.d(LOG, response);
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						String arrayString = array.toString();
						Intent i = new Intent(ConsultaActivity.this, VentasActivity.class);
						i.putExtra("ventas_array", arrayString);
						startActivity(i);
					} else {
						Toast.makeText(ConsultaActivity.this, Error.ERROR_RESULTADOS, Toast.LENGTH_SHORT).show();
					}					
				} catch (JSONException e) {
					Toast.makeText(ConsultaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(ConsultaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
