package org.addcel.prosa.view;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import org.addcel.chedraui.R;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.miuraapp.Client;
import org.addcel.miuraapp.LeeTarjetaDeslizada;
import org.addcel.miuraapp.TarjetaVO;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.Comision;
import org.addcel.prosa.dto.Pago;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.id;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PagoVISAActivity extends Activity {
	
	SessionManager session;
	
	private EditText nombreEdit,correoEdit, descEdit, tarjetaEdit, vigenciaEdit, cvvEdit, montoEdit;
	private int monedaInt;
	private double monto;
	private double comision;
	private double total;
	private int tipoTarjeta;
	private boolean porcentaje;
	
	private Comision comisiones;
	private double tipoCambio;
	private ImageButton scanButton;
	private Button datosButton;
	private BluetoothAdapter adapter;
	

	private final static int REQUEST_ENABLE_BT = 1;
	
	
	private static String LOG = "PagoVISAActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pago_visa);
		
		session = new SessionManager(PagoVISAActivity.this);
		
		getScanButton().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent("com.google.zxing.client.android.SCAN"); 
				intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE", "BARCODE_MODE"); 
				startActivityForResult(intent, 0);
			}
		});
		
		comisiones = new Comision();
		comisiones.setComision(Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION)));
		comisiones.setComisionPorcentaje(Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION_PCT)));
		
		nombreEdit = (EditText) findViewById(R.id.edit_nombre);
		correoEdit = (EditText) findViewById(R.id.edit_mail);
		descEdit = (EditText) findViewById(R.id.edit_desc);
		tarjetaEdit = (EditText) findViewById(R.id.edit_tarjeta);
		cvvEdit =(EditText) findViewById(R.id.edit_cvv);
		montoEdit = (EditText) findViewById(R.id.edit_monto);
		vigenciaEdit = (EditText) findViewById(R.id.edit_vigencia);
		monedaInt = 1;
		
		((RadioButton) findViewById(R.id.radio_visa)).setChecked(true);
		tipoTarjeta = 1;
		
		montoEdit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
				if (s.toString().length() > 0) {
					monto = Double.parseDouble(s.toString());
				} else {
					monto = 0;
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.toString().length() > 0) {
					total = monto + comision;
					((TextView) findViewById(R.id.text_total)).setText(Text.formatCurrency(total, true));
				}
			}
		});
		
		findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		findViewById(R.id.button_pago).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				if (validate()) {
					String request = Request.getTokenRequest(PagoVISAActivity.this);
					new WebServiceClient(new TokenListener(), PagoVISAActivity.this, true, Url.GET_TOKEN).execute(request);
				}
			}
		});
		
		getDatosButton().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				setBluetooth();
			}
		});
		
		comision = Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION));
		((TextView) findViewById(R.id.text_comision)).setText(Text.formatCurrency(comision, true));
		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(requestCode == 0)     {         
			if(resultCode == RESULT_OK)         {            
				String contents = data.getStringExtra("SCAN_RESULT");  
				String format = data.getStringExtra("SCAN_RESULT_FORMAT"); 
				
				((EditText) findViewById(R.id.edit_desc)).setText(contents);
				montoEdit.setText("9.0");
				monto = 9.0;	
				
				// Handle successful scan         
			} else if(resultCode == RESULT_CANCELED) {              // Handle cancel             
				Log.i("xZing", "Cancelled");         
			}     
		}
	}
	
	public EditText getVigenciaEdit() {
		if (null == vigenciaEdit) {
			vigenciaEdit = (EditText) findViewById(R.id.edit_vigencia);
		}
		
		return vigenciaEdit;
		
	}
	
	public ImageButton getScanButton() {
		if (null == scanButton) {
			scanButton = (ImageButton) findViewById(R.id.button_scan);
		}
		
		return scanButton;
	}
	
	public void onTipoTarjetaClicked(View view) {
		boolean checked = ((RadioButton) view).isChecked();
		
		switch (view.getId()) {
		case R.id.radio_visa:
			if (checked)
				tipoTarjeta = 1;
			break;

		case R.id.radio_mastercard:
			if (checked)
				tipoTarjeta = 2;
			break;
			
		default:
			break;
		}
	}
	
	public Comision getComisiones() {
		if (null == comisiones) {
			comisiones = new Comision();
			comisiones.setComision(Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION)));
			comisiones.setComisionPorcentaje(Double.parseDouble(session.getUserDetails().get(SessionManager.COMISION_PCT)));
			comisiones.setMinCom(Double.parseDouble(session.getUserDetails().get(SessionManager.MIN_COM)));
			comisiones.setMinComPorcentaje(Double.parseDouble(session.getUserDetails().get(SessionManager.MIN_COM_PCT)));
		}
		
		return comisiones;
	}
	
	public double getTipoCambio() {
		if (0.0 == tipoCambio) {
			tipoCambio = Double.parseDouble(session.getUserDetails().get(SessionManager.TIPO_CAMBIO));
		}
		
		return tipoCambio;
	}
	
	public Button getDatosButton() {
		if (datosButton == null) {
			datosButton = (Button) findViewById(R.id.button_getdatos);
		}
		
		return datosButton;
	}
	
	private boolean validate() {
		
		String nombre = nombreEdit.getText().toString().trim();
		String correo = correoEdit.getText().toString().trim();
		String desc = descEdit.getText().toString().trim();
		String tarjeta = tarjetaEdit.getText().toString().trim();
		String vigencia = vigenciaEdit.getText().toString().trim();
		String cvv = cvvEdit.getText().toString().trim();
		String monto = montoEdit.getText().toString().trim();
		
		if ("".equals(nombre)) {
			nombreEdit.requestFocus();
			nombreEdit.setError("El campo nombre no puede ir vac�o.");
			return false;
		}
		
	
		if ("".equals(correo)) {
			correoEdit.requestFocus();
			correoEdit.setError("El campo correo no puede ir vac�o.");
			return false;
		}
		
		if (!Validador.esCorreo(correo)) {
			correoEdit.requestFocus();
			correoEdit.setError("El correo introducido no es v�lido.");
			return false;
		}
		
		if ("".equals(desc)) {
			descEdit.requestFocus();
			descEdit.setError("Obtenga o introduzca c�digo de producto");
			return false;
		}
		
		if ("".equals(tarjeta)) {
			tarjetaEdit.requestFocus();
			tarjetaEdit.setError("El campo tarjeta no puede ir vac�o.");
			return false;
		}
		
		if ("".equals(vigencia)) {
			vigenciaEdit.requestFocus();
			vigenciaEdit.setError("El campo vigencia no debe ir vac�o.");
		}
		
		if (tipoTarjeta == 1 && !Validador.esTarjetaDeCreditoVisa(tarjeta)) {
			tarjetaEdit.requestFocus();
			tarjetaEdit.setError("La tarjeta introducida no es v�lida.");
			return false;
		}
		
		if (tipoTarjeta == 2 && !Validador.esTarjetaDeCreditoMasterCard(tarjeta)) {
			tarjetaEdit.requestFocus();
			tarjetaEdit.setError("La tarjeta introducida no es v�lida.");
			return false;
		}
		
		if ("".equals(cvv)) {
			cvvEdit.requestFocus();
			cvvEdit.setError("El campo cvv no puede ir vac�o.");
			return false;
		}
		
		if (3 > cvv.length()) {
			cvvEdit.requestFocus();
			cvvEdit.setError("El campo cvv debe tener 3 caracteres.");
			return false;
		}
		
		if ("".equals(monto)) {
			montoEdit.requestFocus();
			montoEdit.setError("El campo monto no puede ir vac�o");
			return false;
		}
		
		return true;
	}
	
	 public void setBluetooth() {
	    adapter = BluetoothAdapter.getDefaultAdapter();
	    
	    if (adapter == null) {
	      Toast.makeText(this, "Bluetooth no disponible en este dispositivo", Toast.LENGTH_SHORT).show();
	    } else {
	    	new BluetoothTask().execute(adapter);
	    }
	 }
		 
	 
	 public static final byte[] intToByteArray(int value) {
		 return new byte[] {(byte) (value >>> 24),
				 			(byte) (value >>> 16),
				 			(byte) (value >>> 8),
		 					(byte) value};
	 }

	 
	private class BluetoothTask extends AsyncTask<BluetoothAdapter, Void, TarjetaVO> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			Dialogos.makeDialog(PagoVISAActivity.this, "Cargando", "Obteniendo datos de tarjeta").show();
		}
		
		@Override
		protected TarjetaVO doInBackground(BluetoothAdapter... params) {
			// TODO Auto-generated method stub
			if (!params[0].isEnabled()) {
	    	    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	    	    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
			

			BluetoothSocket tmp = null;
			
			BluetoothDevice device = params[0].getRemoteDevice("20:13:04:08:29:D8");
			Log.i(LOG, "Dispositivo: " + device.getName());
			
			UUID uuid = UUID.nameUUIDFromBytes(intToByteArray(0x1101));
			Log.i("LOG", "UUID: " + uuid.toString());
			
			try {
				
				boolean isDiscovering = params[0].isDiscovering();
				Log.i(LOG, "Descubriendo?: " + isDiscovering);
				if (isDiscovering) {
					params[0].cancelDiscovery();
				}
				Method m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
				
				tmp = (BluetoothSocket) m.invoke(device, 1);
				
				assert (tmp != null) : "Socket is Null";
				tmp.connect();
				
				try {
					
					InputStream mmInStream = tmp.getInputStream();
					OutputStream mmOutStream = tmp.getOutputStream();
					
					Client client = new Client(new DataInputStream(mmInStream), mmOutStream);
					LeeTarjetaDeslizada lee = new LeeTarjetaDeslizada();
					client.receiveResponse(null, null);
					
					TarjetaVO datos = lee.transaccion(client);
					mmInStream.close();
					mmOutStream.close();
					tmp.close();
					
					return datos;
					
				} finally {
					Log.i(LOG, "Cerrando Socket");
					tmp.close();
				}
				
			} catch (IOException e) {
        		Log.e("FormAcivity",  "Error de conexion", e);
        	} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(TarjetaVO result) {
			// TODO Auto-generated method stub
			Dialogos.closeDialog();
			
			if (null != result) {
					
				String nombre = "";
				
				if (result.getNombre_titular() == null) {
					nombreEdit.setText(nombre);
				} else {
					nombre = result.getNombre_titular();
					nombreEdit.setText(nombre);
				}
				
				tarjetaEdit.setText(result.getCuenta());
				vigenciaEdit.setText(result.getFecha_expiracion());
				
				
			} else {
				Toast.makeText(PagoVISAActivity.this, "Error de conexi�n con terminal", Toast.LENGTH_SHORT).show();
			}
		}
	}
		 
	
	private class TokenListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					String token = json.getString("token");
					
					String vigencia = vigenciaEdit.getText().toString().trim();
					String mes = vigencia.substring(2, 4);
					String anio = vigencia.substring(0, 2);
					
					Pago pago = new Pago();
					pago.setDescProducto(descEdit.getText().toString().trim());
					pago.setEmail(correoEdit.getText().toString().trim());
					pago.setNombres(nombreEdit.getText().toString().trim());
					pago.setTarjeta(tarjetaEdit.getText().toString().trim());
					pago.setCvv2(cvvEdit.getText().toString().trim());
					pago.setVigencia(mes + "/" + anio);
//					Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR))
					pago.setIdProveedor(24);
					
					Log.d(LOG, "ID Proveedor: " + pago.getIdProveedor());
					pago.setIdUser(Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID)));
					pago.setMoneda(1);
					
					pago.setSubTotal(monto);
					pago.setTipo(tipoTarjeta);
					pago.setToken(token);
					pago.setTotal(total);
					
					String request = Request.pagoRequest(PagoVISAActivity.this, pago);
					
					new WebServiceClient(new PagoListener(), PagoVISAActivity.this, true, Url.PAGO_VISA).execute(request);

					
				} catch (JSONException e) {
					Toast.makeText(PagoVISAActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(PagoVISAActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}
	
	private class PagoListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			
			if (null != response && !response.equals("")) {
				
				Log.d(LOG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					

					Intent i = new Intent(PagoVISAActivity.this, DetalleActivity.class);
					i.putExtra("pago_response", json.toString());
					
					if (0 == json.optInt("idError", -1)) {
						i.putExtra("subtotal", monto);
						i.putExtra("total", total);
						startActivity(i);
						finish();
					} else {
						startActivity(i);
					}
					

				} catch (JSONException e) {
					Toast.makeText(PagoVISAActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(PagoVISAActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
			
		}
	}
}
