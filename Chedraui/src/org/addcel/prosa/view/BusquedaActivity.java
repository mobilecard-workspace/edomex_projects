package org.addcel.prosa.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class BusquedaActivity extends Activity {
	
	private EditText nombreEdit, paternoEdit, maternoEdit, loginEdit;
	private SessionManager session;
	
	private static final String LOG = "BusquedaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_busqueda);
		
		session = new SessionManager(BusquedaActivity.this);
		
		nombreEdit = (EditText) findViewById(R.id.edit_nombre);
		paternoEdit = (EditText) findViewById(R.id.edit_paterno);
		maternoEdit = (EditText) findViewById(R.id.edit_materno);
		loginEdit = (EditText) findViewById(R.id.edit_login);
		
		findViewById(R.id.button_buscar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (validate()) {
					String nombre = nombreEdit.getText().toString().trim();
					String paterno = paternoEdit.getText().toString().trim();
					String materno = maternoEdit.getText().toString().trim();
					String login = loginEdit.getText().toString().trim();
					int idProveedor = 24;
//						Integer.parseInt(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
					
					String request = Request.findUserRequest(nombre, paterno, materno, login, idProveedor);
					
					new WebServiceClient(new FindUserListener(), BusquedaActivity.this, true, Url.FIND_USER).execute(request);
				}
			}
		});
	}
	
	private boolean validate() {
		String nombre = nombreEdit.getText().toString().trim();
		String paterno = paternoEdit.getText().toString().trim();
		String materno = maternoEdit.getText().toString().trim();
		String login = loginEdit.getText().toString().trim();
		
		if (nombre.equals("") && paterno.equals("")  && materno.equals("")  && login.equals("")) {
			Toast.makeText(BusquedaActivity.this, Error.ERROR_BUSQUEDA, Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}
	}
	
	private class FindUserListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				Log.d(LOG, "Response FindUser: " + response);
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 >= array.length()) {
						Toast.makeText(BusquedaActivity.this, Error.ERROR_RESULTADOS, Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
						intent.putExtra("resultados", response);
						startActivity(intent);
					}
					
				} catch (JSONException e) {
					Toast.makeText(BusquedaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(BusquedaActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
