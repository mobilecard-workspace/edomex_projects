package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.interfaces.DialogOkListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.adapter.ResultadosAdapter;
import org.addcel.prosa.dto.Usuario;
import org.addcel.util.Dialogos;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ResultadosActivity extends Activity {
	
	private ListView resultadosListView;
	private List<Usuario> resultados;
	private String resultadosExtra;
	private JSONArray resultadosJSONArray;
	
	private static final String LOG = "ResultadosActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultados);
		getResultadosListView().setAdapter(new ResultadosAdapter(ResultadosActivity.this, getResultados()));
		getResultadosListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				final Usuario usuario = (Usuario)((ResultadosAdapter) arg0.getAdapter()).getItem(arg2);
				
				Dialogos.makeResultadosAlert(ResultadosActivity.this, "Selecciona:", new DialogOkListener() {
					
					@Override
					public void ok(DialogInterface dialog, int id) {
						Intent i = new Intent(ResultadosActivity.this, UserUpdateActivity.class);
						i.putExtra("usuario", usuario);
						startActivity(i);
						finish();
					}
					
					@Override
					public void cancel(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						Intent i = new Intent(ResultadosActivity.this, CambioPasswordActivity.class);
						i.putExtra("supervisor", true);
						i.putExtra("idUser", usuario.getIdUser());
						i.putExtra("nombre_usuario", usuario.getLogin());
						startActivity(i);
						finish();
					}
				});
			}
		});
	}
	
	public JSONArray getResultadosJSONArray() {
		if (null == resultadosJSONArray) {
			
			if (null == resultadosExtra) {
				resultadosExtra = getIntent().getStringExtra("resultados");
				Log.d(LOG, resultadosExtra);
			}
			
			try {
				resultadosJSONArray = new JSONArray(resultadosExtra);
			} catch (JSONException e) {
				return null;
			}
		}
		
		return resultadosJSONArray;
		
			
	}
	
	public ListView getResultadosListView() {
		if (null == resultadosListView) {
			resultadosListView = (ListView) findViewById(R.id.list_resultados);
		}
		
		return resultadosListView;
	}
	
	public List<Usuario> getResultados() {
		if (null == resultados) {
			resultados = new ArrayList<Usuario>();
			
			for (int i = 0; i < getResultadosJSONArray().length(); i++) {
				JSONObject j = getResultadosJSONArray().optJSONObject(i);
				
				Usuario usuario = new Usuario();
				usuario.setIdUser(j.optInt("idUser"));
				usuario.setNombres(j.optString("nombres"));
				usuario.setApellidoP(j.optString("apellidoP"));
				usuario.setApellidoM(j.optString("apellidoM"));
				usuario.setEmail(j.optString("email"));
				usuario.setTelefono(j.optString("telefono"));
				usuario.setIdProveedor(j.optInt("idUserEmpresa"));
				usuario.setIdTienda(j.optInt("idTienda"));
				usuario.setPassword(j.optString("password"));
				usuario.setLastLogin(j.optLong("lastLogin", 0));
				usuario.setFechaAlta(j.optLong("fechaAlta", 0));
				usuario.setIdSupervisor(j.optInt("idSupervisor"));
				usuario.setLogin(j.optString("login"));
				usuario.setLoginCount(j.optInt("loginCount"));
				usuario.setIdStatus(j.optInt("idStatus"));
				usuario.setIdRole(j.optInt("idRole"));
				usuario.setDescTienda(j.optString("desc_tienda"));
				usuario.setDescStatus(j.optString("desc_status"));
				usuario.setDescRole(j.optString("desc_role"));
				
				resultados.add(usuario);
			}
		}
		
		return resultados;
	}

}
