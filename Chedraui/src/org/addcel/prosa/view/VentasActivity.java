package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.chedraui.R;
import org.addcel.prosa.adapter.ConsultaAdapter;
import org.addcel.prosa.dto.Venta;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class VentasActivity extends Activity {
	
	private ListView ventasListView;
	private List<Venta> ventas;
	private String ventasExtra;
	private JSONArray ventasArray;
	
	private static final String LOG = "VentasActivity";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ventas);
		getVentasListView().setAdapter(new ConsultaAdapter(VentasActivity.this, getVentas()));
		getVentasListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				final Venta venta = (Venta)((ConsultaAdapter) arg0.getAdapter()).getItem(arg2);
				
				
			}
		});
		
	}
	
	public JSONArray getVentasArray() {
		if (null == ventasArray) {
			
			if (null == ventasExtra) {
				ventasExtra = getIntent().getStringExtra("ventas_array");
				Log.d(LOG, ventasExtra);
			}
			
			try {
				ventasArray = new JSONArray(ventasExtra);
			} catch (JSONException e) {
				return null;
			}
		}
			
		return ventasArray;
	}
	
	public ListView getVentasListView() {
		if (null == ventasListView) {
			ventasListView = (ListView) findViewById(R.id.list_ventas);
		}
		
		return ventasListView;
	}
	
	public List<Venta> getVentas() {
		if (null == ventas) {
			ventas = new ArrayList<Venta>();
			
			for (int i = 0; i < getVentasArray().length(); i++) {
				JSONObject j  = getVentasArray().optJSONObject(i);
				
				Venta venta = new Venta();
				venta.setDescProducto(j.optString("descProducto"));
				venta.setFecha(j.optString("fecha"));
				venta.setMoneda(j.optString("moneda"));
				venta.setNombre(j.optString("nombres"));
				venta.setReferencia(j.optString("referencia"));
				venta.setTotal(j.optString("total"));
				
				ventas.add(venta);
				
			}
		}
		
		return ventas;
	}
}
