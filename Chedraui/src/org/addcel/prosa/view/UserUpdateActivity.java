package org.addcel.prosa.view;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.dto.ItemCatalogoDTO;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.chedraui.R;
import org.addcel.prosa.constants.Error;
import org.addcel.prosa.constants.Status;
import org.addcel.prosa.constants.Url;
import org.addcel.prosa.dto.Rol;
import org.addcel.prosa.dto.Tienda;
import org.addcel.prosa.dto.Usuario;
import org.addcel.prosa.util.Request;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class UserUpdateActivity extends Activity {
	
	private Usuario usuario;
	
	SessionManager session;
	private static final String LOG = "UserUpdateActivity";
	private List<Tienda> tiendas;
	private List<Rol> roles;
	private List<ItemCatalogoDTO> statuses;
	private Spinner tiendaSpinner;
	private Spinner rolesSpinner;
	private Spinner statusSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		
		session = new SessionManager(UserUpdateActivity.this);
		
		((TextView) findViewById(R.id.text_header)).setText("Actualizaci�n de datos");
		
		findViewById(R.id.layout_status).setVisibility(View.VISIBLE);

		((Button) findViewById(R.id.button_registrar)).setText("Actualizar Datos");
		
		final EditText login = (EditText) findViewById(R.id.edit_login);
		final EditText nombre = (EditText) findViewById(R.id.edit_nombre);
		final EditText paterno = (EditText) findViewById(R.id.edit_paterno);
		final EditText materno = (EditText) findViewById(R.id.edit_materno);
		final EditText celular = (EditText) findViewById(R.id.edit_celular);
		final EditText correo = (EditText) findViewById(R.id.edit_correo);
		
		findViewById(R.id.button_borrar).setVisibility(View.VISIBLE);
		findViewById(R.id.button_borrar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				int idUser = getUsuario().getIdUser();
				int idProveedor = getUsuario().getIdProveedor();
				int idSupervisor = Integer.parseInt(session.getUserDetails().get(SessionManager.USR_ID));
				String login = getUsuario().getLogin();
				int idRole = getUsuario().getIdRole();
				
				String request = Request.deleteUserRequest(idUser, idProveedor, idSupervisor, login, idRole);
				new WebServiceClient(new DeleteUserListener(), UserUpdateActivity.this, true, Url.DELETE_USER).execute(request);
			}
		});
		
		findViewById(R.id.button_registrar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getUsuario().setLogin(login.getText().toString().trim());
				getUsuario().setNombres(nombre.getText().toString().trim());
				getUsuario().setApellidoP(paterno.getText().toString().trim());
				getUsuario().setApellidoM(materno.getText().toString().trim());
				getUsuario().setTelefono(celular.getText().toString().trim());
				getUsuario().setEmail(correo.getText().toString().trim());
				getUsuario().setIdTienda(((Tienda) getTiendaSpinner().getSelectedItem()).getIdTienda());
				getUsuario().setIdRole(((Rol) getRolesSpinner().getSelectedItem()).getIdRol());
				
				int idStatus =Integer.valueOf(((ItemCatalogoDTO) getStatusSpinner().getSelectedItem()).getId());
				getUsuario().setIdStatus(idStatus);
				
				String request = Request.updateUserRequest(getUsuario());
				
				new WebServiceClient(new UpdateUserListener(), UserUpdateActivity.this, true, Url.UPDATE_USER).execute(request);
				
			}
		});
		
		String param = Request.getTiendasRequest(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
		
		if (!"".equals(param)) {
			new WebServiceClient(new TiendaListener(), UserUpdateActivity.this, false, Url.GET_TIENDAS).execute(param);
		} else {
			Toast.makeText(UserUpdateActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
		}
	}
	
	public Usuario getUsuario() {
		if (null == usuario) {
			usuario = getIntent().getParcelableExtra("usuario");
			Log.d(LOG, " USUARIO: " + usuario);
		}
		
		return usuario;
	}
	public List<Tienda> getTiendas() {
		if (null == tiendas) {
			tiendas = new LinkedList<Tienda>();
		}
		
		return tiendas;
	}
	
	public Spinner getTiendaSpinner() {
		if (null == tiendaSpinner) {
			tiendaSpinner = (Spinner) findViewById(R.id.spinner_tienda);
		}
		
		return tiendaSpinner;
	}
	
	public List<Rol> getRoles() {
		if (null == roles) {
			roles = new LinkedList<Rol>();
		}
		
		return roles;
	}
	
	public Spinner getRolesSpinner() {
		if (null == rolesSpinner) {
			rolesSpinner = (Spinner) findViewById(R.id.spinner_rol);
		}
		
		return rolesSpinner;
	}
	
	public List<ItemCatalogoDTO> getStatuses() {
		if (null == statuses) {
			statuses = new ArrayList<ItemCatalogoDTO>();
			statuses.add(new ItemCatalogoDTO(Status.ACTIVO.getValue(), Status.ACTIVO.getName()));
			statuses.add(new ItemCatalogoDTO(Status.BLOQUEADO.getValue(), Status.BLOQUEADO.getName()));
			statuses.add(new ItemCatalogoDTO(Status.BORRADO.getValue(), Status.BORRADO.getName()));
		}
		
		return statuses;
	}
	
	public Spinner getStatusSpinner() {
		if (null == statusSpinner) {
			statusSpinner = (Spinner) findViewById(R.id.spinner_status);
		}
		
		return statusSpinner;
	}
	
	private boolean validate() {
		return true;
	}
	
	private class TiendaListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						for (int i = 0; i < array.length(); i++) {
							JSONObject json = array.getJSONObject(i);
							Tienda t = new Tienda(json.optInt("idTienda"), json.optString("descripcion"), json.optString("proveedor"));
							getTiendas().add(t);
						}
						getTiendaSpinner().setAdapter(AppUtils.createTiendaAdapter(UserUpdateActivity.this, getTiendas()));
						
						for (int i = 0; i < getTiendas().size(); i++) {
							if (getTiendas().get(i).getIdTienda() == getUsuario().getIdTienda()) {
								getTiendaSpinner().setSelection(i);
								break;
							}
						}
						
						String param = Request.getRolesRequest(session.getUserDetails().get(SessionManager.ID_PROVEEDOR));
						new WebServiceClient(new RolListener(), UserUpdateActivity.this, true, Url.GET_ROLES).execute(param);
						
					} else {
						Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
					}
					
				} catch (JSONException e) {
					Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class RolListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONArray array = new JSONArray(response);
					
					if (0 < array.length()) {
						for (int i = 0; i < array.length(); i++) {
							JSONObject json = array.getJSONObject(i);
							Rol r = new Rol(json.optInt("idRol"), json.optString("maxLogin"), json.optInt("idAplication"), json.optString("descripcion"));
							getRoles().add(r);
						}
						
						getRolesSpinner().setAdapter(AppUtils.createRolAdapter(UserUpdateActivity.this, getRoles()));
						
						for (int i = 0; i < getRoles().size(); i++) {
							if (getRoles().get(i).getIdRol() == getUsuario().getIdRole()) {
								getRolesSpinner().setSelection(i);
								break;
							}
						}
						
						getStatusSpinner().setAdapter(AppUtils.createCatalogAdapter(UserUpdateActivity.this, getStatuses()));
						
						for (int i = 0; i < getStatuses().size(); i++) {
							if (Integer.valueOf(getStatuses().get(i).getId()) == getUsuario().getIdStatus()) {
								getStatusSpinner().setSelection(i);
								break;
							}
						}
						
						final EditText login = (EditText) findViewById(R.id.edit_login);
						final EditText nombre = (EditText) findViewById(R.id.edit_nombre);
						final EditText paterno = (EditText) findViewById(R.id.edit_paterno);
						final EditText materno = (EditText) findViewById(R.id.edit_materno);
						final EditText celular = (EditText) findViewById(R.id.edit_celular);
						final EditText correo = (EditText) findViewById(R.id.edit_correo);
					
						login.setText(getUsuario().getLogin());
						nombre.setText(getUsuario().getNombres());
						paterno.setText(getUsuario().getApellidoP());
						materno.setText(getUsuario().getApellidoM());
						celular.setText(getUsuario().getTelefono());
						correo.setText(getUsuario().getEmail());
					
					} else {
						Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
					}
					
				} catch (JSONException e) {
					Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(UserUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class UpdateUserListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				Log.d(LOG, "Response UpdateUser: " + response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					int idError = json.optInt("idError", -1);
					String mensajeError = json.optString("mensajeError", Error.ERROR_CONEXION);
					
					switch (idError) {
						case 0:
							Toast.makeText(UserUpdateActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							finish();
							break;
	
						default:
							Toast.makeText(UserUpdateActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							break;
					}
					
				} catch (JSONException e) {
					Toast.makeText(UserUpdateActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(UserUpdateActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private class DeleteUserListener implements WSResponseListener {
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				Log.d(LOG, "Response DeleteUser: " + response);
				
				try {
					JSONObject json = new JSONObject(response);
					int idError = json.optInt("idError", -1);
					String mensajeError = json.optString("mensajeError", Error.ERROR_CONEXION);
					
					switch (idError) {
						case 0:
							Toast.makeText(UserUpdateActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							finish();
							break;
	
						default:
							Toast.makeText(UserUpdateActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
							break;
					}
				} catch (JSONException e) {
					Toast.makeText(UserUpdateActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(UserUpdateActivity.this, Error.ERROR_CONEXION, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
