package com.addcel.gdf;

import net.rim.device.api.ui.UiApplication;

import com.addcel.gdf.view.VSplash;

public class AStart extends UiApplication {

	public static void main(String[] args) {

		AStart theApp = new AStart();
		theApp.enterEventDispatcher();
	}

	public AStart() {

		pushScreen(new VSplash());
	}
}

