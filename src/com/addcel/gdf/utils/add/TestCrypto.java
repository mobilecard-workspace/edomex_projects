package com.addcel.gdf.utils.add;

public class TestCrypto {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestCrypto test = new TestCrypto();
		String key = "12345678";
//		String cadena=test.getJson();
		String cadena = "{\"toDO\":3,\"placa\":\"183XGF\",\"usuario\":\"addcel\",\"password\":\"25b6f919837ece5aab57930fcb1ee09e\"}";
		cadena = AddcelCrypto.encryptSensitive(key, cadena);

		
		//char a = "+".charAt(0);
		//char b = " ".charAt(0);
		//cadena = cadena.replace(a, b);

		//cadena = AddcelCrypto.decryptHard(cadena);
		
//		String cadena = "08Qlq+yXRwje6J8HfGg0l62RK624f60KP62iEkQ4GyyJ0fyWrsGJnDi//gLWeaEJpeggpbcz8A6f9spayaNl1NqvuMrXluv5BxeB2lpmYPGr0oOp38XCI3TvUZnZEF158iMveRgZomTqM3LTmgFAuXiHzN3a+lqm90ZuGG/WV/RhcmzOtVWbnGXudkQJNCP2505MV1rjypeJstV2Ap6nvbyIi38xGt8lIqvTpfXIbOUPN0HjYn41INOjOif3w3xWHfLJCmjR2Q3rMsE5RLy9/elqNynUwkphlmCNlHcnFXjwnr9Xf/gNMW0lJPEy/Ij/ph4C+DFIUvkie/DSVtN6I0QvkV+FsCjFdzauwhKRbUm8ZaWipBWRYTBhO+pAiVhvbaahOYs/yvRCu7bKip3nk1cxnUuj7KMQ==";
		cadena = AddcelCrypto.decryptSensitive(cadena);
	}

	private String json;
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public TestCrypto(){
		getObjeto();
	}
	public TestCrypto(String json){
		this.json = json;
	}
	
	private void getObjeto(){
		this.json = " "+
		" { " +
		    " \"placa\":\"100ABC\", " +
		    " \"condonacion\":false, " +
		    " \"interes\":false, " +
		    " \"subsidio\":false, " +
		    " \"error\":\"\", " +
		    " \"ejercicio\":2013, " +
		    " \"modelo\":2005, " +
		    " \"total\":2379, " +
		    " \"lineacaptura\":\"84105XX100ABC7RK52JV\", " +
		    " \"vigencia\":\"Jul 31, 2013 1:00:00 AM\" , " +
		    " \"tipoServicio\":\"PARTICULAR\", " +
		    " \"cve_vehi\":\"0040720\", " +
		    " \"fech_factura\":\"2005-04-22\", " +
		    " \"tenencia\":1872.69, " +
		    " \"derecho\":411, " +
		    " \"tenActualizacion\":0, " +
		    " \"tenRecargo\":78.09, " +
		    " \"derActualizacion\":0, " +
		    " \"derRecargo\":17.13, " +
		    " \"totalDerecho\":428.13, " +
		    " \"derechos\":411, " +
		    " \"actualizacion\":0, " +
		    " \"recargos\":95.22, " +
		    " \"tenSubsidio\":0, " +
		    " \"tenencia2\":\"1872.69\", " +
		    " \"derechos2\":\"411.00\", " +
		    " \"tenSubsidio2\":\"0.00\", " +
		    " \"total2\":\"2379.00\", " +
		    " \"vigencia2\":1375250400000 " +
		" } ";
		
		
	}
}
