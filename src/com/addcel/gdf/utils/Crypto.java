package com.addcel.gdf.utils;

public class Crypto {

	public String key = "1234567890ABCDEF0123456789ABCDEF";
	
	public String encriptarSec(String user, String password, String json){
		
		String value = null;
		String jsonEncrypt = null;
		String jsonEncrypt2 = null;

		if ((user != null) && (password != null)) {

			user = user.trim();
			password = password.trim();
			
			String parse = UtilSecurity.parsePass(password);
			jsonEncrypt = UtilSecurity.aesEncrypt(parse, json);
			jsonEncrypt2 = UtilSecurity.mergeStr(jsonEncrypt, password);
			value = jsonEncrypt2;
		}
		
		return value;
	}
	
	
	public String desencriptarSec(String password, String json) {

		String pass = UtilSecurity.parsePass(password);
		
		json = UtilSecurity.aesDecrypt(pass, json);
		return json;
	}


	public String encriptarCat(String json) {

		json = UtilSecurity.aesEncrypt(key, json);
		return json;
	}
	
	
	public String desencriptarCat(String json) {

		json = UtilSecurity.aesDecrypt(key, json);
		return json;
	}
}
