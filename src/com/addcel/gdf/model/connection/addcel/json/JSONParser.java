package com.addcel.gdf.model.connection.addcel.json;


import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.GeneralBean;
import com.addcel.gdf.dto.UserBean;

public class JSONParser {

	public JSONParser() {

	}

	public UserBean getUser(String json) {

		UserBean userBean = null;
		JSONObject jsObject = null;
		String user = "";
		String login = "";
		String pswd = "";
		String birthday = "";
		String phone = "";
		String registerDate = "";
		String name = "";
		String lastName = "";
		String address = "";
		String credit = "";
		String life = "";
		String bank = "";
		String creditType = "";
		String provider = "";
		String status = "";
		String mail = "";
		String idUser = "";
		String materno = "";
		String sexo = "";
		String tel_casa = "";
		String tel_oficina = "";
		String id_estado = "";
		String ciudad = "";
		String calle = "";
		String num_ext = "";
		String num_interior = "";
		String colonia = "";
		String cp = "";
		String dom_amex = "";
		
		try {

			jsObject = new JSONObject(json);

			if (jsObject.has("usuario")) {
				user = jsObject.getString("usuario");
			}

			if (jsObject.has("login")) {
				login = jsObject.getString("login");
			}

			if (jsObject.has("password")) {
				pswd = jsObject.getString("password");
			}

			if (jsObject.has("nacimiento")) {
				birthday = jsObject.getString("nacimiento");
			}

			if (jsObject.has("telefono")) {
				phone = jsObject.getString("telefono");
			}

			if (jsObject.has("registro")) {
				registerDate = jsObject.getString("registro");
			}

			if (jsObject.has("nombre")) {
				name = jsObject.getString("nombre");
			}

			if (jsObject.has("apellido")) {
				lastName = jsObject.getString("apellido");
			}

			if (jsObject.has("direccion")) {
				address = jsObject.getString("direccion");
			}

			if (jsObject.has("tarjeta")) {
				credit = jsObject.getString("tarjeta");
			}

			if (jsObject.has("vigencia")) {
				life = jsObject.getString("vigencia");
			}

			if (jsObject.has("banco")) {
				bank = jsObject.getString("banco");
			}

			if (jsObject.has("tipotarjeta")) {
				creditType = jsObject.getString("tipotarjeta");
			}

			if (jsObject.has("proveedor")) {
				provider = jsObject.getString("proveedor");
			}

			if (jsObject.has("status")) {
				status = jsObject.getString("status");
			}
			if (jsObject.has("mail")) {
				mail = jsObject.getString("mail");
			}
			if (jsObject.has("usuario")) {
				idUser = jsObject.getString("usuario");
			}
			if (jsObject.has("materno")) {
				materno = jsObject.getString("materno");
			}
			if (jsObject.has("sexo")) {
				sexo = jsObject.getString("sexo");
			}
			if (jsObject.has("tel_casa")) {
				tel_casa = jsObject.getString("tel_casa");
			}
			if (jsObject.has("tel_oficina")) {
				tel_oficina = jsObject.getString("tel_oficina");
			}
			if (jsObject.has("id_estado")) {
				id_estado = jsObject.getString("id_estado");
			}
			if (jsObject.has("ciudad")) {
				ciudad = jsObject.getString("ciudad");
			}
			if (jsObject.has("calle")) {
				calle = jsObject.getString("calle");
			}
			if (jsObject.has("num_ext")) {
				num_ext = jsObject.getString("num_ext");
			}
			if (jsObject.has("num_interior")) {
				num_interior = jsObject.getString("num_interior");
			}
			if (jsObject.has("colonia")) {
				colonia = jsObject.getString("colonia");
			}
			if (jsObject.has("cp")) {
				cp = jsObject.getString("cp");
			}
			if (jsObject.has("dom_amex")) {
				dom_amex = jsObject.getString("dom_amex");
			}
			/*
			 * String =""; String sexo=""; String tel_casa=""; String
			 * tel_oficina=""; String id_estado=""; String ciudad=""; String
			 * calle=""; String num_ext=""; String num_interior=""; String
			 * colonia=""; String cp=""; String dom_amex="";
			 */

			userBean = new UserBean(login, pswd, birthday, phone, registerDate,
					name, lastName, address, credit, life, bank, creditType,
					provider, status, mail, idUser, materno, sexo, tel_casa,
					tel_oficina, id_estado, ciudad, calle, num_ext,
					num_interior, colonia, cp, dom_amex);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// System.out.println(e.toString());
			return userBean;
		}

		return userBean;

	}

	public boolean isLogin(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
				// MainClass.IdUser=id;

			}

			if (msj.equals("1")) {
				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(id.indexOf("|") + 1,
							id.length());
					UserBean.idLogin = mensaje;
				}

				isLogin = true;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isLogin;
		}

		return isLogin;

	}

	public boolean isLogin2(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {
				msj = jsObject.getString("resultado");
			}
			if (jsObject.has("mensaje")) {
				id = jsObject.getString("mensaje");
			}

			if (msj.equals("99")) {

				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(0, id.indexOf("|"));
					String iduser = id.substring(id.indexOf("|") + 1,
							id.length());
					System.out.println("mensaje " + mensaje);
				}
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;
	}

	
	public boolean isLogin3(String json) {

		boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
			}

			if (msj.equals("98")) {
				if (id.indexOf("|") != -1) {
					String mensaje = id.substring(0, id.indexOf("|"));
					String iduser = id.substring(id.indexOf("|") + 1,
							id.length());
					System.out.println("mensaje " + mensaje);
					// MainClass.msg=mensaje;
					// MainClass.IdUser=iduser;
					System.out.println("iduser " + iduser);
				}
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;

	}

	public String setSMS(String json) {

		//boolean isLogin = false;
		String msj = "";
		String id = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}
			if (jsObject.has("mensaje")) {

				id = jsObject.getString("mensaje");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return id;

	}

	public boolean setTag(String json) {

		boolean isLogin = false;
		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}

			if (msj.equals("0")) {
				isLogin = true;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return isLogin;

	}

	public String getMessageError(String json) {

		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("mensaje")) {

				msj = jsObject.getString("mensaje");

			}

		} catch (JSONException e) {
			e.printStackTrace();
			msj = "Favor de intentar m�s tarde.";
		}

		return msj;
	}

	
	public boolean isRegister(String json) {

		boolean isRegister = false;
		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("resultado")) {

				msj = jsObject.getString("resultado");

			}

			if (msj.equals("1")) {
				isRegister = true;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isRegister;
		}

		return isRegister;

	}

	public String getMessage(String json) {

		String message = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			if (jsObject.has("mensaje")) {

				message = jsObject.getString("mensaje");

			}

		} catch (JSONException e) {
			e.printStackTrace();
			message = "Favor de intentar m�s tarde.";
		}

		return message;
	}

	
	public GeneralBean[] getInfoCivil(String type, String json) {

		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("clave")) {

					clave = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave);

			}

		} catch (JSONException e) {
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}

	public GeneralBean[] getInfoCredits(String type, String json) {

		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("clave")) {

					clave = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave);

			}

		} catch (JSONException e) {
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}

	
	public GeneralBean[] getProducts(String type, String json) {
		System.out.println(json);
		GeneralBean[] bankBeans = null;
		JSONObject jsObject = null;
		JSONObject jsObjectInternal = null;
		JSONArray jsArray = null;
		int lenght = 0;
		String description = "";
		String clave = "";
		String claveE = "";
		String nombre = "no";

		try {

			jsObject = new JSONObject(json);

			jsArray = jsObject.getJSONArray(type);

			lenght = jsArray.length();

			bankBeans = new GeneralBean[lenght];

			for (int i = 0; i < lenght; i++) {

				jsObjectInternal = jsArray.getJSONObject(i);

				if (jsObjectInternal.has("descripcion")) {

					description = jsObjectInternal.getString("descripcion");

				}

				if (jsObjectInternal.has("claveWS")) {

					clave = jsObjectInternal.getString("claveWS");

				}
				if (jsObjectInternal.has("nombre")) {

					nombre = jsObjectInternal.getString("nombre");

				}

				if (jsObjectInternal.has("clave")) {

					claveE = jsObjectInternal.getString("clave");

				}

				bankBeans[i] = new GeneralBean(description, clave, nombre,
						claveE);

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			bankBeans = new GeneralBean[0];
		}

		return bankBeans;
	}

	/*
	 * 
	 * public ProductBean[] getCategories(String json){
	 * 
	 * ProductBean[] productBean = null;
	 * 
	 * JSONObject jsObject = null; JSONArray jsArray = null; int until = 0;
	 * String clave = ""; String path = ""; String description = "";
	 * 
	 * try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("categorias");
	 * 
	 * until = jsArray.length();
	 * 
	 * productBean = new ProductBean[until];
	 * 
	 * for(int i = 0; i < until; i++){
	 * 
	 * JSONObject jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * if(jsObjectInternal.has("id")){ clave = jsObjectInternal.getString("id");
	 * }
	 * 
	 * if(jsObjectInternal.has("path")){ path =
	 * jsObjectInternal.getString("path"); }
	 * 
	 * if(jsObjectInternal.has("descripcion")){ description =
	 * jsObjectInternal.getString("descripcion"); }
	 * 
	 * productBean[i] = new ProductBean(clave, path, description);
	 * 
	 * }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); productBean = new ProductBean[0]; return
	 * productBean; }
	 * 
	 * return productBean;
	 * 
	 * }
	 * 
	 * public GeneralBean[] getProviders(String json){ System.out.println(json);
	 * GeneralBean[] bankBeans = null; JSONObject jsObject = null; JSONObject
	 * jsObjectInternal = null; JSONArray jsArray = null; int lenght = 0; String
	 * description = ""; String clave = ""; String claveWS = ""; String path =
	 * ""; String compatible = ""; String compatiblevisa = ""; try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("proveedores");
	 * 
	 * lenght = jsArray.length();
	 * 
	 * bankBeans = new GeneralBean[lenght];
	 * 
	 * for(int i = 0; i < lenght; i++){
	 * 
	 * jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * 
	 * if(jsObjectInternal.has("descripcion")){ description =
	 * jsObjectInternal.getString("descripcion"); } clave =
	 * jsObjectInternal.getString("clave"); claveWS =
	 * jsObjectInternal.getString("claveWS"); path =
	 * jsObjectInternal.getString("path"); compatible =
	 * jsObjectInternal.getString("compatible"); compatiblevisa =
	 * jsObjectInternal.getString("tipotarjeta");
	 * 
	 * 
	 * System.out.println(clave); System.out.println(description);
	 * System.out.println(path); System.out.println(claveWS);
	 * System.out.println(compatible); System.out.println(compatiblevisa);
	 * 
	 * 
	 * 
	 * bankBeans[i] = new GeneralBean(description, clave, path,
	 * claveWS,compatible,compatiblevisa);
	 * 
	 * }
	 * 
	 * 
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); bankBeans = new GeneralBean[0]; }
	 * 
	 * return bankBeans; }
	 * 
	 * 
	 * 
	 * 
	 * public ConsultBean[] getConsults(String json){
	 * 
	 * ConsultBean[] consultBeans = null;
	 * 
	 * JSONObject jsObject = null; JSONArray jsArray = null; int until = 0;
	 * String user = ""; String provider = ""; String product = ""; String date
	 * = ""; String concept = ""; String amount = ""; String numAuthorization =
	 * ""; String codeError = ""; String carId = ""; String status = "";
	 * 
	 * try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("compras");
	 * 
	 * until = jsArray.length();
	 * 
	 * consultBeans = new ConsultBean[until];
	 * 
	 * for(int i = 0; i < until; i++){
	 * 
	 * JSONObject jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * user = jsObjectInternal.getString("usuario"); provider =
	 * jsObjectInternal.getString("proveedor"); product =
	 * jsObjectInternal.getString("producto"); date =
	 * jsObjectInternal.getString("fecha"); concept =
	 * jsObjectInternal.getString("concepto"); amount =
	 * jsObjectInternal.getString("cargo"); numAuthorization =
	 * jsObjectInternal.getString("no_autorizacion"); codeError =
	 * jsObjectInternal.getString("codigo_error"); carId =
	 * jsObjectInternal.getString("car_id"); status =
	 * jsObjectInternal.getString("status");
	 * 
	 * consultBeans[i] = new ConsultBean(user, provider, product, date, concept,
	 * amount, numAuthorization,codeError, carId, status);
	 * 
	 * }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); consultBeans = new ConsultBean[0]; return
	 * consultBeans; }
	 * 
	 * return consultBeans;
	 * 
	 * }
	 * 
	 * public boolean isUpdate(String json){
	 * 
	 * boolean isLogin = false; int msj = 0;
	 * 
	 * try {
	 * 
	 * JSONObject jsObject = new JSONObject(json);
	 * 
	 * msj = jsObject.getInt("resultado");
	 * 
	 * if(msj == 1){ isLogin = true; }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return isLogin; }
	 * 
	 * return isLogin;
	 * 
	 * }
	 * 
	 * public String getTerms(String json){
	 * 
	 * String msj = "";
	 * 
	 * try {
	 * 
	 * JSONObject jsObject = new JSONObject(json);
	 * 
	 * msj = jsObject.getString("Descripcion");
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); msj = "Informaci�n no disponible."; return msj; }
	 * 
	 * return msj;
	 * 
	 * }
	 * 
	 * public PurchaseBean getResultPurchase(String json){
	 * 
	 * PurchaseBean purchaseBean = new PurchaseBean(); String operation = "";
	 * String result = ""; String message = ""; String folio = "";
	 * 
	 * try {
	 * 
	 * JSONObject jsObject = new JSONObject(json);
	 * 
	 * // operation = jsObject.getString("operacion");
	 * if(jsObject.has("resultado")){ result = jsObject.getString("resultado");
	 * }
	 * 
	 * if(jsObject.has("mensaje")){ message = jsObject.getString("mensaje"); }
	 * 
	 * if(jsObject.has("folio")){ folio = jsObject.getString("folio"); }
	 * 
	 * 
	 * purchaseBean = new PurchaseBean(operation, result, message, folio);
	 * 
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace();
	 * 
	 * // result = jsObject.getString("resultado"); // message =
	 * jsObject.getString("mensaje"); // folio = jsObject.getString("folio");
	 * 
	 * purchaseBean.setResult(result); purchaseBean.setMessage(
	 * "Informaci�n no disponibe. Favor de intentar m�s tarde."); return
	 * purchaseBean; }
	 * 
	 * return purchaseBean;
	 * 
	 * }
	 * 
	 * public ProductBean[] getPromos(String json){
	 * 
	 * ProductBean[] productBeans = new ProductBean[0];
	 * 
	 * String clave = ""; String path = ""; String description = "";
	 * 
	 * JSONObject jsObject = null; JSONArray jsArray = null; int until = 0;
	 * 
	 * try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("promociones");
	 * 
	 * until = jsArray.length();
	 * 
	 * productBeans = new ProductBean[until];
	 * 
	 * for(int i = 0; i < until; i++){
	 * 
	 * JSONObject jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * clave = jsObjectInternal.getString("clave"); path =
	 * jsObjectInternal.getString("path"); description =
	 * jsObjectInternal.getString("descripcion");
	 * 
	 * productBeans[i] = new ProductBean(clave, path, description);
	 * 
	 * }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return productBeans; }
	 * 
	 * return productBeans;
	 * 
	 * }
	 * 
	 * 
	 * public TipoRecargaTag[] getTags(String json){
	 * 
	 * TipoRecargaTag[] productBeans = new TipoRecargaTag[0];
	 * 
	 * String clave = "";
	 * 
	 * String nombre = "";
	 * 
	 * JSONObject jsObject = null; JSONArray jsArray = null; int until = 0;
	 * 
	 * try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("tipoRecargaTag");
	 * 
	 * until = jsArray.length();
	 * 
	 * productBeans = new TipoRecargaTag[until];
	 * 
	 * for(int i = 0; i < until; i++){
	 * 
	 * JSONObject jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * clave = jsObjectInternal.getString("clave");
	 * 
	 * nombre = jsObjectInternal.getString("nombre");
	 * 
	 * productBeans[i] = new TipoRecargaTag(clave, nombre);
	 * 
	 * }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return productBeans; }
	 * 
	 * return productBeans;
	 * 
	 * }
	 * 
	 * 
	 * 
	 * public UsuarioTag[] getTagsbyUSer(String json){
	 * 
	 * UsuarioTag[] productBeans = new UsuarioTag[0]; String usuario=""; String
	 * tipotag=""; String etiqueta=""; String numero=""; String dv="";
	 * 
	 * JSONObject jsObject = null; JSONArray jsArray = null; int until = 0;
	 * 
	 * try {
	 * 
	 * jsObject = new JSONObject(json);
	 * 
	 * jsArray = jsObject.getJSONArray("usuarioTag");
	 * 
	 * until = jsArray.length();
	 * 
	 * productBeans = new UsuarioTag[until];
	 * 
	 * for(int i = 0; i < until; i++){
	 * 
	 * JSONObject jsObjectInternal = jsArray.getJSONObject(i);
	 * 
	 * usuario = jsObjectInternal.getString("usuario"); tipotag =
	 * jsObjectInternal.getString("tipotag"); etiqueta =
	 * jsObjectInternal.getString("etiqueta"); numero =
	 * jsObjectInternal.getString("numero"); dv =
	 * jsObjectInternal.getString("dv");
	 * 
	 * 
	 * productBeans[i] = new UsuarioTag(usuario, tipotag,etiqueta,numero,dv);
	 * 
	 * }
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); return productBeans; }
	 * 
	 * return productBeans;
	 * 
	 * }
	 * 
	 * public String getMontoInterjet(String json){ String monto = null;
	 * JSONObject jsonObject = null;
	 * 
	 * try{ jsonObject = new JSONObject(json); monto =
	 * jsonObject.optString("Monto"); } catch(JSONException e){
	 * e.printStackTrace(); return ""; }
	 * 
	 * return monto; }
	 * 
	 * public long getPurchaseInterjet(String json){ long id; JSONObject
	 * jsonObject = null;
	 * 
	 * try{ jsonObject = new JSONObject(json); id = jsonObject.optLong("id");
	 * }catch (JSONException e) { // TODO: handle exception return 0L; }
	 * 
	 * return id; }
	 */

}
