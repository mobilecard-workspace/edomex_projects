package com.addcel.gdf.model.connection.addcel.InfoThread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.base.Communicator;
import com.addcel.gdf.model.connection.addcel.base.HttpListener;
import com.addcel.gdf.model.connection.addcel.json.JSONParser;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilSecurity;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.password.PasswordUpdater;


public class Login implements HttpListener {

	private String url = Url.URL_LOGIN;

	private String password = null;
	private String json = null;
	private String jsonEncrypt = null;
	private String jsonEncrypt2 = null;

	private String user = null;
	
	private String post;
	//private HttpPoster poster;
	private Viewable viewable;
	private MainScreen mainScreen;
	private SplashScreen splashScreen;

	public Login(Viewable viewable, String user, String password) {

		setClass(viewable, user, password);
	}

	
	public Login(Viewable viewable, String user, String password, MainScreen mainScreen) {

		this.mainScreen = mainScreen;
		setClass(viewable, user, password);
	}

	
	private void setClass(Viewable viewable, String user, String password){

		this.viewable = viewable;
		splashScreen = SplashScreen.getInstance();

		try {

			if ((user != null) && (password != null)) {

				this.user = user.trim();
				this.password = password.trim();
				json = createJSon(user, password);
				String parse = UtilSecurity.parsePass(password);
				jsonEncrypt = UtilSecurity.aesEncrypt(parse, json);
				jsonEncrypt2 = UtilSecurity.mergeStr(jsonEncrypt, password);
				//this.post = "" + jsonEncrypt2;
				this.post = "json=" + jsonEncrypt2;
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			viewable.sendMessage("Error de codificación.");
		}
	}
	
	
	private String createJSon(String user, String password)
			throws UnsupportedEncodingException {

		StringBuffer json = new StringBuffer();

		this.user = user;
		
		json.append("{\"login\":\"").append(user);
		json.append("\",\"tipo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		json.append("\",\"imei\":\"").append(UtilBB.getImei());
		json.append("\",\"modelo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		json.append("\",\"software\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		json.append("\",\"key\":\"").append(UtilBB.getImei());
		json.append("\",\"password\":\"").append(password);
		json.append("\",\"passwordS\":\"").append(UtilSecurity.sha1(password));
		json.append("\"}");

		return json.toString();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {
				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpPost(this, this.url, this.post);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			viewable.sendMessage("No se pudo crear una conexión, intenta de nuevo por favor.");
		}
	}

	public void run() {

		connect();
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {}

	public void receiveHeaders(Vector _headers) {}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;

		try {

			sTemp = new String(response, 0, response.length, "UTF-8");

			sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(password), sTemp);

			synchronized (Application.getEventLock()) {

				splashScreen.remove();

				JSONParser jsParser = new JSONParser();

				if (jsParser.isLogin(sTemp)) {

					UserBean.nameLogin = user;
					UserBean.password = password;

					if (mainScreen == null){
						UiApplication.getUiApplication().popScreen((MainScreen)viewable);
					} else {
						
						viewable.sendMessage("Oprime la opcion nuevamente.");
						UiApplication.getUiApplication().popScreen((MainScreen)viewable);
					}

				} else if (jsParser.isLogin2(sTemp)) {
					
					PasswordUpdater passwordUpdater = new PasswordUpdater(null);
					UiApplication.getUiApplication().pushScreen(passwordUpdater);
				} else if (jsParser.isLogin3(sTemp)) {
					viewable.sendMessage("Intente de nuevo por favor.");
					UserBean.nameLogin = null;
				} else {
					viewable.sendMessage("Intente de nuevo por favor.");
					UserBean.nameLogin = null;
				}
			}

		} catch (UnsupportedEncodingException e) {
			viewable.sendMessage("Error al codificar la respuesta. Intenta de nuevo por favor.");
			e.printStackTrace();
		}
	}

	public void getMessageError(String error) {

		final String errors = error;
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Dialog.alert(errors);
			}
		});
	}
}
