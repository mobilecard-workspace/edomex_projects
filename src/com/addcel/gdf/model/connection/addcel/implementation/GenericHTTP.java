package com.addcel.gdf.model.connection.addcel.implementation;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;

public class GenericHTTP extends HttpListener {

	public GenericHTTP(String post, String url, Viewable viewable) {
		super(post, url, viewable);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);

		json = AddcelCrypto.decryptSensitive(json);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}