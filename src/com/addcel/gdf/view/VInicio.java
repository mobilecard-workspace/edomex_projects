package com.addcel.gdf.view;

import java.util.Vector;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.Comunicado;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.InfoThread.CreditInfoThread;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ComunicadosRichTextField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.VIndex;
import com.addcel.gdf.view.util.UtilColor;

public class VInicio extends CustomMainScreen implements FieldChangeListener, Viewable{

	private SlimSelectedButtonField registro;
	private SlimSelectedButtonField sesion;
	
	private SlimSelectedButtonField consulta;
	private SlimSelectedButtonField tramites;

	private SlimSelectedButtonField anteriorGeneral;
	private SlimSelectedButtonField siguienteGeneral;

	private ComunicadosRichTextField avisosGenerales;
	
	private SplashScreen splashScreen;
	
	private Vector comunicados = new Vector();
	
	public VInicio() {

		super("Edomex", true);

		splashScreen = SplashScreen.getInstance();

		Background background01 = BackgroundFactory.createSolidBackground(UtilColor.COLOR_RED);

		registro = new SlimSelectedButtonField("Registro", ButtonField.CONSUME_CLICK);
		sesion = new SlimSelectedButtonField("Ingresar", ButtonField.CONSUME_CLICK);
		registro.setChangeListener(this);
		sesion.setChangeListener(this);

		CustomButtonFieldManager buttonFieldManager = new CustomButtonFieldManager(registro, sesion);
		buttonFieldManager.add(sesion);
		buttonFieldManager.add(registro);
		add(buttonFieldManager);

		consulta = new SlimSelectedButtonField("Consultar pagos", ButtonField.CONSUME_CLICK);
		tramites = new SlimSelectedButtonField("Pago en L�nea", ButtonField.CONSUME_CLICK);
		consulta.setChangeListener(this);
		tramites.setChangeListener(this);

		CustomButtonFieldManager buttonFieldManager01 = new CustomButtonFieldManager(consulta, tramites);
		buttonFieldManager01.add(consulta);
		buttonFieldManager01.add(tramites);

		add(buttonFieldManager01);

		add(new LabelField(""));

		LabelField field01 = new LabelField("Avisos al Contribuyente.", LabelField.USE_ALL_WIDTH){
		    protected void paint(Graphics g){ 
		        g.setColor(Color.WHITE);
		        super.paint(g);
		    }
		};

		field01.setBackground(background01);
		//add(field01);

		avisosGenerales = new ComunicadosRichTextField();

		Background background = BackgroundFactory.createSolidBackground(UtilColor.EDIT_FIELD_BACKGROUND);
		avisosGenerales.setBackground(background);
		//add(avisosGenerales);

		anteriorGeneral = new SlimSelectedButtonField("Anterior", ButtonField.CONSUME_CLICK);
		siguienteGeneral = new SlimSelectedButtonField("Siguiente", ButtonField.CONSUME_CLICK);

		anteriorGeneral.setChangeListener(this);
		siguienteGeneral.setChangeListener(this);
		
		CustomButtonFieldManager buttonFieldManagerGeneral = new CustomButtonFieldManager(anteriorGeneral, siguienteGeneral);
		buttonFieldManagerGeneral.add(anteriorGeneral);
		buttonFieldManagerGeneral.add(siguienteGeneral);

		add(new LabelField(""));

		add(new LabelField(""));

		JSONObject jsonObject = new JSONObject();
		
		try {
			
			jsonObject.put("idAplicacion", 1);
			
			String post = jsonObject.toString();
			
			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_COMUNICADOS, this);
			genericHTTP.run();

		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informaci�n");
		}
	}


	public void fieldChanged(Field field, int context) {

		if (field == registro){
			CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores");
			creditInfoThread.start();
		} else if (field == sesion){
			UiApplication.getUiApplication().pushScreen(new VAutenticacion());
		} else if (field == tramites){
			UiApplication.getUiApplication().pushScreen(new VTramites("Departamentos"));
		} else if (field == anteriorGeneral){
			Comunicado comunicado = avisosGenerales.getComunicado();
			
			int index = comunicados.lastIndexOf(comunicado);
			int size = comunicados.size();

			if (index > 0){
				comunicado = (Comunicado)comunicados.elementAt(index - 1);
				avisosGenerales.setComunicado(comunicado);
			}
			
		} else if (field == siguienteGeneral){
			
			Comunicado comunicado = avisosGenerales.getComunicado();
			
			int index = comunicados.lastIndexOf(comunicado);
			int size = comunicados.size();
			
			if ((index + 1) < size){
				comunicado = (Comunicado)comunicados.elementAt(index + 1);
				avisosGenerales.setComunicado(comunicado);
			}
		} else if (field == consulta){
			UiApplication.getUiApplication().pushScreen(new VIndex("Consultar pagos"));
		}
	}


	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		if (jsObject.has("comunicados")){
			
			try {
				JSONArray jsonArray = jsObject.getJSONArray("comunicados");
				
				int length = jsonArray.length();
				
				for(int index = 0; index < length; index++){
					
					JSONObject object = (JSONObject) jsonArray.get(index);
					
					Comunicado comunicado = new Comunicado();
					
					comunicado.setContenido(object.optString("contenido"));
					comunicado.setFgColor(object.optString("fgColor"));
					comunicado.setBgColor(object.optString("bgColor"));
					comunicado.setTamFuente(object.optString("tamFuente"));
					
					comunicados.addElement(comunicado);
				}
				
				if (comunicados.size() > 0){
					Comunicado comunicado = (Comunicado)comunicados.elementAt(0);
					avisosGenerales.setComunicado(comunicado);
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
		}
	}


	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
		
	}
}
