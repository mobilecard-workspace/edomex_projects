package com.addcel.gdf.view;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.ColorPasswordEditField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;

public class VPagoSAbierto extends CustomMainScreen {
	
	private ColorLabelField passwordTxt;
	private ColorLabelField cvv2Txt;
	
	private ColorPasswordEditField passwordEdit;
	private ColorEditField cvv2Edit;
	
	private ColorLabelField servicioLabel;
	private ColorLabelField montoLabel;
	private ColorLabelField lineaCapturaLabel;
	
	private HighlightsLabelField servicioTxt;
	private HighlightsLabelField montoTxt;
	private HighlightsLabelField lineaCapturaTxt;

	private SlimSelectedButtonField confirma = null;

	public VPagoSAbierto(String title) {
		
		super("", true);
		
		passwordTxt = new ColorLabelField("Contraseņa: ", LabelField.NON_FOCUSABLE);
		cvv2Txt = new ColorLabelField("CVV2: ", LabelField.NON_FOCUSABLE);
		
		passwordEdit = new ColorPasswordEditField("", "", 12, EditField.FILTER_DEFAULT);
		cvv2Edit = new ColorEditField("", "", 3, EditField.FILTER_INTEGER);
		
		servicioLabel = new ColorLabelField("Servicio: ", LabelField.NON_FOCUSABLE);
		montoLabel = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);
		lineaCapturaLabel = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE);

		servicioTxt = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		montoTxt = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		lineaCapturaTxt = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		
		addHorizontalFiels(servicioLabel, servicioTxt);
		addHorizontalFiels(montoLabel, montoTxt);
		addHorizontalFiels(lineaCapturaLabel, lineaCapturaTxt);
		
		add(cvv2Txt);
		add(cvv2Edit);
		
		add(passwordTxt);
		add(passwordEdit);
		
		confirma = new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		add(new LabelField());
		add(confirma);
	}
	
	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}

}
