package com.addcel.gdf.view;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ListField;
import net.rim.device.api.ui.component.ListFieldCallback;

import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.util.UtilColor;

public class VTramites extends CustomMainScreen implements ListFieldCallback {

	protected String listMembers[] = {
			"Vehiculos"
			//,"Empresas"
			//,"Inmuebles"
	};

	public VTramites(String title) {

		super(title, true);
		
		ListField mylist = new ListField() {

			protected boolean touchEvent(TouchEvent message) {
				switch (message.getEvent()) {
				case TouchEvent.CLICK:
					fieldChangeNotify(1);
					execute();
				case TouchEvent.UNCLICK:
					return true;
				}
				return super.touchEvent(message);
			}

			protected boolean navigationClick(int status, int time) {
				fieldChangeNotify(0);
				execute();
				return super.navigationClick(status, time);
			}

			protected boolean keyChar(char character, int status, int time) {
				if (character == Keypad.KEY_ENTER) {
					fieldChangeNotify(0);
					execute();
					return true;
				}

				return super.keyChar(character, status, time);
			}

			
			private void execute(){
			
				int index = getSelectedIndex();
				
				switch (index) {
				case 0:
					UiApplication.getUiApplication().pushScreen(new VTramitesVehiculos("Vehiculos"));
					break;
				case 1:
					UiApplication.getUiApplication().pushScreen(new VTramitesEmpresas("Empresas"));
					break;
				case 2:
					UiApplication.getUiApplication().pushScreen(new VTramitesOtros("Inmuebles"));
					break;
				}
				
			}
			
		};
		mylist.setCallback(this);
		mylist.setSize(listMembers.length);

		add(mylist);
	}
	
	
	public void drawListRow(ListField listField, Graphics graphics, int index,
			int y, int width) {
		
		if (isFocus()){
			graphics.setColor(UtilColor.LABEL_FIELD_STRING);
		} else {
			graphics.setColor(UtilColor.LABEL_FIELD_STRING);
		}

		graphics.drawText(this.listMembers[index], 0, y, 50);
	}

	public Object get(ListField listField, int index) {
		return listMembers[index];
	}

	public int getPreferredWidth(ListField listField) {
		return 200;
	}

	public int indexOfList(ListField listField, String prefix, int start) {
		return -1;
	}
}


/*
"TR�MITES Y SERVICIOS",
"PROGRAMAS SOCIALES",
"DELEGACIONES",
"EMPLEO",
"CULTURA Y TURISMO",
"TRANSPORTE Y VIALIDAD",
"OBRAS P�BLICAS",
"EDUCACI�N",
"MEDIO AMBIENTE",
"HOGAR Y VIVIENDA",
"SALUD Y DEPORTE",
"SEGURIDAD Y JUSTICIA",
"EMPRESAS Y NEGOCIOS",
"PROTECCI�N CIVIL",
"EQUIDAD Y DERECHOS HUMANOS",
"GOBIERNO",
"TRANSPARENCIA"	
*/