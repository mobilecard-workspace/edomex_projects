package com.addcel.gdf.view.consulta;

import java.util.Calendar;
import java.util.Date;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.ObjectChoiceField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.tenencia.LTenencia;

public class VIndex extends CustomMainScreen implements FieldChangeListener, Viewable {

	private final String nomina = "Nomina";
	private final String tenencia = "Tenencia";
	private final String infraccion = "Infracciones";
	private final String predial = "Pago de predial";
	private final String pagoAgua = "Pago de agua";
	
	private ObjectChoiceField serviciosChoiceField;
	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField cancelar = null;
	private DateField dateField;


	
	public VIndex(String title) {

		super(title, true);

		//String[] servicios = { nomina, tenencia, infraccion, predial, pagoAgua };
		String[] servicios = {"Seleccionar servicio", tenencia};
		
		serviciosChoiceField = new ObjectChoiceField("Selecciona operaci�n", servicios, 1);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM  yyyy");
		dateField = new DateField("Mes y A�o", System.currentTimeMillis(), dateFormat, DateField.DATE);
		
		consultar = new SlimSelectedButtonField("Consultar",
				ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		
		cancelar = new SlimSelectedButtonField("Cancelar",
				ButtonField.CONSUME_CLICK);
		cancelar.setChangeListener(this);
		
		add(serviciosChoiceField);
		add(dateField);

		CustomButtonFieldManager buttonFieldManager01 = new CustomButtonFieldManager(consultar, cancelar);
		buttonFieldManager01.add(consultar);
		buttonFieldManager01.add(cancelar);

		add(buttonFieldManager01);
	}


	public void sendMessage(String message) {
		Dialog.alert(message);
	}

	
    public Date addDays(Date d, long days){
    	
        d.setTime(d.getTime() + days * 1000 * 60 * 60 * 24);
        return d;
    }
	
	
	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == consultar){
			
			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {

				int index = serviciosChoiceField.getSelectedIndex();
				long dateEnd = dateField.getDate();

				index++;

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date(dateEnd));
				Date date = calendar.getTime();
				
				date = addDays(date, -7);
				
				long hoy = System.currentTimeMillis();
				
				boolean result = dateEnd < hoy;
				
				if (result){
					
					String dateFormatStart = UtilDate.getYYYY_MM_DDFromLong(date.getTime());
					String dateFormatEnd = UtilDate.getYYYY_MM_DDFromLong(dateEnd);
					
					JSONObject jsonObject = new JSONObject();
					
					try {

						calendar.setTime(new Date(dateEnd));

						int mes = calendar.get(Calendar.MONTH) + 1;
						int anio = calendar.get(Calendar.YEAR);

						jsonObject.put("idUser",  UserBean.idLogin);
						jsonObject.put("idProveedor", 23);
						jsonObject.put("mes", String.valueOf(mes));
						jsonObject.put("anio", String.valueOf(anio));

						String post = jsonObject.toString();
						
						String data = "json=" + AddcelCrypto.encryptHard(post);
						final GenericHTTPHard pagoPlacas = new GenericHTTPHard(this, Url.URL_EDOMEX_CONSULTA_PAGOS, data);
						
						UiApplication.getUiApplication().invokeAndWait(new Runnable() { 
							
							public void run() { 
								
								pagoPlacas.run();
							}
						});
						
					} catch (JSONException e) {
						Dialog.alert("Error al interpretar los datos.");
						e.printStackTrace();
					}

				} else {
					
					Dialog.alert("Verificar la fecha");
				}
			}
		}
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		int error = jsObject.optInt("idError");
		
		if (error == 0){
			
			if (jsObject.has("pagos")){
				UiApplication.getUiApplication().pushScreen(new LTenencia("Pagos", jsObject));
			}
		} else {

			String msjError = jsObject.optString("mensajeError");
			Dialog.alert(msjError);
		}
	}
}


// TODO Reemplazar
/*
String json = "{\"consultaPredial\":[{\"bimestre\":\"04\",\"concepto\":\"Impuesto Predial\",\"cuentaP\":\"033109120023\",\"intImpuesto\":2543,\"linCap\":\"80033109120029J9AN0N\",\"reduccion\":\"0\",\"vencimiento\":\"2013-09-01\",\"id_bitacora\":110002,\"id_usuario\":13709053,\"id_producto\":0,\"no_autorizacion\":\"008002\",\"fechaPago\":\"2013-08-02 23:36:38.0\",\"statusPago\":1},{\"bimestre\":\"04\",\"concepto\":\"Impuesto Predial:001\",\"cuentaP\":\"033109120023\",\"intImpuesto\":2543,\"linCap\":\"00000109120029J9AAA\",\"reduccion\":\"15.80\",\"vencimiento\":\"2013-09-02\",\"id_bitacora\":110003,\"id_usuario\":13709053,\"id_producto\":0,\"no_autorizacion\":\"008003\",\"fechaPago\":\"2013-08-02 23:35:16.0\",\"statusPago\":1}]}";

try {
	
	jsObject = new JSONObject(json);
	
} catch (JSONException e) {

	e.printStackTrace();
}
*/


// mas fecha del pago + datos que introduce el cliente
//splashScreen.start();

/*
String post = jsonObject.toString();
String data = AddcelCrypto.encryptHard(post);
GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_COMUNICADOS, this);
genericHTTP.run();						
 */

/*
public VIndex(String title) {
	super(title);

	final String[] MONTHS = { "Enero", "Febrero", "Marzo", "Abril", "Mayo",
			"Junio", "Julio", "Agosto", "Septiembre", "Octubre",
			"Noviembre", "Diciembre" };

	final String[] YEARS = UtilDate.getYears(-10);

	SpinBoxField monthSpinBox = new TextSpinBoxField(MONTHS);
	SpinBoxField yearsSpinBox = new TextSpinBoxField(YEARS);

	SpinBoxFieldManager spinBoxManager = new SpinBoxFieldManager();
	spinBoxManager.setVisibleRows(3);
	spinBoxManager.setRowHeight(40);

	spinBoxManager.add(monthSpinBox);
	spinBoxManager.add(yearsSpinBox);
	spinBoxManager.setClickToLock(true);

	String[] servicios = { nomina, tenencia, infraccion, predial, pagoAgua };

	serviciosChoiceField = new ObjectChoiceField("Servicios", servicios, 0);
	dateFieldEnd = new DateField("Fin", System.currentTimeMillis(),
			DateField.DATE);
	consultar = new SlimSelectedButtonField("Consultar",
			ButtonField.CONSUME_CLICK);
	consultar.setChangeListener(this);

	add(serviciosChoiceField);
	add(spinBoxManager);
	add(consultar);
}
*/