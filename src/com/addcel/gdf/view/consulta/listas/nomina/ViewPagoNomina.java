package com.addcel.gdf.view.consulta.listas.nomina;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.util.UtilNumber;

public class ViewPagoNomina extends CustomMainScreen implements FieldChangeListener, Viewable {



	
/*	
	private LabelField textRFC;
	private LabelField textRemuneraciones;
	private LabelField textTrabajadores;

	private ColorEditField editRFC;
	private ColorEditField editRemuneraciones;
	private ColorEditField editTrabajadores;
	
	private SlimSelectedButtonField enviar = null;
*/
	
	
	private LabelField textRemuneracionesPago;
	private LabelField textImpuesto;
	private LabelField textImpuestoActualizado;
	private LabelField textRecargos;
	private LabelField textTotal;
	private LabelField textLineaCaptura;
	private LabelField textVigencia;
	
	private HighlightsLabelField showRemuneracionesPago;
	private HighlightsLabelField showImpuesto;
	private HighlightsLabelField showImpuestoActualizado;
	private HighlightsLabelField showRecargos;
	private HighlightsLabelField showTotal;
	private HighlightsLabelField showLineaCaptura;
	private HighlightsLabelField showVigencia;
	
	private SlimSelectedButtonField consultar = null;


	private int indexDerecho = 0;
	private HorizontalFieldManager fieldManagerDerecho = null;
	private boolean isDerecho = true;
	
	private boolean listoPagar = false;
	private JSONObject jsData = null;
	
	private ListaNomina listaNomina;
	
	private SplashScreen splashScreen;

	public ViewPagoNomina(String title, ListaNomina listaNomina) {

		super(title, true);
		
		splashScreen = SplashScreen.getInstance();
		
		this.listaNomina = listaNomina;
		
		consultar = new SlimSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);

		consultar.setChangeListener(this);

		textRemuneracionesPago = new ColorLabelField("Remuneraciones grabadas: ", LabelField.NON_FOCUSABLE);
		textImpuesto = new ColorLabelField("Impuesto: ", LabelField.NON_FOCUSABLE);
		textImpuestoActualizado = new ColorLabelField("Actualizacion: ", LabelField.NON_FOCUSABLE);
		textRecargos = new ColorLabelField("Recargos: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineaCaptura = new ColorLabelField("Linea de Captura: ", LabelField.NON_FOCUSABLE);
		textVigencia = new ColorLabelField("Vigencia: ", LabelField.NON_FOCUSABLE);

		showRemuneracionesPago = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showImpuesto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showImpuestoActualizado = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showRecargos = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showVigencia = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		add(new NullField());

		addHorizontalFiels(textRemuneracionesPago, showRemuneracionesPago);
		addHorizontalFiels(textImpuesto, showImpuesto);
		addHorizontalFiels(textImpuestoActualizado, showImpuestoActualizado);
		addHorizontalFiels(textRecargos, showRecargos);
		addHorizontalFiels(textTotal, showTotal);
		addHorizontalFiels(textVigencia, showVigencia);

		add(textLineaCaptura);
		add(showLineaCaptura);		
		add(consultar);
		
		showRemuneracionesPago.setText(UtilNumber.formatCurrency(listaNomina.getRemuneraciones()));
		showImpuesto.setText(UtilNumber.formatCurrency(listaNomina.getImpuesto()));
		showImpuestoActualizado.setText(UtilNumber.formatCurrency(listaNomina.getImpuesto_actualizado()));
		showRecargos.setText(UtilNumber.formatCurrency(listaNomina.getRecargos()));
		showTotal.setText(UtilNumber.formatCurrency(listaNomina.getTotalPago()));
		showLineaCaptura.setText(listaNomina.getLinea_captura());
		showVigencia.setText(listaNomina.getVigencia());
		
		
		
		
	}

	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	public void fieldChanged(Field field, int context) {

		if(field == consultar){

			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("id_bitacora", listaNomina.getId_bitacora());
				jsonObject.put("id_producto", DTO.STATUS_NOMINA);
				jsonObject.put("id_usuario", UserBean.idLogin);
				//jsonObject.put("id_aplicacion", DTO.STATUS_NOMINA);

				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptHard(post);

				splashScreen.start();
				
				GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
				genericHTTP.run();
				
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al recopilar la informacion");
			}
		}
	}


	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			sendMessage(mensaje);
		}
	}

	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}

	
	
	private void borrarCamposActualizacion(){
		
		if (isDerecho){
			this.delete(fieldManagerDerecho);
			isDerecho = false;
		}
		
		this.invalidate();
	}
	
	
	private void agregarCamposActualizacion(){
		
		if (!isDerecho){
			this.insert(fieldManagerDerecho, indexDerecho);
			isDerecho = true;
		}
		
		this.invalidate();
	}

}	


