package com.addcel.gdf.view.consulta.listas.tenencia;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.common.Adapter;
import com.addcel.gdf.view.consulta.listas.tenencia.dto.ListaTenencia;
import com.addcel.gdf.view.util.UtilNumber;


public class ViewPagoTenencia extends CustomMainScreen implements FieldChangeListener, Viewable {

	private LabelField textNombre;
	private LabelField textEmail;
	private LabelField textFecha;
	private LabelField textProducto;
	private LabelField textPlaca;
	private LabelField textIdBitacora;
	private LabelField textReferencia;
	private LabelField textLinea;
	private LabelField textTotal;

	private HighlightsLabelField editNombre;
	private HighlightsLabelField editEmail;
	private HighlightsLabelField editFecha;
	private HighlightsLabelField editProducto;
	private HighlightsLabelField editPlaca;
	private HighlightsLabelField editIdBitacora;
	private HighlightsLabelField editReferencia;
	private HighlightsLabelField editLinea;
	private HighlightsLabelField editTotal;
	
	private CustomSelectedButtonField consultar = null;
	private ListaTenencia listaTenencia;
	private SplashScreen splashScreen;
	
	public ViewPagoTenencia(String title, Adapter adapter) {
		
		super(title, true);
		
		this.listaTenencia = (ListaTenencia)adapter;
		
		splashScreen = SplashScreen.getInstance();
		addComponents(listaTenencia);
	}
	
	
	private void addComponents(ListaTenencia listaTenencia){

		textNombre = new ColorLabelField("Nombre: ", LabelField.NON_FOCUSABLE);
		textEmail = new ColorLabelField("Email: ", LabelField.NON_FOCUSABLE);
		textFecha = new ColorLabelField("Fecha: ", LabelField.NON_FOCUSABLE);

		textProducto = new ColorLabelField("Servicio: ", LabelField.NON_FOCUSABLE);		
		textPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
		textIdBitacora = new ColorLabelField("Folio: ", LabelField.NON_FOCUSABLE);
		
		textReferencia = new ColorLabelField("Autorizaci�n: ", LabelField.NON_FOCUSABLE);
		textLinea = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);

		editNombre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editEmail = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editFecha = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		editProducto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editPlaca = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editIdBitacora = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editReferencia = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLinea = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		consultar = new CustomSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);

		add(new NullField());

		addHorizontalFiels(textProducto, editProducto);
		//addHorizontalFiels(textEmail, editEmail);
		addHorizontalFiels(textIdBitacora, editIdBitacora);
		addHorizontalFiels(textReferencia, editReferencia);
		addHorizontalFiels(textPlaca, editPlaca);
		addHorizontalFiels(textNombre, editNombre);
		addHorizontalFiels(textFecha, editFecha);
		addHorizontalFiels(textTotal, editTotal);
		
		add(textLinea);
		add(editLinea);
		//addHorizontalFiels(, );
		
		add(consultar);

		if (listaTenencia != null){

			String total = UtilNumber.formatCurrency(listaTenencia.getTotalPago());
			String lineacaptura = listaTenencia.getLinea_captura();
			
			editNombre.setText(listaTenencia.getNombre());
			editEmail.setText(listaTenencia.getEmail());
			editFecha.setText(listaTenencia.getFecha());
			editProducto.setText(listaTenencia.getProducto());
			editPlaca.setText(listaTenencia.getPlaca());
			editIdBitacora.setText(listaTenencia.getIdBitacora());
			editReferencia.setText(listaTenencia.getReferencia());
			editLinea.setText(listaTenencia.getLinea());
			editTotal.setText(total);
			
		} else {
			Dialog.alert("No se recibi� informaci�n.");
		}
	}


	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}

	
	
	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea seleccionar otra direcci�n de mail?", new String[] {"Cambiar", "Continuar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		}
	}
	
	
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("idBitacora", listaTenencia.getIdBitacora());
			jsonObject.put("idProveedor", 23);
			jsonObject.put("idUser", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			splashScreen.start();

			String post = jsonObject.toString();

			String data = "json=" + AddcelCrypto.encryptHard(post);
			splashScreen.start();
			GenericHTTPHard genericHTTP = new GenericHTTPHard(this, Url.URL_EDOMEX_REENVIO, data);
			genericHTTP.run();
			
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	
	
	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("idError")){
			
			String mensaje = jsObject.optString("mensajeError");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}
/*
	public void fieldChanged(Field field, int context) {

		if(field == consultar){

			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("idBitacora", listaTenencia.getIdBitacora());
				jsonObject.put("idProveedor", 23);
				jsonObject.put("idUser", UserBean.idLogin);
				String post = jsonObject.toString();

				String data = "json=" + AddcelCrypto.encryptHard(post);
				splashScreen.start();
				GenericHTTPHard genericHTTP = new GenericHTTPHard(this, Url.URL_EDOMEX_REENVIO, data);
				genericHTTP.run();
				
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al recopilar la informacion");
			}
		}
	}


	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		String msjError = jsObject.optString("mensajeError");

		sendMessage(msjError);

	}
*/

	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}
}





/*

	private HighlightsLabelField editLC;
	private HighlightsLabelField editVigencia;

	private HighlightsLabelField editTenencia;
	private HighlightsLabelField editTenActualizacion;
	private HighlightsLabelField editTenRecargo;
	private HighlightsLabelField editDerecho;
	private HighlightsLabelField editDerRecargo;
	private HighlightsLabelField editDerActualizacion;
	private HighlightsLabelField editTotalDerecho;
	
	private HighlightsLabelField editTotal;
	private HighlightsLabelField editLineacaptura;
	
	
	Nombre
	Email
	Fecha
	Producto
	Placa
	IdBitacora
	Referencia
	Linea
	Total

*/