package com.addcel.gdf.view.consulta.listas.tenencia;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;
import com.addcel.gdf.view.consulta.listas.tenencia.dto.ListaTenencia;

public class LTenencia extends CustomMainScreen {

	public LTenencia(String title, JSONObject jsObject) {
		
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("pagos");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject) jsonArray.get(index);

				ListaTenencia listaTenencia = new ListaTenencia();

				listaTenencia.setLinea_captura(jsonObject.optString("linea"));
				listaTenencia.setId_producto(jsonObject.optString("placa"));
				listaTenencia.setFechaPago(jsonObject.optString("fecha"));
				listaTenencia.setTotalPago(jsonObject.optString("total"));
				listaTenencia.setIdBitacora(jsonObject.optString("idBitacora"));
				
				listaTenencia.setReferencia(jsonObject.optString("referencia"));//autorizacion
				listaTenencia.setNombre(jsonObject.optString("nombre"));
				listaTenencia.setProducto(jsonObject.optString("producto"));
				listaTenencia.setMoneda(jsonObject.optString("moneda"));
				listaTenencia.setEmail(jsonObject.optString("email"));
				
				CustomGridField fieldManager = new CustomGridField(listaTenencia, DTO.STATUS_TENENCIA);

				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
