package com.addcel.gdf.view.consulta.listas.tenencia.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;


public class ListaTenencia implements Adapter {

	private String idBitacora;
	private String referencia;
	private String linea;
	private String placa;
	private String nombre;
	private String fecha;
	private String producto;
	private String total;
	private String moneda;
	private String email;
	
	
	
	
	
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFechaPago() {
		return fecha;
	}
	
	public void setFechaPago(String fecha) {
		this.fecha = fecha;
	}

	public String getTotalPago() {
		return total;
	}
	
	public void setTotalPago(String total) {
		this.total = total;
	}
	
	public String getId_producto() {
		return placa;
	}
	
	public void setId_producto(String placa) {
		this.placa = placa;
	}
	
	public String getLinea_captura() {
		return linea;
	}
	
	public void setLinea_captura(String linea) {
		this.linea = linea;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

}
