package com.addcel.gdf.view.consulta.listas.predial;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;

public class LPredial extends CustomMainScreen{


	public LPredial(String title, JSONObject jsObject) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaPredial");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject onbject = (JSONObject) jsonArray.get(index);
				
				ListaPredial listaPredial = new ListaPredial();
				
				listaPredial.setBimestre(onbject.optString("bimestre")); 
				listaPredial.setConcepto(onbject.optString("concepto"));
				listaPredial.setCuentaP(onbject.optString("cuentaP"));
				listaPredial.setIntImpuesto(onbject.optString("importe"));
				listaPredial.setLinea_captura(onbject.optString("linea_captura"));
				listaPredial.setReduccion(onbject.optString("reduccion"));
				listaPredial.setVencimiento(onbject.optString("vencimiento"));
				listaPredial.setId_bitacora(onbject.optString("id_bitacora"));
				listaPredial.setId_usuario(onbject.optString("id_usuario"));
				listaPredial.setId_producto(onbject.optString("id_producto"));
				listaPredial.setNo_autorizacion(onbject.optString("no_autorizacion"));
				listaPredial.setFechaPago(onbject.optString("fechaPago"));
				listaPredial.setStatusPago(onbject.optString("statusPago"));
				listaPredial.setTotal(onbject.optString("total"));
				listaPredial.setTotalPago(onbject.optString("totalPago"));
				
				//CustomGridFieldPredial fieldManager = new CustomGridFieldPredial(listaPredial);
				
				CustomGridField fieldManager = new CustomGridField(listaPredial, DTO.STATUS_PREDIAL);
				
				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
