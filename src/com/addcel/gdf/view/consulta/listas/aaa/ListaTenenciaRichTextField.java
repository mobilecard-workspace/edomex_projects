package com.addcel.gdf.view.consulta.listas.aaa;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.tenencia.dto.ListaTenencia;
import com.addcel.gdf.view.util.UtilColor;


public class ListaTenenciaRichTextField extends RichTextField {


	final public static int DATE_EVENTO = 0;
	final public static int DATE_PLACAS = 1;
	final public static int DATE_FECHA = 2;
	
	final private String evento = "Infracción: ";
	final private String placas = "Placas: ";
	final private String fecha = "Fecha: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	
	public ListaTenenciaRichTextField(ListaTenencia listaTenencia, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = (Display.getWidth()*9) / 20; 
		size = Display.getWidth();
		
		switch (type) {
		case ListaTenenciaRichTextField.DATE_EVENTO:
			title = evento;
			data = listaTenencia.getFechaPago();
			break;
		case ListaTenenciaRichTextField.DATE_PLACAS:
			title = placas;
			data = listaTenencia.getId_producto();
			break;
		case ListaTenenciaRichTextField.DATE_FECHA:
			title = fecha;
			data = listaTenencia.getFechaPago();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
