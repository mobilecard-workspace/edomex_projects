package com.addcel.gdf.view.consulta.listas.aaa;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.nomina.ViewPagoNomina;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.util.UtilColor;


public class CustomGridFieldNomina extends VerticalFieldManager implements Viewable {

	private ListaNomina listaNomina;
	
	public CustomGridFieldNomina(ListaNomina listaNomina) {
		super();
		
		this.listaNomina = listaNomina;
		
		add(new ListaNominaRichTextField(listaNomina, ListaNominaRichTextField.DATE_RFC));
		add(new ListaNominaRichTextField(listaNomina, ListaNominaRichTextField.DATE_MES));
		add(new ListaNominaRichTextField(listaNomina, ListaNominaRichTextField.DATE_ANIO));
		add(new ListaNominaRichTextField(listaNomina, ListaNominaRichTextField.DATE_REMUNERACIONES));
		add(new ListaNominaRichTextField(listaNomina, ListaNominaRichTextField.DATE_TRABAJADORES));

		add(new NullField());
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.LIST_FOCUS);
			graphics.setGlobalAlpha(180);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.LIST_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}	
	
	
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}


	private void showView(){
		
		
		UiApplication.getUiApplication().pushScreen(new ViewPagoNomina("Resumen", this.listaNomina));
		
	}


	public void setData(int request, JSONObject jsObject) {
	}


	public void sendMessage(String message) {
	}
}