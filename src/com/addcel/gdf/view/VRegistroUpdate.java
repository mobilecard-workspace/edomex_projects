package com.addcel.gdf.view;

import java.util.Date;
import java.util.Calendar;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.GeneralBean;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.model.connection.addcel.implementation.UpdateUser;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.UtilSecurity;
import com.addcel.gdf.utils.Utils;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;

public class VRegistroUpdate extends CustomMainScreen implements FieldChangeListener, Viewable{

	private LabelField lnombreUsuario;
	private LabelField lnumeroCelular01;
	//private LabelField lconfNumeroCelular;
	private LabelField lemail;
	//private LabelField lconfirmarEmail;
	private LabelField lproveedor;

	private LabelField lnombre;
	private LabelField lapellidoPaterno;
	private LabelField lapellidoMaterno;
	private LabelField lsexo;
	private LabelField lfechaNacimiento;

	//private LabelField lcurp;
	//private LabelField lrfc;

	private LabelField ltelefonoCasa;
	private LabelField ltelefonoOficina;

	private LabelField lestado;
	private LabelField lciudad;
	private LabelField lcalle;

	private LabelField lnumExterior;
	private LabelField lnumInterior;
	private LabelField lcolonia;
	private LabelField lCP;

	private LabelField lnumeroTarjeta;
	private LabelField ltipoTarjeta;
	//private LabelField ldomicilioTarjeta;

	private LabelField lAnioTarjeta;
	private LabelField lMesTarjeta;
	
	private EditField nombreUsuario;
	private EditField numeroCelular01;
	//private EditField confNumeroCelular;
	private EditField email;
	//private EditField confirmarEmail;
	private ObjectChoiceField proveedor;

	private EditField nombre;
	private EditField apellidoPaterno;
	private EditField apellidoMaterno;
	private ObjectChoiceField sexo;
	private DateField fechaNacimiento;

	//private EditField curp;
	//private EditField rfc;

	private EditField telefonoCasa;
	private EditField telefonoOficina;

	private ObjectChoiceField estado;
	private EditField ciudad;
	private EditField calle;

	private EditField numExterior;
	private EditField numInterior;
	private EditField colonia;
	private EditField CP;

	private EditField numeroTarjeta;
	private ObjectChoiceField tipoTarjeta;
	//private EditField domicilioTarjeta;

	private ObjectChoiceField anioTarjeta;
	private ObjectChoiceField mesTarjeta;

	public CheckboxField acceptTerms = null;
	
	private ButtonField terminos;
	private ButtonField aceptar;

	private GeneralBean[] EdoBeans = null;
	private GeneralBean[] bankBeans = null;
	private GeneralBean[] creditBeans = null;
	private GeneralBean[] providersBeans = null;
	
	private JSONObject jsObject;
	
	private int intProveedor;
	private int intSexo;
	private int intEstado;
	private int intTipoTarjeta;
	private int intMes;
	private int intAnio;
	
	private String anios[] = UtilDate.getYears(5);
	
	private String meses[] = {"enero", "febrero", "marzo", 
			  "abril", "mayo", "junio", "julio", 
			  "agosto", "septiembre", "octubre", 
			  "noviembre", "diciembre"};
	
	private String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
	
	
	public VRegistroUpdate(GeneralBean[] EdoBeans, GeneralBean[] bankBeans, GeneralBean[] creditBeans, GeneralBean[] providersBeans) {

		super("Actualizar registro", false);

		this.EdoBeans = EdoBeans;
		this.bankBeans = bankBeans;
		this.creditBeans = creditBeans;
		this.providersBeans = providersBeans;

		getValores();
	}

	
	private void getValores() {

		String password = UserBean.password;
		String nameLogin = UserBean.nameLogin;
		
		String json = "{\"login\":\""
				+ nameLogin
				+ "\",\"password\":" + "\"" + password
				+ "\"}";
		
		String jsonCrypto = UtilSecurity.aesEncrypt(Utils.parsePass(password), json);
		String n2 = Utils.mergeStr(jsonCrypto, password);

		n2 = "json=" + n2;
		UpdateUser updateUser = new UpdateUser(this, Url.URL_GET_USERDATA, n2, password);
		updateUser.run();
	}


	public void setData(int request, JSONObject jsObject) {

		if (jsObject.has("resultado")) {

			String mensaje = jsObject.optString("mensaje", null);

			Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null,
					Dialog.DEFAULT_CLOSE);

			int i = dialog.doModal();

			if (i == Dialog.OK) {
				UiApplication.getUiApplication().popScreen(this);
			}

		} else if (jsObject.has("mensaje")) {

			try {
				String value = jsObject.getString("mensaje");
				Dialog.alert(value);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Dialog.alert("Error al leer la respuesta.");
			}
		} else if (jsObject.has("consultaTerminosCondiciones")) {

			try {

				JSONArray jArray = jsObject.getJSONArray("consultaTerminosCondiciones");

				if (jArray.length() > 0) {

					JSONObject object = jArray.getJSONObject(0);
					String desc_termino = object.optString("desc_termino");
					Dialog.alert(desc_termino);
				}

			} catch (JSONException e) {
				Dialog.alert("Error al leer la respuesta.");
			}
		} else {

			this.jsObject = jsObject;

			setProveedor(jsObject);
			setSexo(jsObject);
			setEstado(jsObject);
			setTipoTarjeta(jsObject);
			setFechaTarjeta(jsObject);
			setNacimiento(jsObject);
			createInstances();
			setFields(jsObject);
			addVariables();
		}

	}
	
	
	private void setProveedor(JSONObject jsObject){
		
		intProveedor = jsObject.optInt("proveedor", -1) - 1;
		
		proveedor = new ObjectChoiceField("", providersBeans, intProveedor);
	}
	
	
	private void setSexo(JSONObject jsObject){
		
		String stringSexo = jsObject.optString("sexo", null);
		
		stringSexo = stringSexo.toLowerCase();
		
		if((stringSexo.equals("hombre"))||
		    (stringSexo.equals("masculino"))||
			(stringSexo.equals("m"))){
			intSexo = 2;
		} else {
			intSexo = 1;
		}

		sexo = new ObjectChoiceField("", sexos, intSexo);
	}
	
	
	private void setEstado(JSONObject jsObject){
		
		intEstado = jsObject.optInt("id_estado", -1) - 1;
		estado = new ObjectChoiceField("", EdoBeans, intEstado);
	}
	
	
	public void setTipoTarjeta(JSONObject jsObject){
		
		intTipoTarjeta = jsObject.optInt("tipotarjeta", -1) - 1;
		
		GeneralBean[] temp = new GeneralBean[2];
		
		temp[0] = creditBeans[0];
		temp[1] = creditBeans[1];
		
		tipoTarjeta = new ObjectChoiceField("", temp, intTipoTarjeta);
	}
	
	
	public void setFechaTarjeta(JSONObject jsObject){
		
		int tempIndex = 0;
		String stringFecha = jsObject.optString("vigencia", null);
		
		String mes = stringFecha.substring(0, 2);
		String anio = "20" + stringFecha.substring(3, 5);
		
		intMes = Integer.parseInt(mes) - 1;
		intAnio = Integer.parseInt(anio);
		
		int length = anios.length;
		
		for(int index = 0; index < length; index++){
			
			String year = anios[index];
			
			if (anio.equals(year)){
				tempIndex = index;
			}
		}

		mesTarjeta = new ObjectChoiceField("", meses, intMes);
		anioTarjeta = new ObjectChoiceField("", anios, tempIndex);
	}


	public void setNacimiento(JSONObject jsObject){
		
		String nacimiento = jsObject.optString("nacimiento", null);
		
		if (nacimiento != null){

			String anio = nacimiento.substring(0, 4);
			String mes = nacimiento.substring(5, 7);
			String dia = nacimiento.substring(8, 10);

			Calendar calendar = Calendar.getInstance();

			calendar.set(Calendar.YEAR, Integer.parseInt(anio));
			calendar.set(Calendar.MONTH, Integer.parseInt(mes) - 1);
			calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));

			Date fecha = calendar.getTime();

			fechaNacimiento = new DateField("", fecha.getTime(), DateField.DATE);
		}
	}
	
	
	private void createInstances() {

		lnombreUsuario = new ColorLabelField("Nombre de usuario:", LabelField.NON_FOCUSABLE);
		lnumeroCelular01 = new ColorLabelField("N�mero de celular:", LabelField.NON_FOCUSABLE);
		//lconfNumeroCelular = new ColorLabelField("Confirmar n�mero de celular:", LabelField.NON_FOCUSABLE);
		lemail = new ColorLabelField("Correo electr�nico:", LabelField.NON_FOCUSABLE);
		//lconfirmarEmail = new ColorLabelField("Confirmar correo electr�nico:", LabelField.NON_FOCUSABLE);
		lproveedor = new ColorLabelField("Proveedor:", LabelField.NON_FOCUSABLE);
		lnombre = new ColorLabelField("Nombre:", LabelField.NON_FOCUSABLE);
		lapellidoPaterno = new ColorLabelField("Apellido paterno:", LabelField.NON_FOCUSABLE);
		lapellidoMaterno = new ColorLabelField("Apellido materno:", LabelField.NON_FOCUSABLE);
		lsexo = new ColorLabelField("Sexo:", LabelField.NON_FOCUSABLE);
		lfechaNacimiento = new ColorLabelField("Fecha de nacimiento:", LabelField.NON_FOCUSABLE);

		//lcurp = new ColorLabelField("CURP:", LabelField.NON_FOCUSABLE);
		//lrfc = new ColorLabelField("RFC:", LabelField.NON_FOCUSABLE);

		ltelefonoCasa = new ColorLabelField("Tel�fono de casa:", LabelField.NON_FOCUSABLE);
		ltelefonoOficina = new ColorLabelField("Tel�fono de oficina:", LabelField.NON_FOCUSABLE);

		lestado = new ColorLabelField("Estado:", LabelField.NON_FOCUSABLE);
		lciudad = new ColorLabelField("Ciudad:", LabelField.NON_FOCUSABLE);
		lcalle = new ColorLabelField("Calle:", LabelField.NON_FOCUSABLE);

		lnumExterior = new ColorLabelField("N�mero exterior:", LabelField.NON_FOCUSABLE);
		lnumInterior = new ColorLabelField("N�mero interior:", LabelField.NON_FOCUSABLE);
		lcolonia = new ColorLabelField("Colonia:", LabelField.NON_FOCUSABLE);
		lCP = new ColorLabelField("C�digo postal:", LabelField.NON_FOCUSABLE);

		lnumeroTarjeta = new ColorLabelField("N�mero de tarjeta:", LabelField.NON_FOCUSABLE);
		ltipoTarjeta = new ColorLabelField("Tipo de tarjeta:", LabelField.NON_FOCUSABLE);
		//ldomicilioTarjeta = new ColorLabelField("Domicilio de estado de cuenta de tarjeta:", LabelField.NON_FOCUSABLE);
		
		nombreUsuario = new ColorEditField("", "");
		numeroCelular01 = new ColorEditField("", "");
		//confNumeroCelular = new ColorEditField("", "");
		email = new ColorEditField("", "");
		//confirmarEmail = new ColorEditField("", "");

		nombre = new ColorEditField("", "");
		apellidoPaterno = new ColorEditField("", "");
		apellidoMaterno = new ColorEditField("", "");

		//fechaNacimiento = new DateField("", new Date().getTime(), DateField.DATE);

		//curp = new ColorEditField("", "");
		//rfc = new ColorEditField("", "");

		telefonoCasa = new ColorEditField("", "");
		telefonoOficina = new ColorEditField("", "");

		ciudad = new ColorEditField("", "");
		calle = new ColorEditField("", "");

		numExterior = new ColorEditField("", "");
		numInterior = new ColorEditField("", "");
		colonia = new ColorEditField("", "");
		CP = new ColorEditField("", "");

		numeroTarjeta = new ColorEditField("", "");

		
		//domicilioTarjeta = new ColorEditField("", "");
		
		
		lAnioTarjeta =  new ColorLabelField("A�o Vencimiento:", LabelField.NON_FOCUSABLE); 
		lMesTarjeta =  new ColorLabelField("Mes Vencimiento:", LabelField.NON_FOCUSABLE);
		
		
		
		
		/*
		String anios[] = UtilDate.getYears(5);
		String meses[] = {"enero", "febrero", "marzo", 
				  "abril", "mayo", "junio", "julio", 
				  "agosto", "septiembre", "octubre", 
				  "noviembre", "diciembre"};
		String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
		
		
		estado = new ObjectChoiceField("", EdoBeans, 16);
		tipoTarjeta = new ObjectChoiceField("", creditBeans, 0);
		
		sexo = new ObjectChoiceField("", sexos, 0);
		mesTarjeta = new ObjectChoiceField("", meses, 6);
		anioTarjeta = new ObjectChoiceField("", anios, 0);
		*/
		
		
		
		acceptTerms = new CheckboxField("Acepto terminos y condiciones.", false);
		
		terminos =  new ButtonField("T�rminos y Condiciones", ButtonField.CONSUME_CLICK);
		terminos.setChangeListener(this);
		
		aceptar = new ButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		aceptar.setChangeListener(this);
	}

	
	private void setFields(JSONObject jsObject){
		
		nombreUsuario.setText(jsObject.optString("login", ""));
		numeroCelular01.setText(jsObject.optString("telefono", ""));
		email.setText(jsObject.optString("mail", ""));


		nombre.setText(jsObject.optString("nombre", ""));
		apellidoPaterno.setText(jsObject.optString("apellido", ""));
		apellidoMaterno.setText(jsObject.optString("materno", ""));

		//curp.setText(jsObject.optString("curp", ""));
		//rfc.setText(jsObject.optString("rfc", ""));

		telefonoCasa.setText(jsObject.optString("tel_casa", ""));
		telefonoOficina.setText(jsObject.optString("tel_oficina", ""));

		ciudad.setText(jsObject.optString("ciudad", ""));
		calle.setText(jsObject.optString("calle", ""));

		numExterior.setText(jsObject.optString("num_ext", ""));
		numInterior.setText(jsObject.optString("num_interior", ""));
		colonia.setText(jsObject.optString("colonia", ""));
		CP.setText(jsObject.optString("cp", ""));

		numeroTarjeta.setText(jsObject.optString("tarjeta", ""));
		//domicilioTarjeta.setText(jsObject.optString("domicilio_tarjeta", ""));
	}
	
	
	private void addVariables() {

		add(lnombreUsuario);
		add(nombreUsuario);
		add(lnumeroCelular01);
		add(numeroCelular01);
		//add(lconfNumeroCelular);
		//add(confNumeroCelular);
		add(lemail);
		add(email);
		//add(lconfirmarEmail);
		//add(confirmarEmail);
		add(lproveedor);
		add(proveedor);

		add(lnombre);
		add(nombre);
		add(lapellidoPaterno);
		add(apellidoPaterno);
		add(lapellidoMaterno);
		add(apellidoMaterno);
		add(lsexo);
		add(sexo);
		add(lfechaNacimiento);
		add(fechaNacimiento);

		//add(lcurp);
		//add(curp);
		//add(lrfc);
		//add(rfc);

		add(ltelefonoCasa);
		add(telefonoCasa);
		add(ltelefonoOficina);
		add(telefonoOficina);

		add(lestado);
		add(estado);
		add(lciudad);
		add(ciudad);
		add(lcalle);
		add(calle);

		add(lnumExterior);
		add(numExterior);
		add(lnumInterior);
		add(numInterior);
		add(lcolonia);
		add(colonia);
		add(lCP);
		add(CP);

		add(lnumeroTarjeta);
		add(numeroTarjeta);
		add(ltipoTarjeta);
		add(tipoTarjeta);
		
		add(lMesTarjeta);
		add(mesTarjeta);
		add(lAnioTarjeta);
		add(anioTarjeta);
		
		add(terminos);
		add(acceptTerms);

		add(aceptar);
	}

	private boolean verificarDatos() {

		boolean value = true;

		if (nombreUsuario.getText().length() == 0) {
			Dialog.alert("Falto introducir el usuario.");
			value = false;
		} else if (numeroCelular01.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de celular.");
			value = false;
		} else if (email.getText().length() == 0) {
			Dialog.alert("Falto introducir el correo electr�nico.");
			value = false;
		} else if (nombre.getText().length() == 0) {
			Dialog.alert("Falto introducir el nombre.");
			value = false;
		} else if (apellidoPaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido paterno.");
			value = false;
		} else if (apellidoMaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido materno.");
			value = false;
		} else if (telefonoCasa.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la casa.");
			value = false;
		} else if (telefonoOficina.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la oficina.");
			value = false;
		} else if (ciudad.getText().length() == 0) {
			Dialog.alert("Falto introducir la ciudad.");
			value = false;
		} else if (calle.getText().length() == 0) {
			Dialog.alert("Falto introducir la calle.");
			value = false;
		} else if (numExterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero exterior.");
			value = false;
		} else if (numInterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero interior.");
			value = false;
		} else if (colonia.getText().length() == 0) {
			Dialog.alert("Falto introducir la colonia.");
			value = false;
		} else if (CP.getText().length() == 0) {
			Dialog.alert("Falto introducir el C�digo postal.");
			value = false;
		} else if (numeroTarjeta.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de tarjeta.");
			value = false;
		} 

		return value;
	}

	public void fieldChanged(Field field, int context) {

		if (field == aceptar) {

			if (acceptTerms.getChecked()){
				
				if (verificarDatos()){
					crearRegistro();
				}
				
			} else {
				
				Dialog.alert("Debe de aceptar los terminos y condiciones.");
			}
			
		} else if (field == terminos){

			try {
				JSONObject jo = new JSONObject();

				jo.put("id_aplicacion", 16);
				jo.put("id_producto", 3);
				
				String post = jo.toString();

				String data = AddcelCrypto.encryptHard(post);

				GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_TERMS, this);
				genericHTTP.run();

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private void crearRegistro(){
		
		try {
			
		
			long lDate = fechaNacimiento.getDate();
			String dNacimiento = UtilDate.getYYYY_MM_DDFromLong(lDate);

			
			int numMonth = mesTarjeta.getSelectedIndex() + 1;

			String stringMonth = String.valueOf(numMonth);

			if (numMonth < 10) {
				stringMonth = "0" + stringMonth;
			}		
			
			String SAnioTarjeta = getDataFromChoice(anioTarjeta);
			SAnioTarjeta = SAnioTarjeta.substring((SAnioTarjeta.length() - 2), SAnioTarjeta.length());


			String sSexo = getDataFromChoice(sexo);
			sSexo = sSexo.substring(0, 1);

			String sProveedor = getDataFromChoice(proveedor);
			String sEstado = getDataFromChoice(estado);
			String sTipoTarjeta = getDataFromChoice(tipoTarjeta);

			jsObject.remove("login");
			jsObject.remove("telefono");
			jsObject.remove("mail");
			jsObject.remove("nombre");
			jsObject.remove("apellido");
			jsObject.remove("materno");
			jsObject.remove("curp");
			jsObject.remove("rfc");
			jsObject.remove("tel_casa");
			jsObject.remove("tel_oficina");
			jsObject.remove("ciudad");
			jsObject.remove("calle");
			jsObject.remove("num_ext");
			jsObject.remove("num_interior");
			jsObject.remove("colonia");
			jsObject.remove("cp");
			jsObject.remove("tarjeta");
			jsObject.remove("domicilio_tarjeta");
			jsObject.remove("password");
			
			
			jsObject.put("password", UserBean.password);
			
			jsObject.put("nacimiento", dNacimiento);
			jsObject.put("vigencia", stringMonth + "/" + SAnioTarjeta);
			jsObject.put("sexo", sSexo);
			
			jsObject.put("proveedor", sProveedor);
			jsObject.put("id_estado", sEstado);
			jsObject.put("tipotarjeta", sTipoTarjeta);
			
			
			jsObject.put("login", nombreUsuario.getText());
			jsObject.put("telefono", numeroCelular01.getText());
			jsObject.put("mail", email.getText());
			jsObject.put("nombre", nombre.getText());
			jsObject.put("apellido", apellidoPaterno.getText());
			jsObject.put("materno", apellidoMaterno.getText());

			jsObject.put("tel_casa", telefonoCasa.getText());
			jsObject.put("tel_oficina", telefonoOficina.getText());
			jsObject.put("ciudad", ciudad.getText());
			jsObject.put("calle", calle.getText());
			jsObject.put("num_ext", numExterior.getText());
			jsObject.put("num_interior", numInterior.getText());
			jsObject.put("colonia", colonia.getText());
			jsObject.put("cp", CP.getText());
			jsObject.put("tarjeta", numeroTarjeta.getText());

			String passTEMP = UserBean.passTEMP;

			String json = jsObject.toString();

			String jsonCrypto = UtilSecurity.aesEncrypt(Utils.parsePass(UserBean.password), json);

			jsonCrypto = "json=" +  Utils.mergeStr(jsonCrypto, UserBean.password);

			UpdateUser updateUser = new UpdateUser(this, Url.ADC_USER_UPDATE, jsonCrypto, UserBean.password);

			updateUser.run();

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	

	public void sendMessage(String message) {
	}
	

	private String getDataFromChoice(ObjectChoiceField choiceField){
		
		int iValue = choiceField.getSelectedIndex();
		Object oValue = (Object)choiceField.getChoice(iValue);
		
		String sValue = null;
		
		if (oValue instanceof GeneralBean){
			
			GeneralBean bean = (GeneralBean)oValue;
			sValue = bean.getClave();
		} else {
			
			sValue = (String)oValue;
		}

		return sValue;
	}
}

