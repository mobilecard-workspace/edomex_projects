package com.addcel.gdf.view;

import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.view.components.custom.CustomMainScreen;

public class VPagoWeb extends CustomMainScreen {


	public VPagoWeb(String title) {
		super("", true);
	}

	
	
	
	public void execute(String get){
	//public void execute(){
		
		BrowserFieldConfig config = new BrowserFieldConfig();
        config.setProperty(BrowserFieldConfig.ENABLE_COOKIES,Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.NAVIGATION_MODE,BrowserFieldConfig.NAVIGATION_MODE_POINTER);
        BrowserField browserField = new BrowserField(config); 
        browserField.requestContent(Url.URL_EDOMEX_PAGO + "?json=" + get);
        
        //browserField.requestContent("http://50.57.192.213:8080/GDFServicios/Procom3DSecureGDF" + "?json=" + get);
        
        
	    //browserField.requestContent("http://mobilecard.mx:8080/ProComGDF/prosa_gdf.jsp?" + "");
	    
	    add(browserField);
	}
}
