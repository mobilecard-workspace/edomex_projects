package com.addcel.gdf.view;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.GeneralBean;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.InfoThread.RegisterThread;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.model.connection.addcel.implementation.Terminos;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.UtilSecurity;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;

public class VRegistro extends CustomMainScreen implements FieldChangeListener, Viewable{

	LabelField lnombreUsuario;
	LabelField lnumeroCelular01;
	LabelField lconfNumeroCelular;
	LabelField lemail;
	LabelField lconfirmarEmail;
	LabelField lproveedor;

	LabelField lnombre;
	LabelField lapellidoPaterno;
	LabelField lapellidoMaterno;
	LabelField lsexo;
	LabelField lfechaNacimiento;

	LabelField lcurp;
	LabelField lrfc;
	//LabelField lplacas;
	//LabelField lcuentaPredial;

	LabelField ltelefonoCasa;
	LabelField ltelefonoOficina;

	LabelField lestado;
	LabelField lciudad;
	LabelField lcalle;

	LabelField lnumExterior;
	LabelField lnumInterior;
	LabelField lcolonia;
	LabelField lCP;

	LabelField lnumeroTarjeta;
	LabelField ltipoTarjeta;
	LabelField ldomicilioTarjeta;

	LabelField lAnioTarjeta;
	LabelField lMesTarjeta;
	
	
	
	EditField nombreUsuario;
	EditField numeroCelular01;
	EditField confNumeroCelular;
	EditField email;
	EditField confirmarEmail;
	ObjectChoiceField proveedor;

	EditField nombre;
	EditField apellidoPaterno;
	EditField apellidoMaterno;
	ObjectChoiceField sexo;
	DateField fechaNacimiento;

	EditField curp;
	EditField rfc;
	//EditField placas;
	//EditField cuentaPredial;

	EditField telefonoCasa;
	EditField telefonoOficina;

	ObjectChoiceField estado;
	EditField ciudad;
	EditField calle;

	EditField numExterior;
	EditField numInterior;
	EditField colonia;
	EditField CP;

	EditField numeroTarjeta;
	ObjectChoiceField tipoTarjeta;
	EditField domicilioTarjeta;

	ObjectChoiceField anioTarjeta;
	ObjectChoiceField mesTarjeta;
	
	
	public CheckboxField acceptTerms = null;
	
	ButtonField terminos;
	ButtonField aceptar;

	private GeneralBean[] EdoBeans = null;
	private GeneralBean[] bankBeans = null;
	private GeneralBean[] creditBeans = null;
	private GeneralBean[] providersBeans = null;
	
	
	public VRegistro(GeneralBean[] EdoBeans, GeneralBean[] bankBeans, GeneralBean[] creditBeans, GeneralBean[] providersBeans) {

		super("Registro", false);
		this.EdoBeans = EdoBeans;
		this.bankBeans = bankBeans;
		this.creditBeans = creditBeans;
		this.providersBeans = providersBeans;
		
		setVariables();
		addVariables();

	}

	private void setVariables() {

		lnombreUsuario = new ColorLabelField("Nombre de usuario:",
				LabelField.NON_FOCUSABLE);
		lnumeroCelular01 = new ColorLabelField("N�mero de celular:",
				LabelField.NON_FOCUSABLE);
		lconfNumeroCelular = new ColorLabelField("Confirmar n�mero de celular:",
				LabelField.NON_FOCUSABLE);
		lemail = new ColorLabelField("Correo electr�nico:", LabelField.NON_FOCUSABLE);
		lconfirmarEmail = new ColorLabelField("Confirmar correo electr�nico:",
				LabelField.NON_FOCUSABLE);
		lproveedor = new ColorLabelField("Proveedor:", LabelField.NON_FOCUSABLE);

		lnombre = new ColorLabelField("Nombre:", LabelField.NON_FOCUSABLE);
		lapellidoPaterno = new ColorLabelField("Apellido paterno:",
				LabelField.NON_FOCUSABLE);
		lapellidoMaterno = new ColorLabelField("Apellido materno:",
				LabelField.NON_FOCUSABLE);
		lsexo = new ColorLabelField("Sexo:", LabelField.NON_FOCUSABLE);
		lfechaNacimiento = new ColorLabelField("Fecha de nacimiento:",
				LabelField.NON_FOCUSABLE);

		lcurp = new ColorLabelField("CURP:", LabelField.NON_FOCUSABLE);
		lrfc = new ColorLabelField("RFC:", LabelField.NON_FOCUSABLE);
		//lplacas = new ColorLabelField("N�mero de placas:", LabelField.NON_FOCUSABLE);
		//lcuentaPredial = new ColorLabelField("Cuenta predial",LabelField.NON_FOCUSABLE);

		ltelefonoCasa = new ColorLabelField("Tel�fono de casa:",
				LabelField.NON_FOCUSABLE);
		ltelefonoOficina = new ColorLabelField("Tel�fono de oficina:",
				LabelField.NON_FOCUSABLE);

		lestado = new ColorLabelField("Estado:", LabelField.NON_FOCUSABLE);
		lciudad = new ColorLabelField("Ciudad:", LabelField.NON_FOCUSABLE);
		lcalle = new ColorLabelField("Calle:", LabelField.NON_FOCUSABLE);

		lnumExterior = new ColorLabelField("N�mero exterior:",
				LabelField.NON_FOCUSABLE);
		lnumInterior = new ColorLabelField("N�mero interior:",
				LabelField.NON_FOCUSABLE);
		lcolonia = new ColorLabelField("Colonia:", LabelField.NON_FOCUSABLE);
		lCP = new ColorLabelField("C�digo postal:", LabelField.NON_FOCUSABLE);

		lnumeroTarjeta = new ColorLabelField("N�mero de tarjeta:",
				LabelField.NON_FOCUSABLE);
		ltipoTarjeta = new ColorLabelField("Tipo de tarjeta:",
				LabelField.NON_FOCUSABLE);
		ldomicilioTarjeta = new ColorLabelField(
				"Domicilio de estado de cuenta de tarjeta:",
				LabelField.NON_FOCUSABLE);
/*
		nombreUsuario = new ColorEditField("", "nombreUsuario");
		numeroCelular01 = new ColorEditField("", "9999895623");
		confNumeroCelular = new ColorEditField("", "9999895623");
		email = new ColorEditField("", "nomehables@incomunicacion.com");
		confirmarEmail = new ColorEditField("", "nomehables@incomunicacion.com");

		//String proveedores[] = { "Iusacell", "Movistar", "Telcel" };

		proveedor = new ObjectChoiceField("", providersBeans, 0);

		nombre = new ColorEditField("", "nombre");
		apellidoPaterno = new ColorEditField("", "apellidoPaterno");
		apellidoMaterno = new ColorEditField("", "apellidoMaterno");

		//String sexos[] = { "Masculino", "Femenino" };
		String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
		sexo = new ObjectChoiceField("", sexos, 0);

		fechaNacimiento = new DateField("", new Date().getTime(),
				DateField.DATE);

		curp = new ColorEditField("", "curp");
		rfc = new ColorEditField("", "rfc");
		placas = new ColorEditField("", "placas");
		cuentaPredial = new ColorEditField("", "cuentaPredial");

		telefonoCasa = new ColorEditField("", "9999326598");
		telefonoOficina = new ColorEditField("", "9999326598");

		estado = new ObjectChoiceField("", EdoBeans, 16);

		ciudad = new ColorEditField("", "ciudad");
		calle = new ColorEditField("", "calle");

		numExterior = new ColorEditField("", "119");
		numInterior = new ColorEditField("", "32");
		colonia = new ColorEditField("", "colonia");
		CP = new ColorEditField("", "CP");

		numeroTarjeta = new ColorEditField("", "numeroTarjeta");

		tipoTarjeta = new ObjectChoiceField("", creditBeans, 0);
		domicilioTarjeta = new ColorEditField("", "domicilioTarjeta");
*/
		
		
		nombreUsuario = new ColorEditField("", "");
		numeroCelular01 = new ColorEditField("", "");
		confNumeroCelular = new ColorEditField("", "");
		email = new ColorEditField("", "");
		confirmarEmail = new ColorEditField("", "");

		//String proveedores[] = { "Iusacell", "Movistar", "Telcel" };

		proveedor = new ObjectChoiceField("", providersBeans, 0);

		nombre = new ColorEditField("", "");
		apellidoPaterno = new ColorEditField("", "");
		apellidoMaterno = new ColorEditField("", "");

		//String sexos[] = { "Masculino", "Femenino" };
		String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
		sexo = new ObjectChoiceField("", sexos, 0);

		fechaNacimiento = new DateField("", new Date().getTime(),
				DateField.DATE);

		curp = new ColorEditField("", "");
		rfc = new ColorEditField("", "");
		//placas = new ColorEditField("", "");
		//cuentaPredial = new ColorEditField("", "");

		telefonoCasa = new ColorEditField("", "");
		telefonoOficina = new ColorEditField("", "");

		estado = new ObjectChoiceField("", EdoBeans, 16);

		ciudad = new ColorEditField("", "");
		calle = new ColorEditField("", "");

		numExterior = new ColorEditField("", "");
		numInterior = new ColorEditField("", "");
		colonia = new ColorEditField("", "");
		CP = new ColorEditField("", "");

		numeroTarjeta = new ColorEditField("", "");

		tipoTarjeta = new ObjectChoiceField("", creditBeans, 0);
		domicilioTarjeta = new ColorEditField("", "");
		
		
		lAnioTarjeta =  new ColorLabelField("A�o Vencimiento:", LabelField.NON_FOCUSABLE); 
		lMesTarjeta =  new ColorLabelField("Mes Vencimiento:", LabelField.NON_FOCUSABLE);
		
		
		String meses[] = {"enero", "febrero", "marzo", 
						  "abril", "mayo", "junio", "julio", 
						  "agosto", "septiembre", "octubre", 
						  "noviembre", "diciembre"};
		
		mesTarjeta = new ObjectChoiceField("", meses, 6);
		
		String anios[] = UtilDate.getYears(5);
		anioTarjeta = new ObjectChoiceField("", anios, 0);
		
		acceptTerms = new CheckboxField("Acepto terminos y condiciones.", false);
		
		terminos =  new ButtonField("T�rminos y Condiciones", ButtonField.CONSUME_CLICK);
		terminos.setChangeListener(this);
		
		aceptar = new ButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		aceptar.setChangeListener(this);
	}

	private void addVariables() {

		add(lnombreUsuario);
		add(nombreUsuario);
		add(lnumeroCelular01);
		add(numeroCelular01);
		add(lconfNumeroCelular);
		add(confNumeroCelular);
		add(lemail);
		add(email);
		add(lconfirmarEmail);
		add(confirmarEmail);
		add(lproveedor);
		add(proveedor);

		add(lnombre);
		add(nombre);
		add(lapellidoPaterno);
		add(apellidoPaterno);
		add(lapellidoMaterno);
		add(apellidoMaterno);
		add(lsexo);
		add(sexo);
		add(lfechaNacimiento);
		add(fechaNacimiento);

		add(lcurp);
		add(curp);
		add(lrfc);
		add(rfc);
		//add(lplacas);
		//add(placas);
		//add(lcuentaPredial);
		//add(cuentaPredial);

		add(ltelefonoCasa);
		add(telefonoCasa);
		add(ltelefonoOficina);
		add(telefonoOficina);

		add(lestado);
		add(estado);
		add(lciudad);
		add(ciudad);
		add(lcalle);
		add(calle);

		add(lnumExterior);
		add(numExterior);
		add(lnumInterior);
		add(numInterior);
		add(lcolonia);
		add(colonia);
		add(lCP);
		add(CP);

		add(lnumeroTarjeta);
		add(numeroTarjeta);
		add(ltipoTarjeta);
		add(tipoTarjeta);
		
		add(lMesTarjeta);
		add(mesTarjeta);
		add(lAnioTarjeta);
		add(anioTarjeta);

		add(ldomicilioTarjeta);
		add(domicilioTarjeta);
		
		add(acceptTerms);
		
		add(terminos);
		add(aceptar);
	}

	private boolean verificarDatos() {

		boolean value = true;

		if (nombreUsuario.getText().length() == 0) {
			Dialog.alert("Falto introducir el usuario.");
			value = false;
		} else if (numeroCelular01.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de celular.");
			value = false;
		} else if (confNumeroCelular.getText().length() == 0) {
			Dialog.alert("Falto introducir la confirmaci�n del n�mero de celular.");
			value = false;
		} else if (email.getText().length() == 0) {
			Dialog.alert("Falto introducir el correo electr�nico.");
			value = false;
		} else if (confirmarEmail.getText().length() == 0) {
			Dialog.alert("Falto introducir la confirmaci�n del correo electr�nico.");
			value = false;
		} else if (nombre.getText().length() == 0) {
			Dialog.alert("Falto introducir el nombre.");
			value = false;
		} else if (apellidoPaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido paterno.");
			value = false;
		} else if (apellidoMaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido materno.");
			value = false;
		} else if (curp.getText().length() == 0) {
			Dialog.alert("Falto introducir el CURP");
			value = false;
		} else if (rfc.getText().length() == 0) {
			Dialog.alert("Falto introducir el RFC");
			value = false;
		}
		
		//else if (placas.getText().length() == 0) {
			//Dialog.alert("Falto introducir el n�mero de placas.");
			//value = false;
		//} 
		
		//else if (cuentaPredial.getText().length() == 0) {
			//Dialog.alert("Falto introducir la cuenta predial.");
			//value = false;
		//} 
		
		
		else if (telefonoCasa.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la casa.");
			value = false;
		} else if (telefonoOficina.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la oficina.");
			value = false;
		} else if (ciudad.getText().length() == 0) {
			Dialog.alert("Falto introducir la ciudad.");
			value = false;
		} else if (calle.getText().length() == 0) {
			Dialog.alert("Falto introducir la calle.");
			value = false;
		} else if (numExterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero exterior.");
			value = false;
		} else if (numInterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero interior.");
			value = false;
		} else if (colonia.getText().length() == 0) {
			Dialog.alert("Falto introducir la colonia.");
			value = false;
		} else if (CP.getText().length() == 0) {
			Dialog.alert("Falto introducir el C�digo postal.");
			value = false;
		} else if (numeroTarjeta.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de tarjeta.");
			value = false;
		} else if (domicilioTarjeta.getText().length() == 0) {
			Dialog.alert("Falto introducir el domicilio del estado de cuenta.");
			value = false;
		}

		return value;
	}

	public void fieldChanged(Field field, int context) {

		if (field == aceptar) {

			
			if (acceptTerms.getChecked()){
				
				if (verificarDatos()){
					crearRegistro();
				}
				
			} else {
				
				Dialog.alert("Debe de aceptar los terminos y condiciones.");
			}
		} else if (field == terminos){
			
			
			//{"id_usuario":"1370905309595","id_aplicacion":1.0.0,"id_termino":2,"id_producto":1}

			
			
			
			try {
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("id_aplicacion", 16);
				jsonObject.put("id_producto", 3);
				
				String post = jsonObject.toString();
				
				String data = AddcelCrypto.encryptHard(post);

				GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_TERMS, this);
				genericHTTP.run();
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
	
	
	private void crearRegistro(){
		
		try {
				String parsePass02 = UtilSecurity.parsePass(UserBean.passTEMP);
				String userJson02 = generateUserJSON();
				String n = UtilSecurity.aesEncrypt(parsePass02, userJson02);
				String n2 = UtilSecurity.mergeStr(n, UserBean.passTEMP);
				new RegisterThread(n2, this).start();

		} catch (Exception e) {
			System.out.println("error " + e.toString());
			Dialog.alert("Ocurri� un error. Int�ntelo de nuevo m�s tarde");
		}
	}
	
	private String createUserJson(String pass){
		
		String value = "{\"login\":\"" + nombreUsuario.getText().trim() + 
						"\", \"password\":\"" + pass + 
						"\"}";
		return value;
	}
	
	
	public String generateUserJSON() throws UnsupportedEncodingException {

		StringBuffer userJSON = new StringBuffer();
		
		userJSON.append("{\"login\":\"").append(nombreUsuario.getText().trim());
		userJSON.append("\",\"password\":\"").append(UserBean.passTEMP);
		
		userJSON.append("\",\"curp\":\"").append(curp.getText().trim());
		userJSON.append("\",\"rfc\":\"").append(rfc.getText().trim());
		//userJSON.append("\",\"placa\":\"").append(placas.getText().trim());
		//userJSON.append("\",\"ctaPredial\":\"").append(cuentaPredial.getText().trim());
		
		userJSON.append("\",\"nacimiento\":\"").append(UtilDate.getYYYY_MM_DDFromLong(fechaNacimiento.getDate()));
		userJSON.append("\",\"telefono\":\"").append(numeroCelular01.getText().trim());
		userJSON.append("\",\"nombre\":\"").append(nombre.getText().trim());
		userJSON.append("\",\"apellido\":\"").append(apellidoPaterno.getText().trim());
		userJSON.append("\",\"ciudad\":\"").append(ciudad.getText());
		userJSON.append("\",\"tarjeta\":\"").append(numeroTarjeta.getText().trim());
		userJSON.append("\",\"terminos\":\"1");
		userJSON.append("\",\"registro\":\"").append(UtilDate.getDateToDDMMAAAA(new Date()));
		userJSON.append("\",\"mail\":\"").append(email.getText().trim());
		userJSON.append("\",\"materno\":\"").append(apellidoMaterno.getText().trim());
		userJSON.append("\",\"sexo\":\"").append(getDataFromChoice(sexo));
		userJSON.append("\",\"tel_casa\":\"").append(telefonoCasa.getText().trim());
		userJSON.append("\",\"tel_oficina\":\"").append(telefonoOficina.getText().trim());
		userJSON.append("\",\"id_estado\":\"").append(getDataFromChoice(estado));
		//userJSON.append("\",\"id_estado\":\"").append(estado.getSelectedIndex());
		userJSON.append("\",\"ciudad\":\"").append(ciudad.getText().trim());
		userJSON.append("\",\"calle\":\"").append(calle.getText().trim());
		userJSON.append("\",\"num_ext\":\"").append(numExterior.getText().trim());
		userJSON.append("\",\"num_interior\":\"").append(numInterior.getText().trim());
		userJSON.append("\",\"colonia\":\"").append(colonia.getText().trim());
		userJSON.append("\",\"cp\":\"").append(CP.getText().trim());


		if (tipoTarjeta.getSelectedIndex() == 2) {
			userJSON.append("\",\"dom_amex\":\"").append(domicilioTarjeta.getText().trim());
		} else {
			userJSON.append("\",\"dom_amex\":\"");
		}

		
		int numMonth = mesTarjeta.getSelectedIndex() + 1;

		String stringMonth = String.valueOf(numMonth);

		if (numMonth < 10) {
			stringMonth = "0" + stringMonth;
		}		
		
		String SAnioTarjeta = getDataFromChoice(anioTarjeta);
		SAnioTarjeta = SAnioTarjeta.substring((SAnioTarjeta.length() - 2), SAnioTarjeta.length());
		userJSON.append("\",\"vigencia\":\"").append(stringMonth).append("/").append(SAnioTarjeta);
		
		//userJSON.append("\",\"banco\":\"").append(proveedor.getSelectedIndex());
		
		userJSON.append("\",\"banco\":\"").append(getDataFromChoice(proveedor));
		userJSON.append("\",\"tipotarjeta\":\"").append(getDataFromChoice(tipoTarjeta));
		userJSON.append("\",\"proveedor\":\"").append(getDataFromChoice(proveedor));
		
		userJSON.append("\",\"imei\":\"").append(UtilBB.getImei());
		userJSON.append("\",\"etiqueta\":\"\",\"numero\":\"0\",\"dv\":\"0\",\"idtiporecargatag\":\"1");
		userJSON.append("\",\"tipo\":\"").append(UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		userJSON.append("\",\"software\":\"").append(UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		userJSON.append("\",\"modelo\":\"").append(UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		userJSON.append("\",\"key\":\"").append(UtilBB.getImei() + "\"");
		userJSON.append(",\"status\":1}");
		System.out.println(userJSON.toString());
		return userJSON.toString();

	}
	
	
	//public  String TAG_REGISTER = ",\"etiqueta\":\"\",\"numero\":\"0\",\"dv\":\"0\",\"idtiporecargatag\":\"1";
	
	private String getDataFromChoice(ObjectChoiceField choiceField){
		
		int iValue = choiceField.getSelectedIndex();
		Object oValue = (Object)choiceField.getChoice(iValue);
		
		String sValue = null;
		
		if (oValue instanceof GeneralBean){
			
			GeneralBean bean = (GeneralBean)oValue;
			sValue = bean.getClave();
		} else {
			
			sValue = (String)oValue;
		}
		
		
		return sValue;
	}

	public void setData(int request, JSONObject jsObject) {

		/*
		{"resultado":5,"mensaje":"No es posible registrar el usuario."}
		*/
		
		String value;
		try {
			value = jsObject.getString("mensaje");
			Dialog.alert(value);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Dialog.alert("Error al leer la respuesta.");
		}
		
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}
	
}



/*
String estados[] = { "Aguascalientes",
		"Baja California", "Baja California Sur", "Campeche",
		"Chiapas", "Chihuahua", "Coahuila", "Colima", "Distrito Federal", "Durango",
		"Guanajuato", "Guerrero", "Hidalgo", "Jalisco",
		"Estado de M�xico", "Michoac�n", "Morelos", "Nayarit",
		"Nuevo Le�n", "Oaxaca", "Puebla", "Quer�taro", "Quintana Roo",
		"San Luis Potos�", "Sinaloa", "Sonora", "Tabasco",
		"Tamaulipas", "Tlaxcala", "Veracruz", "Yucat�n", "Zacatecas", };
*/
//String tipoTarjetas[] = { "Visa", "MasterCard", "American Express"};