package com.addcel.gdf.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;

import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.addcel.InfoThread.Login;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.ColorPasswordEditField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;


public class VAutenticacion extends CustomMainScreen implements Viewable,
		FieldChangeListener {

	private EditField userEditLogin = null;
	private ColorPasswordEditField userEditPassword = null;
	private ButtonField buttonField = null;
	private MainScreen mainScreen = null;
	private String mensaje = "Para llevar a cabo tus pagos, es necesario dar de alta tus datos en MobileCard. Si ya eres usuario inicia sesi�n, de lo contrario reg�strate.";
	
	
	private com.addcel.gdf.view.animated.SplashScreen splashScreen;
	
	public VAutenticacion() {

		super("Ingresar", false);
		setView();
	}

	public VAutenticacion(MainScreen mainScreen) {

		super("Ingresar", false);
		this.mainScreen = mainScreen;
		setView();
	}

	private void setView(){
		
		splashScreen = SplashScreen.getInstance();
		
		if (mainScreen != null){
			RichTextField richTextField = new RichTextField(mensaje, RichTextField.NON_FOCUSABLE);
			add(richTextField);
			add(new LabelField(""));
		}

		HorizontalFieldManager loginFieldManager = new HorizontalFieldManager();
		ColorLabelField userLabelLogin = new ColorLabelField("Usuario: ", LabelField.NON_FOCUSABLE);
		userEditLogin = new ColorEditField("", "");
		loginFieldManager.add(userLabelLogin);
		loginFieldManager.add(userEditLogin);
		add(loginFieldManager);

		HorizontalFieldManager passwordFieldManager = new HorizontalFieldManager();
		ColorLabelField userLabelPassword = new ColorLabelField("Password: ", LabelField.NON_FOCUSABLE);
		userEditPassword = new ColorPasswordEditField("", "");

		passwordFieldManager.add(userLabelPassword);
		passwordFieldManager.add(userEditPassword);
		add(passwordFieldManager);

		buttonField = new ButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		buttonField.setChangeListener(this);
		add(buttonField);
	}


	public void sendMessage(String message) {

		final String messages = message;

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Dialog.alert(messages);
			}
		});

	}

	public void setData(int request, JSONObject jsObject) {
		
		System.out.println(jsObject.toString());
	}

	
	public void fieldChanged(Field field, int context) {

		if (field == buttonField) {

			String login = userEditLogin.getText();
			String password = userEditPassword.getText();

			if (checkFields(login, password)) {

				getData(login, password);
			} else {

				sendMessage("Falta informaci�n de el usuario � la contrase�a.");
			}
		}
	}

	
	private void getData(String login, String password){
		
		Login loginThread = null;
		if (mainScreen == null){
			loginThread = new Login(this, login, password);

		} else {
			loginThread = new Login(this, login, password, mainScreen);
		}
		loginThread.run();
	}
	
	
	private boolean checkFields(String login, String password) {

		boolean value = false;

		if ((login != null) && (login.length() > 0) && (password != null)
				&& (password.length() > 0)) {
			value = true;
		}

		return value;
	}
}



/*
splashScreen.start();

Timer timer = new Timer();
Wait wait = new Wait(this);
timer.schedule(wait, 2800);
*/


/*

	class Wait extends TimerTask {
		
		private CustomMainScreen screen;
		
		public Wait(CustomMainScreen screen) {
			
			this.screen = screen;
		}

		public void run() {

			synchronized (Application.getEventLock()) {
				
				splashScreen.remove();
				UiApplication.getUiApplication().popScreen(screen);
			}
		}
	}


*/