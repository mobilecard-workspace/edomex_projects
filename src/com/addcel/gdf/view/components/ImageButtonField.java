package com.addcel.gdf.view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

public class ImageButtonField extends Field {
	private String _label;
	private Font _font;

	private Bitmap _currentPicture;
	private Bitmap _onPicture;
	private Bitmap _offPicture;
	boolean no = true;
	String id = null;
	private boolean selected = false;

	public ImageButtonField(String text, String _onImg, String _offImg,
			long focusable) {
		super(focusable);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;
	}

	public ImageButtonField(String text, String _onImg, String _offImg,
			String _id) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;
		id = _id;

	}

	public ImageButtonField(String text, String _onImg, String _offImg) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;

	}

	public ImageButtonField(String text, String _onImg, String _offImg,
			boolean _no) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;

		no = _no;
	}

	public int getPreferredHeight() {
		return this._onPicture.getHeight();
	}

	public int getPreferredWidth() {
		return this._onPicture.getWidth();
	}

	protected void onFocus(int direction) {

	}

	protected void onUnfocus() {
		this.selected = false;
		_currentPicture = _offPicture;
		invalidate();
	}

	protected void drawFocus(Graphics graphics, boolean on) {
		this.selected = true;
		_currentPicture = _onPicture;
		graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture, 0,
				0);
		invalidate();
	}

	protected void pintaFocus(Graphics graphics) {
		graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture, 0,
				0);
		invalidate();
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()),
				Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {

		if (!this.selected) {
			graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture,
					0, 0);
		} else {
			pintaFocus(graphics);
		}
		invalidate();
	}

	protected void paintBackground(Graphics g) {
	}

	protected boolean navigationClick(int status, int time) {
		if (no && id != null) {
		} else if (_label.equals("minuto")) {
		}

		fieldChangeNotify(1);

		return true;
	}

	public boolean isSeleccionado() {
		return selected;
	}
}