package com.addcel.gdf.view.components.color;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.gdf.view.util.UtilColor;

public class HighlightsLabelField extends LabelField{

	private String text;
	private int width = 0;
	private int drawnGlyph;
	
	public HighlightsLabelField(String text, int drawnGlyph){
		super(text, LabelField.NON_FOCUSABLE);
		this.text = text;
		width = Display.getWidth();
		this.drawnGlyph = drawnGlyph;
	}

	
	public HighlightsLabelField(String text, long style ){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
	}
	
	
	public HighlightsLabelField(String text, long style, int color){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
	}

	public void paint(Graphics graphics) {

		graphics.setBackgroundColor(UtilColor.EDIT_FIELD_BACKGROUND);
		graphics.clear();
		graphics.setColor(UtilColor.EDIT_FIELD_STRING);
		graphics.drawText(text, 0, 0, drawnGlyph, width);
		
		
		super.paint(graphics);
	}
}