package com.addcel.gdf.view.components.custom;

import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomVerticalFieldManager extends VerticalFieldManager {

	public int width = 0;
	public int height = 0;

	public CustomVerticalFieldManager(int width, long style) {

		super(style);

		this.width = width;

	}

	public CustomVerticalFieldManager(int width, int height, long style) {

		super(style);

		this.width = width;
		this.height = height;

	}

	protected void sublayout(int maxWidth, int maxHeight) {
		// TODO Auto-generated method stub
		if (height == 0) {
			super.sublayout(this.width, maxHeight);
		} else {

			super.sublayout(this.width, height);
			setExtent(this.width, height);
		}
	}

	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return this.width;
	}

}
