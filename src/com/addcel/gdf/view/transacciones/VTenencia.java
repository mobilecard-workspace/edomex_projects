package com.addcel.gdf.view.transacciones;


import java.io.IOException;
import java.util.Date;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.Bitacora;
import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.connection.addcel.implementation.tareas.Tareas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;


public class VTenencia extends CustomMainScreen implements FieldChangeListener, Viewable {

	private ColorEditField editPlaca = null;
	private ColorEditField editTel = null;
	private ColorEditField ceditPlaca = null;
	private ObjectChoiceField editEvento = null;
	private ObjectChoiceField editPlacas = null;

	private HighlightsLabelField editTipoServicio;
	private HighlightsLabelField editLC;
	//private HighlightsLabelField editVigencia;

	private HighlightsLabelField editTenencia;
	private HighlightsLabelField editTenActualizacion;
	private HighlightsLabelField editTenRecargo;
	private HighlightsLabelField editDerecho;
	private HighlightsLabelField editDerRecargo;
	private HighlightsLabelField editDerActualizacion;
	private HighlightsLabelField editTotalDerecho;
	private HighlightsLabelField editTotal;
	private HighlightsLabelField editLineacaptura;
	
	private LabelField textTipoServicio;
	private LabelField textLC;
	//private LabelField textVigencia;
	
	private LabelField textTenencia;
	private LabelField textTenActualizacion;
	private LabelField textTenRecargo;
	private LabelField textDerecho;
	private LabelField textDerRecargo;
	private LabelField textDerActualizacion;
	private LabelField textTotalDerecho;
	private LabelField textTotal;
	private LabelField textLineacaptura;

	private String tenencia;
	private String tenActualizacion;
	private String tenRecargo;
	private String derecho;
	private String derRecargo;
	private String derActualizacion;
	private String totalDerecho;
	private String total;
	private String lineacaptura;
	
	private String tipoServicio;
	private String fecha;

	private SlimSelectedButtonField confirma = null;
	private SlimSelectedButtonField verificar = null;
	//private SlimSelectedButtonField agregarPlacas = null;
	private CustomSelectedButtonField pagar = null;
	private CustomSelectedButtonField cancelar = null;
	
	private SplashScreen splashScreen;
	private boolean listoPagar = false;
	
	private Bitacora bitacora;
	private long idBitacora = 0;
	
	private int indexDerecho = 0;
	private int indexTenecia = 0;
	private HorizontalFieldManager fieldManagerDerecho = null;
	private HorizontalFieldManager fieldManagerTenencia = null;
	private boolean isDerecho = true;
	private boolean isTenencia = true;

	private final int STATUS_TRAER_INFO = 0; 
	private final int STATUS_PAGAR = 1;
	private final int STATUS_INSERTA_MOV = 2;
	private final int STATUS_INSERTA_BITACORA = 3;
	private int status = 0;

	private JSONObject jsData;

	private String sPlaca = null;


	public VTenencia(String title, int borrame) {
		
		super("", true);
		splashScreen = SplashScreen.getInstance();
		bitacora = new Bitacora();
		addComponents();
		existenCamposActualizacion();
	}
	
	
	private void addComponents(){

		LabelField textPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
		LabelField textTel = new ColorLabelField("Telefono: ", LabelField.NON_FOCUSABLE);
		//LabelField ctextPlaca = new ColorLabelField("Confirmar Placa: ", LabelField.NON_FOCUSABLE);
		//LabelField textPlacas = new ColorLabelField("Placas: ", LabelField.NON_FOCUSABLE);
		//LabelField textEvento = new ColorLabelField("Evento: ", LabelField.NON_FOCUSABLE);
		
		String anios[] = UtilDate.getYears(- 5);
		editEvento = new ObjectChoiceField("", anios, 0);
		editPlaca = new ColorEditField("", "");
		editTel = new ColorEditField("", "");
		ceditPlaca = new ColorEditField("", "");
		
		HorizontalFieldManager managerPlaca = new HorizontalFieldManager();
		managerPlaca.add(textPlaca);
		managerPlaca.add(editPlaca);
		add(managerPlaca);

		HorizontalFieldManager managerTel = new HorizontalFieldManager();
		managerTel.add(textTel);
		managerTel.add(editTel);
		add(managerTel);

		confirma = new SlimSelectedButtonField("Confirma", ButtonField.CONSUME_CLICK);
		confirma.setChangeListener(this);

		textTipoServicio = new ColorLabelField("Servicio: ", LabelField.NON_FOCUSABLE);
		textLC = new ColorLabelField("LC: ", LabelField.NON_FOCUSABLE);

		textTenencia = new ColorLabelField("Tenencia: ", LabelField.NON_FOCUSABLE);		
		textTenActualizacion = new ColorLabelField("Act. Tenencia: ", LabelField.NON_FOCUSABLE);
		textTenRecargo = new ColorLabelField("Rec. Tenencia: ", LabelField.NON_FOCUSABLE);
		textDerecho = new ColorLabelField("Derechos: ", LabelField.NON_FOCUSABLE);

		
		textDerActualizacion = new ColorLabelField("Act. Derechos: ", LabelField.NON_FOCUSABLE);
		textDerRecargo = new ColorLabelField("Rec. Derechos: ", LabelField.NON_FOCUSABLE);
		textTotalDerecho = new ColorLabelField("Total Derechos: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineacaptura = new ColorLabelField("Línea de Captura: ", LabelField.NON_FOCUSABLE);


		editTipoServicio = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		editLC = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		//editVigencia = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		
		editTenencia = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTenActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTenRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerecho = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotalDerecho = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLineacaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		verificar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		//agregarPlacas = new SlimSelectedButtonField("Agregar Placa", ButtonField.CONSUME_CLICK);
		pagar = new CustomSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		
		verificar.setChangeListener(this);
		//agregarPlacas.setChangeListener(this);
		pagar.setChangeListener(this);
		cancelar.setChangeListener(this);
		//CustomButtonFieldManager managerAgregarPlacas = new CustomButtonFieldManager(verificar, agregarPlacas);
		//managerAgregarPlacas.add(verificar);
		//managerAgregarPlacas.add(agregarPlacas);
		//add(managerAgregarPlacas);
		
		add(verificar);
		add(new LabelField(""));
/*
	    a).-Tenencia
	    b).-Rec Tenencia
	    c).-Refrendo
	    d).-Rec Refrendo
	    e).-Act Tenencia(Debe mostrar ceros cuando así lo amerite).
	    f).-Act Refrendo(Debe mostrar ceros cuando así lo amerite).
	    g).-Linea de Captura.
	    h).-Total.
*/
/*
		add(new LabelField(""));

		addHorizontalFiels(textTenencia, editTenencia);
		addHorizontalFiels(textTenRecargo, editTenRecargo);
		addHorizontalFiels(textTenActualizacion, editTenActualizacion);
		
		addHorizontalFiels(textDerecho, editDerecho);
		addHorizontalFiels(textDerRecargo, editDerRecargo);
		addHorizontalFiels(textDerActualizacion, editDerActualizacion);
		*/
		
		addHorizontalFiels(textTotal, editTotal);
		//addHorizontalFiels(textTipoServicio, editTipoServicio);

		//add(new LabelField(""));

		addHorizontalFiels(textLC, editLC);

		//add(new LabelField(""));
		CustomButtonFieldManager managerBtn = new CustomButtonFieldManager(pagar, cancelar);
		managerBtn.add(pagar);
		managerBtn.add(cancelar);
		add(managerBtn);
		
		//add(pagar);
	}


	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}


	private boolean validarPlaca(){

		boolean regreso = false;

		String aPlaca = editPlaca.getText(); 
		String bPlaca = ceditPlaca.getText();

		aPlaca = aPlaca.toUpperCase();
		bPlaca = bPlaca.toUpperCase();

		editPlaca.setText(aPlaca);
		ceditPlaca.setText(bPlaca);

		if ((aPlaca.length() > 0)&&(bPlaca.length() > 0)){
			regreso = aPlaca.equals(bPlaca); 
		}

		return regreso; 
	}
	
	
	public void fieldChanged(Field field, int context) {

		
		if (field == confirma){
			
			if (!validarPlaca()){
				Dialog.alert("No coincide la placa, favor de validarla.");
			}
			
		} else if (field == verificar){

			
			if (!validarPlaca()){
				Dialog.alert("No coincide la placa, favor de validarla.");
			} else {
				
				if (UserBean.nameLogin == null){
					UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
				} else {
					
					status = this.STATUS_TRAER_INFO;
					
					editTipoServicio.setText("");
					editLC.setText("");
					//editVigencia.setText("");
					
					editTenencia.setText("");
					editTenActualizacion.setText("");
					editTenRecargo.setText("");
					editDerecho.setText("");
					editDerRecargo.setText("");
					editDerActualizacion.setText("");
					editTotalDerecho.setText("");
					editTotal.setText("");
					editLineacaptura.setText("");
					
					getData();
				}
			}
			


		} else if(field == pagar){

			if (listoPagar){
				pagar();
			} else {
				Dialog.alert("Falta información para generar el pago.");
			}

		} 
		/*
		else if (field == agregarPlacas){

			agregarPlacas();
		}
		*/
	}



	private void getData() {

		int iPlacas = editPlacas.getSelectedIndex();
		
		if (iPlacas == 0){
			sPlaca = editPlaca.getText();
			if (sPlaca != null){
				sPlaca = sPlaca.toUpperCase();
				editPlaca.setText(sPlaca);
			}
		} else {
			sPlaca = (String) editPlacas.getChoice(iPlacas);
		}

		int iValue = editEvento.getSelectedIndex();
		String sEvento = (String) editEvento.getChoice(iValue);

		if ((sPlaca != null) && (sEvento != null) && (sPlaca.length() > 4) && (sEvento.length() == 4)) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("usuario", UserBean.idLogin);
				jsonObject.put("toDO", DTO.STATUS_TENENCIA);
				jsonObject.put("placa", sPlaca);
				jsonObject.put("ejercicio", sEvento);
				jsonObject.put("subsidio", true);
				jsonObject.put("condonacion", false);
				jsonObject.put("interes", false);

				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptSensitive(UserBean.password, post);

				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}	
	
	
	private void agregarPlacas() {
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Placa:",
				Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {
				
				String placa = inputField.getText();
				
				if (placa != null){
					DataRMS placas = new DataRMS(DataRMS.PLACAS);
					placa = placa.toUpperCase();
					placas.addData(placa);
					String sPlacas[] = placas.getData();
					editPlacas.setChoices(sPlacas);
				} else {
					Dialog.alert("Verificar valor de placa");
				}

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar información");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposición");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la información es inexacta.");
				e.printStackTrace();
			}
		}
	}


	private void pagar(){
		
		try {

			//jsData.put("usuario", UserBean.idLogin);

			String imei = UtilBB.getImei();
			
			jsData.put("id_usuario", UserBean.idLogin);
			jsData.put("id_producto", DTO.STATUS_TENENCIA);
			jsData.put("imei", imei);
			jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
			jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
			jsData.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
			jsData.put("wkey", imei);
			jsData.put("cx", "0.0");
			jsData.put("cy", "0.0");
			
			String post = jsData.toString();
			
			String data = AddcelCrypto.encryptSensitive(UserBean.password, post);
			
			VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
			pagoWeb.execute(data);
			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().pushScreen(pagoWeb);
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al agregar la información para mandarla.");
		}
	}


	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		switch (status) {
		case STATUS_TRAER_INFO:
			try {
				
				jsData = jsObject;
				
				tenActualizacion = jsObject.getString("tenActualizacion");
				derActualizacion = jsObject.getString("derActualizacion");
				
				if (!jsObject.has("tenActualizacion")||jsObject.isNull("tenActualizacion")){
					
					borrarCamposActualizacion();
				} else {
					
					agregarCamposActualizacion();
				}
				
				/*
				String dato = jsObject.getString("fech_factura");
				
				dato = UtilDate.convertAAAA_MM_DDtoDD_MM_AAAA(dato);
				
				System.out.println(dato);
				*/
				tenencia = jsObject.optString("tenencia");
				tenRecargo = jsObject.getString("tenRecargo");
				derecho = jsObject.getString("derechos");
				derRecargo = jsObject.getString("derRecargo");
				totalDerecho = jsObject.getString("totalDerecho");
				//total = jsObject.getString("total");
				total = jsObject.getString("totalPago");
				lineacaptura = jsObject.getString("linea_captura");
				tipoServicio = jsObject.getString("tipoServicio");
				fecha  = jsObject.getString("vigencia");
				/*
				long vigencia2 = jsObject.getLong("vigencia");
				
				Date vigencia = new Date(vigencia2);
				fecha = UtilDate.getDateToDD_MM_AAAA(vigencia);
*/
				editLC.setText(lineacaptura);
				//editVigencia.setText(fecha);
				editTipoServicio.setText(tipoServicio);
				editTenencia.setText(UtilNumber.formatCurrency(tenencia));
				editTenActualizacion.setText(UtilNumber.formatCurrency(tenActualizacion));
				editTenRecargo.setText(UtilNumber.formatCurrency(tenRecargo));
				editDerecho.setText(UtilNumber.formatCurrency(derecho));
				editDerRecargo.setText(UtilNumber.formatCurrency(derRecargo));
				editDerActualizacion.setText(UtilNumber.formatCurrency(derActualizacion));
				editTotalDerecho.setText(UtilNumber.formatCurrency(totalDerecho));
				editTotal.setText(UtilNumber.formatCurrency(total));
				editLineacaptura.setText(lineacaptura);

				listoPagar = true;
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar la información");
			}
			
			break;

		case STATUS_PAGAR:
			break;
			
		case STATUS_INSERTA_MOV:
			String codError = jsObject.optString("codError");
			String detError = jsObject.optString("detError");
			
			if (codError.equals("0")){
				Tareas tareas = new Tareas();

				String get = tareas.toCreatePagoWeb(UserBean.nameLogin, idBitacora, String.valueOf(total), DTO.PAGO_TENENCIA);
				
				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(get);
				UiApplication.getUiApplication().popScreen(this);
			    UiApplication.getUiApplication().pushScreen(pagoWeb);
			} else {
				
				Dialog.alert(detError);
			}

			break;
		}
	}

	
	private void procesarJson(JSONObject jsObject){

		try {
			
			if (jsObject.has("registrado")){
				
				long idBitacora = jsObject.optLong("registrado");
				
				if (idBitacora == 0){
					
					Dialog.alert("Tu pago no puede ser procesado en este momento.");
					
				} else {

					Tareas tareas = new Tareas();
					String get = tareas.toCreatePagoWeb(UserBean.nameLogin, idBitacora, String.valueOf(total), DTO.PAGO_TENENCIA);
					VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
					pagoWeb.execute(get);
					UiApplication.getUiApplication().popScreen(this);
				    UiApplication.getUiApplication().pushScreen(pagoWeb);
				}

			} else if (jsObject.has("placa")){

				tenActualizacion = jsObject.getString("tenActualizacion");
				derActualizacion = jsObject.getString("derActualizacion");
				
				if (!jsObject.has("tenActualizacion")||jsObject.isNull("tenActualizacion")){
					
					borrarCamposActualizacion();
				} else {
					
					agregarCamposActualizacion();
				}
				
				tenencia = jsObject.getString("tenencia");
				tenRecargo = jsObject.getString("tenRecargo");
				derecho = jsObject.getString("derecho");
				derRecargo = jsObject.getString("derRecargo");
				totalDerecho = jsObject.getString("totalDerecho");
				total = jsObject.getString("totalPago");
				lineacaptura = jsObject.getString("lineacaptura");
				tipoServicio = jsObject.getString("tipoServicio");
				long vigencia2 = jsObject.getLong("vigencia");
				
				Date vigencia = new Date(vigencia2);
				fecha = UtilDate.getDateToDD_MM_AAAA(vigencia);

				editLC.setText(lineacaptura);
				//editVigencia.setText(fecha);
				editTipoServicio.setText(tipoServicio);
				
				editTenencia.setText(UtilNumber.formatCurrency(tenencia));
				editTenActualizacion.setText(UtilNumber.formatCurrency(tenActualizacion));
				editTenRecargo.setText(UtilNumber.formatCurrency(tenRecargo));
				editDerecho.setText(UtilNumber.formatCurrency(derecho));
				editDerRecargo.setText(UtilNumber.formatCurrency(derRecargo));
				editDerActualizacion.setText(UtilNumber.formatCurrency(derActualizacion));
				editTotalDerecho.setText(UtilNumber.formatCurrency(totalDerecho));
				editTotal.setText(UtilNumber.formatCurrency(total));
				editLineacaptura.setText(lineacaptura);

				listoPagar = true;
			} else {

				String error = jsObject.getString("error");
				String numError = jsObject.getString("numError");

				sendMessage("Error: " + numError);
				
				listoPagar = false;
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al leer la información.");
		}
	}

	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}
	
	
	private void existenCamposActualizacion() {

		isDerecho = false;
		isTenencia = false;

		int size = this.getFieldCount();

		for (int index3 = 0; index3 < size; index3++) {

			Field field = this.getField(index3);

			if (field instanceof HorizontalFieldManager) {

				HorizontalFieldManager fieldManager = (HorizontalFieldManager) field;

				int size2 = fieldManager.getFieldCount();

				for (int index2 = 0; index2 < size2; index2++) {

					Field field2 = fieldManager.getField(index2);

					if (field2.equals((Field) editDerActualizacion)) {
						indexDerecho = index3;
						fieldManagerDerecho = fieldManager;
						isDerecho = true;
					}

					if (field2.equals((Field) editTenActualizacion)) {
						indexTenecia = index3;
						fieldManagerTenencia = fieldManager;
						isTenencia = true;
					}
				}
			}
		}
	}	
	
	private void borrarCamposActualizacion(){
		
		if (isDerecho){
			this.delete(fieldManagerDerecho);
			isDerecho = false;
		}
		
		if (isTenencia){
			this.delete(fieldManagerTenencia);
			isTenencia = false;
		}
		
		this.invalidate();
	}
	
	
	private void agregarCamposActualizacion(){
		
		if (!isDerecho){
			this.insert(fieldManagerDerecho, indexDerecho);
			isDerecho = true;
		}
		
		if (!isTenencia){
			this.insert(fieldManagerTenencia, indexTenecia);
			isTenencia = true;
		}
		
		this.invalidate();
	}
}




