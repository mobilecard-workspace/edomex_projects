package com.addcel.gdf.view.transacciones;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTP;
import com.addcel.gdf.model.connection.addcel.implementation.tareas.Tareas;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;


public class VNomina extends CustomMainScreen implements FieldChangeListener, Viewable {

	private LabelField textMes;
	private LabelField textAnio;

	private ObjectChoiceField editMes = null;
	private ObjectChoiceField editAnio = null;

	private LabelField textRFC;
	private LabelField confTextRFC;
	private LabelField textRemuneraciones;
	private LabelField textTrabajadores;

	private ColorEditField editRFC;
	private ColorEditField confEditRFC;
	private ColorEditField editRemuneraciones;
	private ColorEditField editTrabajadores;

	private SlimSelectedButtonField confirma = null;
	private SlimSelectedButtonField enviar = null;

	private LabelField textRFC01;
	private LabelField textAnio01;
	private LabelField textMes01;
	
	private LabelField textRemuneracionesPago;
	private LabelField textImpuesto;
	private LabelField textImpuestoActualizado;
	private LabelField textRecargos;
	private LabelField textTotal;
	private LabelField textLineaCaptura;
	private LabelField textVigencia;

	
	private HighlightsLabelField showRFC01;
	private HighlightsLabelField showAnio01;
	private HighlightsLabelField showMes01;
	
	private HighlightsLabelField showRemuneracionesPago;
	private HighlightsLabelField showImpuesto;
	private HighlightsLabelField showImpuestoActualizado;
	private HighlightsLabelField showRecargos;
	private HighlightsLabelField showTotal;
	private HighlightsLabelField showLineaCaptura;
	private HighlightsLabelField showVigencia;
	
	private SlimSelectedButtonField pagar = null;

	private String remuneraciones;
	private String impuesto;
	private String impuesto_actualizado;
	private String recargos;
	private String total;
	private String lineacaptura;
	private String vigencia;

	private int indexDerecho = 0;
	private HorizontalFieldManager fieldManagerDerecho = null;
	private boolean isDerecho = true;
	
	private boolean listoPagar = false;
	private JSONObject jsData = null;

	private String sRFC = null;
	private String anio = null;
	private String sMes = null;
	
	
	public VNomina(String title) {
		super("", true);
		
		String months[] = new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo",
				"Junio", "Julio", "Agosto", "Septiembre", "Octubre",
				"Noviembre", "Diciembre" };
		
		String anios[] = UtilDate.getYears(-5);
		
		editMes = new ObjectChoiceField("", months, 0);
		editAnio = new ObjectChoiceField("", anios, 0);
		
		
		textMes = new ColorLabelField("Mes: ", LabelField.NON_FOCUSABLE);
		textAnio = new ColorLabelField("A�o: ", LabelField.NON_FOCUSABLE);

		editRFC = new ColorEditField("", "");
		confEditRFC = new ColorEditField("", "");
		editRemuneraciones = new ColorEditField("", "");
		editTrabajadores = new ColorEditField("", "");

		textRFC = new ColorLabelField("Ingresa tu RFC: ", LabelField.NON_FOCUSABLE);
		confTextRFC = new ColorLabelField("Confirmar tu RFC: ", LabelField.NON_FOCUSABLE);
		textRemuneraciones = new ColorLabelField("Remuneraciones: ", LabelField.NON_FOCUSABLE);
		textTrabajadores = new ColorLabelField("No. de Trabajadores: ", LabelField.NON_FOCUSABLE);

		
		confirma = new SlimSelectedButtonField("Confirma", ButtonField.CONSUME_CLICK);
		enviar = new SlimSelectedButtonField("Enviar", ButtonField.CONSUME_CLICK);
		pagar = new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);

		confirma.setChangeListener(this);
		enviar.setChangeListener(this);
		pagar.setChangeListener(this);

		
		textRFC01 = new ColorLabelField("RFC: ", LabelField.NON_FOCUSABLE);
		textAnio01 = new ColorLabelField("A�o: ", LabelField.NON_FOCUSABLE);
		textMes01 = new ColorLabelField("Mes: ", LabelField.NON_FOCUSABLE);

		textRemuneracionesPago = new ColorLabelField("Remuneraciones grabadas: ", LabelField.NON_FOCUSABLE);
		textImpuesto = new ColorLabelField("Impuesto: ", LabelField.NON_FOCUSABLE);
		textImpuestoActualizado = new ColorLabelField("Actualizacion: ", LabelField.NON_FOCUSABLE);
		textRecargos = new ColorLabelField("Recargos: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineaCaptura = new ColorLabelField("Linea de Captura: ", LabelField.NON_FOCUSABLE);
		textVigencia = new ColorLabelField("Vigencia: ", LabelField.NON_FOCUSABLE);


		showRFC01 = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showAnio01 = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showMes01 = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		showRemuneracionesPago = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showImpuesto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showImpuestoActualizado = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showRecargos = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		showVigencia = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		add(textRFC);
		add(editRFC);

		add(confTextRFC);
		add(confEditRFC);
		add(confirma);
		
		addHorizontalFiels(textMes, editMes);
		addHorizontalFiels(textAnio, editAnio);

		add(textRemuneraciones);
		add(editRemuneraciones);

		add(textTrabajadores);
		add(editTrabajadores);

		add(enviar);
		add(new LabelField(""));

		addHorizontalFiels(textRFC01, showRFC01);
		addHorizontalFiels(textAnio01, showAnio01);
		addHorizontalFiels(textMes01, showMes01);
		
		addHorizontalFiels(textRemuneracionesPago, showRemuneracionesPago);
		addHorizontalFiels(textImpuesto, showImpuesto);
		addHorizontalFiels(textImpuestoActualizado, showImpuestoActualizado);
		addHorizontalFiels(textRecargos, showRecargos);
		addHorizontalFiels(textTotal, showTotal);

		add(textLineaCaptura);
		add(showLineaCaptura);		
		add(pagar);

		existenCamposActualizacion();
	}

	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	

	public void setData(int request, JSONObject jsObject) {


		if (jsObject.has("lineacapturaCB")){
			
			jsData = jsObject;
			
			try {
				
				if (
				
					(!jsObject.has("impuesto_actualizado"))||
					(jsObject.isNull("impuesto_actualizado"))				
				){
					borrarCamposActualizacion();
				} else {
					agregarCamposActualizacion();
				}

				remuneraciones = jsObject.getString("remuneraciones");
				impuesto = jsObject.getString("impuesto");
				impuesto_actualizado = jsObject.getString("impuesto_actualizado");
				recargos = jsObject.getString("recargos");
				total = jsObject.getString("totalPago");
				lineacaptura = jsObject.getString("linea_captura");
				vigencia = jsObject.getString("vigencia");

				
				showRFC01.setText(sRFC);
				showAnio01.setText(anio);
				showMes01.setText(sMes);
				
				showRemuneracionesPago.setText(UtilNumber.formatCurrency(remuneraciones));
				showImpuesto.setText(UtilNumber.formatCurrency(impuesto));
				showImpuestoActualizado.setText(UtilNumber.formatCurrency(impuesto_actualizado));
				showRecargos.setText(UtilNumber.formatCurrency(recargos));
				showTotal.setText(UtilNumber.formatCurrency(total));
				showLineaCaptura.setText(lineacaptura);
				showVigencia.setText(vigencia);
				
				listoPagar = true;

			} catch (JSONException e) {
				Dialog.alert("Error al leer informaci�n.");
				e.printStackTrace();
			}
		} else if (jsObject.has("registrado")){
			
			long idBitacora = jsObject.optLong("registrado");
			
			if (idBitacora == 0){
				
				Dialog.alert("Tu pago no puede ser procesado en este momento.");
				
			} else {
				
				Tareas tareas = new Tareas();
				
				String get = tareas.toCreatePagoWeb(UserBean.nameLogin, idBitacora, total, DTO.PAGO_NOMINA);

				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(get);
				UiApplication.getUiApplication().popScreen(this);
			    UiApplication.getUiApplication().pushScreen(pagoWeb);
			}
			
		} else if (jsObject.has("error")){
			
			String error = jsObject.optString("error");
			
			if (error.equals("1")){
				String descripcion = jsObject.optString("error_descripcion");
				Dialog.alert("Periodo de pago mayor al vigente.");
			}
			
		}
	}

	public void sendMessage(String message) {
	}

	private boolean validarRFC(){
		
		boolean regreso = false;
		
		String aRFC = editRFC.getText(); 
		String bRFC = confEditRFC.getText();
		
		aRFC = aRFC.toUpperCase();
		bRFC = bRFC.toUpperCase();
		
		editRFC.setText(aRFC);
		confEditRFC.setText(bRFC);
		
		if ((aRFC.length() > 0)&&(bRFC.length() > 0)){
			regreso = aRFC.equals(bRFC); 
		}

		return regreso; 
	}
	
	public void fieldChanged(Field arg0, int arg1) {
		

		if (arg0 == confirma){
		
			if (!validarRFC()){
				Dialog.alert("No coincide el RFC, favor de validarlo.");
			}
			
		} else if (arg0 == enviar){

			if (!validarRFC()){
				Dialog.alert("No coincide el RFC, favor de validar.");
			} else {
				if (UserBean.nameLogin == null){
					UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
				} else {
					if (verificarCampos()){
						
						if (verificarNumTrabajadores()){
							
							int iMes = editMes.getSelectedIndex() + 1;

							if (iMes < 10){
								sMes = "0" + String.valueOf(iMes); 
							} else {
								sMes = String.valueOf(iMes);
							}
							
							int index = editAnio.getSelectedIndex();
							anio = (String) editAnio.getChoice(index);
							
							JSONObject jsonObject = new JSONObject();
							
							try {
								
								sRFC = editRFC.getText();
								jsonObject.put("toDO", DTO.STATUS_NOMINA);
								jsonObject.put("rfc", sRFC);

								jsonObject.put("mes_pago_nomina", sMes);
								jsonObject.put("anio_pago_nomina", anio);
								jsonObject.put("remuneraciones", editRemuneraciones.getText());
								jsonObject.put("num_trabajadores", editTrabajadores.getText());

								jsonObject.put("tipo_declaracion", "1");
								jsonObject.put("interes", "TRUE");

								String post = jsonObject.toString();

								String data = AddcelCrypto.encryptSensitive(UserBean.password, post);

								GenericHTTP genericHTTP = new GenericHTTP(data, Url.URL_GDF, this);
								genericHTTP.run();

							} catch (JSONException e) {
								e.printStackTrace();
								Dialog.alert("Error al reunir la informaci�n.");
							}
						} else {
							
							Dialog.alert("El No. de trabajadores no puede ser cero");
						}
						
						
					} else {
						Dialog.alert("Falta informaci�n");
					}
				}
			}
		} else if (arg0 == pagar){
			
			if (listoPagar){
				
				try {

					String imei = UtilBB.getImei();

					jsData.put("id_usuario", UserBean.idLogin);
					jsData.put("id_producto", DTO.STATUS_NOMINA);
					jsData.put("imei", imei);
					jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
					jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
					jsData.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					jsData.put("wkey", imei);
					jsData.put("cx", "0.0");
					jsData.put("cy", "0.0");

					String post = jsData.toString();
					
					String data = AddcelCrypto.encryptSensitive(UserBean.password, post);
					
					VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
					pagoWeb.execute(data);
					UiApplication.getUiApplication().popScreen(this);
				    UiApplication.getUiApplication().pushScreen(pagoWeb);
					
				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert("Error al recopilar la informaci�n.");
				}
			} else {
				Dialog.alert("Falta informaci�n para pagar, por favor termine el proceso.");
			}
		}
	}

	
	private void existenCamposActualizacion() {

		isDerecho = false;

		int size = this.getFieldCount();

		for (int index3 = 0; index3 < size; index3++) {

			Field field = this.getField(index3);

			if (field instanceof HorizontalFieldManager) {

				HorizontalFieldManager fieldManager = (HorizontalFieldManager) field;

				int size2 = fieldManager.getFieldCount();

				for (int index2 = 0; index2 < size2; index2++) {

					Field field2 = fieldManager.getField(index2);

					if (field2.equals((Field) showImpuestoActualizado)) {
						indexDerecho = index3;
						fieldManagerDerecho = fieldManager;
						isDerecho = true;
					}
				}
			}
		}
	}	
	
	private void borrarCamposActualizacion(){
		
		if (isDerecho){
			this.delete(fieldManagerDerecho);
			isDerecho = false;
		}
		
		this.invalidate();
	}
	
	
	private void agregarCamposActualizacion(){
		
		if (!isDerecho){
			this.insert(fieldManagerDerecho, indexDerecho);
			isDerecho = true;
		}
		
		this.invalidate();
	}

	
	private boolean verificarCampos(){
		
		boolean verificar = false;
		
		if (
				verificarDatos(editRFC) &&
				verificarDatos(editRemuneraciones) &&
				verificarDatos(editTrabajadores)
			){
			
			String tRFC = editRFC.getText();
			tRFC = tRFC.toUpperCase();
			editRFC.setText(tRFC);
			verificar = true;
		}
			
		return verificar;
	}
	
	private boolean verificarNumTrabajadores(){
		
		boolean result = true; 
		
		String numero = editTrabajadores.getText();
		
		if ((numero.length() == 1)&&(numero.equals("0"))){
			result = false;
		}
		
		return result;
	}
	
	
	private boolean verificarDatos(EditField editField){
		
		boolean verificar = false;
		
		String data = editField.getText();
		
		if ((data != null)&&(data.length()>0)){
			verificar = true;
		}
		
		return verificar;
	}
}


/*

	boolean debug = true;

			if (debug){
				borrarCamposActualizacion();
			} else {
				agregarCamposActualizacion();
			}
			
			debug = !debug;

*/

/*
"remuneraciones":20000, 
 "impuesto":500,
 "impuesto_actualizado":506.45,
 "recargos":15.75,
 "Total":522,
 "lineacaptura":"96HSA902HXM374PH32QP",
 "vigencia":"2013-05-31",
*/