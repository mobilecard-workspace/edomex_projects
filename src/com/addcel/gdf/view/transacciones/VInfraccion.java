package com.addcel.gdf.view.transacciones;

import java.io.IOException;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.ChoiceField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.Adeudo;
import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.connection.addcel.implementation.tareas.Tareas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;


public class VInfraccion extends CustomMainScreen implements FieldChangeListener, Viewable {

	private ColorEditField editPlaca;
	private ColorEditField ceditPlaca;
	private ObjectChoiceField editPlacas;

	private HighlightsLabelField editImporteTotal;
	private ObjectChoiceField editFolios = new ObjectChoiceField();
	private HighlightsLabelField editFecha;
	private HighlightsLabelField editFolio;
	private HighlightsLabelField editActualizacion;
	private HighlightsLabelField editRecargo;

	private HighlightsLabelField editImporte;
	private HighlightsLabelField editTotalPagar;
	private HighlightsLabelField editLineaCaptura;

	private LabelField textImporteTotal;
	private LabelField textFolios;
	private LabelField textFecha;
	private LabelField textFolio;
	private LabelField textActualizacion;
	private LabelField textRecargo;

	private LabelField textImporte;
	private LabelField textTotalPagar;
	private LabelField textLineaCaptura;

	private long idBitacora;
	private double total;

	private SlimSelectedButtonField confirma = null;
	private SlimSelectedButtonField verificar = null;
	private SlimSelectedButtonField agregarPlacas = null;
	private CustomSelectedButtonField pagar = null;

	private boolean listoPagar = false;

	private SplashScreen splashScreen;

	private Adeudo adeudo;
	
	private int indexDerecho = 0;
	private HorizontalFieldManager fieldManagerDerecho = null;
	private boolean isDerecho = true;
	
	
	private final int STATUS_TRAER_INFO = 0; 
	private final int STATUS_PAGAR = 1;
	private final int STATUS_INSERTA_MOV = 2;
	private final int STATUS_INSERTA_BITACORA = 3;
	private int status = 0;
	
	
	private JSONObject jsData;
	
	public VInfraccion(String title) {
		
		super("", true);
		splashScreen = SplashScreen.getInstance();
		addComponents();
		existenCamposActualizacion();
	}
	
	private void addComponents(){

		LabelField textPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
		LabelField ctextPlaca = new ColorLabelField("Confirmar Placa: ", LabelField.NON_FOCUSABLE);
		LabelField textPlacas = new ColorLabelField("Placas: ", LabelField.NON_FOCUSABLE);
		
		editPlaca = new ColorEditField("", "");
		ceditPlaca = new ColorEditField("", "");
		//editPlaca = new ColorEditField("", "0010926");
		
		DataRMS placas = new DataRMS(DataRMS.PLACAS);
		
		try {
			String sPlacas[] = placas.getData();
			editPlacas = new ObjectChoiceField("", sPlacas, 0);			
		} catch (RecordStoreFullException e) {
			Dialog.alert("Ya no se puede almacenar información");
			e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			Dialog.alert("No se encuentra en sistema de almacenamiento");
			e.printStackTrace();
		} catch (RecordStoreException e) {
			Dialog.alert("El almacenamiento no se encuentra a disposición");
			e.printStackTrace();
		} catch (IOException e) {
			Dialog.alert("La lectura de la información es inexacta.");
			e.printStackTrace();
		}
		
		
		textImporteTotal = new ColorLabelField("Importe total: ", LabelField.NON_FOCUSABLE);
		textFolios = new ColorLabelField("Folios: ", LabelField.NON_FOCUSABLE);
		textFecha = new ColorLabelField("Fecha de infracción: ", LabelField.NON_FOCUSABLE);
		textFolio = new ColorLabelField("Folio: ", LabelField.NON_FOCUSABLE);
		textActualizacion = new ColorLabelField("Actualización: ", LabelField.NON_FOCUSABLE);
		textRecargo = new ColorLabelField("Recargos: ", LabelField.NON_FOCUSABLE);
		
		textImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		textTotalPagar  = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineaCaptura = new ColorLabelField("Línea de captura: ", LabelField.NON_FOCUSABLE);
		
		editImporteTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editFecha = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editFolio = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		editImporte = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotalPagar = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		confirma = new SlimSelectedButtonField("Confirma", ButtonField.CONSUME_CLICK);
		verificar = new SlimSelectedButtonField("Calcular", ButtonField.CONSUME_CLICK);
		agregarPlacas = new SlimSelectedButtonField("Agregar Placa", ButtonField.CONSUME_CLICK);
		pagar = new CustomSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		
		confirma.setChangeListener(this);
		verificar.setChangeListener(this);
		agregarPlacas.setChangeListener(this);
		pagar.setChangeListener(this);


		addHorizontalFiels(textPlacas, editPlacas);
		addHorizontalFiels(textPlaca, editPlaca);
		addHorizontalFiels(ctextPlaca, ceditPlaca);
		add(confirma);
		add(new LabelField(""));


		CustomButtonFieldManager managerAgregarPlacas = new CustomButtonFieldManager(verificar, agregarPlacas);
		managerAgregarPlacas.add(verificar);
		managerAgregarPlacas.add(agregarPlacas);
		add(managerAgregarPlacas);
		add(new LabelField(""));
		

		addHorizontalFiels(textImporteTotal, editImporteTotal);
		add(new LabelField(""));

		editFolios.setChangeListener(this);
		addHorizontalFiels(textFolios, editFolios);
		add(new LabelField(""));
		
		addHorizontalFiels(textFecha, editFecha);
		addHorizontalFiels(textFolio, editFolio);
		addHorizontalFiels(textActualizacion, editActualizacion);
		addHorizontalFiels(textRecargo, editRecargo);
		addHorizontalFiels(textImporte, editImporte);
		addHorizontalFiels(textTotalPagar, editTotalPagar);
		
		add(new LabelField(""));
		add(textLineaCaptura);
		add(editLineaCaptura);
		
		add(new LabelField(""));
		
		add(pagar);
	}
	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	private boolean validarPlaca(){

		boolean regreso = false;

		String aPlaca = editPlaca.getText(); 
		String bPlaca = ceditPlaca.getText();

		aPlaca = aPlaca.toUpperCase();
		bPlaca = bPlaca.toUpperCase();

		editPlaca.setText(aPlaca);
		ceditPlaca.setText(bPlaca);

		if ((aPlaca.length() > 0)&&(bPlaca.length() > 0)){
			regreso = aPlaca.equals(bPlaca); 
		}

		return regreso; 
	}
	
	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == confirma){
			
			if (!validarPlaca()){
				Dialog.alert("No coincide la placa, favor de validarla.");
			}
			
		} else if (arg0 == verificar){

			
			if (!validarPlaca()){
				Dialog.alert("No coincide la placa, favor de validarla.");
			} else {
				if (UserBean.nameLogin == null){
					UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
				} else {
					
					status = this.STATUS_TRAER_INFO;

					editImporteTotal.setText("");
					editFecha.setText("");
					editFolio.setText("");
					editActualizacion.setText("");
					editRecargo.setText("");
					
					editImporte.setText("");
					editTotalPagar.setText("");
					editLineaCaptura.setText("");
					
					editFolios.setChoices(null);

					getData();
				}
			}
		} else if (arg0 == editFolios){
			
			if (arg1 == ChoiceField.HIGHLIGHT_SELECT){
				
				int iValue = editFolios.getSelectedIndex();
				adeudo = (Adeudo) editFolios.getChoice(iValue);
				
				editFecha.setText(adeudo.getFecha());
				editFolio.setText(adeudo.getFolio());
				
				if (adeudo.getActualizacion() != null){
					agregarCamposActualizacion();
					editActualizacion.setText(adeudo.getActualizacion());
				} else {
					borrarCamposActualizacion();
				}
				
				editRecargo.setText(adeudo.getRecargo());
				
				editImporte.setText(adeudo.getImporte());
				editTotalPagar.setText(adeudo.getTotalPago());
				editLineaCaptura.setText(adeudo.getLinea_captura());
				
			}
		} else if (arg0 == agregarPlacas){
			
			agregarPlacas();
		} else if(arg0 == pagar){

			if (listoPagar){

				pagar();
			} else {
				Dialog.alert("Falta información para generar el pago.");
			}
		}
	}
	
	
	public void setData(int request, JSONObject jsObject) {
		
		splashScreen.remove();
		
		switch (status) {
		case STATUS_TRAER_INFO:
			try {
				if (jsObject.has("error")){

					String error = jsObject.getString("error");

					if (((error != null)&&(error.length() == 0))||(error.equals("0"))){
						
						if (jsObject.has("adeudo")){
							
							jsData = jsObject;
							
							String sAdeudo = jsObject.getString("adeudo");
							
							int iAdeudo = Integer.parseInt(sAdeudo);
							
							if (iAdeudo > 0){
								
								getInfracciones(jsObject);
								
							} else {
								
								sendMessage("No hay adeudo");
							}
						}
					} else {
						
						int iError = Integer.parseInt(error);
						
						if (iError > 0){
							
							String numError = jsObject.getString("numError");
							sendMessage(numError);
						}
					}
				} else if (jsObject.has("adeudo")){
					String sAdeudo = jsObject.getString("adeudo");
					if (sAdeudo.equals("0")){
						sendMessage("No hay adeudo");
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al leer la información.");
			}

			break;

		case STATUS_INSERTA_MOV:
			String codError = jsObject.optString("codError");
			String detError = jsObject.optString("detError");
			
			if (codError.equals("0")){
				Tareas tareas = new Tareas();

				String get = tareas.toCreatePagoWeb(UserBean.nameLogin, idBitacora, String.valueOf(total), DTO.PAGO_INFRACCION);
				
				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(get);
				UiApplication.getUiApplication().popScreen(this);
			    UiApplication.getUiApplication().pushScreen(pagoWeb);
			} else {
				
				Dialog.alert(detError);
			}
			break;
			
			
		case STATUS_INSERTA_BITACORA:
			if (jsObject.has("registrado")){
				
				idBitacora = jsObject.optLong("registrado");
				
				if (idBitacora == 0){
					
					Dialog.alert("Tu pago no puede ser procesado en este momento.");
					
				} else {
					
				}
			}
			break;
		}
	}

	
	
	
	private void getInfracciones(JSONObject jsObject) throws JSONException{
		
		String importe_total = jsObject.getString("importe_total");
		String adeudo = jsObject.getString("adeudo");

		editImporteTotal.setText(UtilNumber.formatCurrency(importe_total));

		JSONArray jsonArray =  jsObject.getJSONArray("arreglo_folios");

		int length = jsonArray.length();

		Adeudo[] adeudos = new Adeudo[length];

		for(int i = 0; i < length; i++){

			JSONObject oAdeudo = (JSONObject) jsonArray.get(i);

			Adeudo adeudo2 = new Adeudo();
			adeudo2.setFolio(oAdeudo.getString("folio"));
			adeudo2.setLinea_captura(oAdeudo.getString("linea_captura"));
			adeudo2.setFecha(oAdeudo.getString("fechainfraccion"));
			
			
			adeudo2.setImporte(UtilNumber.formatCurrency(oAdeudo.getString("importe")));
			adeudo2.setTotalPago(UtilNumber.formatCurrency(oAdeudo.getString("totalPago")));
			

			if (oAdeudo.has("actualizacion")||!oAdeudo.isNull("actualizacion")){
				adeudo2.setActualizacion(null);
			} else {
				adeudo2.setActualizacion(UtilNumber.formatCurrency(oAdeudo.getString("actualizacion")));
			}

			adeudo2.setRecargo(UtilNumber.formatCurrency(oAdeudo.getString("recargos")));
			adeudo2.setMulta(UtilNumber.formatCurrency(oAdeudo.getString("dias_multa")));

			adeudos[i] = adeudo2;
		}
		
		editFolios.setChoices(adeudos);
		
		listoPagar = true;
	}
	
	
	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}


	private void getData() {

		String sPlaca = null;
		int iPlacas = editPlacas.getSelectedIndex();
		
		if (iPlacas == 0){
			sPlaca = editPlaca.getText();
			if (sPlaca != null){
				sPlaca = sPlaca.toUpperCase();
				editPlaca.setText(sPlaca);
			}
		} else {
			sPlaca = (String) editPlacas.getChoice(iPlacas);
		}

		if ((sPlaca != null)) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("toDO", DTO.STATUS_INFRACCION);
				jsonObject.put("placa", sPlaca);

				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptSensitive(UserBean.password, post);

				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}	
	
	
	private void pagar(){

		if(adeudo != null){
			
			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("folio", adeudo.getFolio());
				jsonObject.put("linea_captura", adeudo.getLinea_captura());
				jsonObject.put("importe", adeudo.getImporte());
				jsonObject.put("fechainfraccion", adeudo.getFecha());
				jsonObject.put("actualizacion", adeudo.getActualizacion());
				jsonObject.put("recargos", adeudo.getRecargo());
				jsonObject.put("dias_multa", adeudo.getMulta());
				
				
				String imei = UtilBB.getImei();
				
				jsonObject.put("id_usuario", UserBean.idLogin);
				jsonObject.put("id_producto", DTO.STATUS_INFRACCION);
				jsonObject.put("imei", imei);
				jsonObject.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
				jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
				jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
				jsonObject.put("wkey", imei);
				jsonObject.put("cx", "0.0");
				jsonObject.put("cy", "0.0");
				
				
				String json = jsonObject.toString();
				
				String data = AddcelCrypto.encryptSensitive(UserBean.password, json);

				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(data);
				UiApplication.getUiApplication().popScreen(this);
				UiApplication.getUiApplication().pushScreen(pagoWeb);
				
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			} 
		} else {
			Dialog.alert("Falta seleccionar un adeudo.");
		}
	}
	
	private void agregarPlacas() {
		
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Placa:",
				Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {

				String placa = inputField.getText();

				if (placa != null){
					DataRMS placas = new DataRMS(DataRMS.PLACAS);
					placa = placa.toUpperCase();
					placas.addData(placa);
					String sPlacas[] = placas.getData();
					editPlacas.setChoices(sPlacas);
				} else {
					Dialog.alert("Verificar valor de placa");
				}
				

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar información");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposición");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la información es inexacta.");
				e.printStackTrace();
			}
		}
	}
	
	
	private void existenCamposActualizacion() {

		isDerecho = false;

		int size = this.getFieldCount();

		for (int index3 = 0; index3 < size; index3++) {

			Field field = this.getField(index3);

			if (field instanceof HorizontalFieldManager) {

				HorizontalFieldManager fieldManager = (HorizontalFieldManager) field;

				int size2 = fieldManager.getFieldCount();

				for (int index2 = 0; index2 < size2; index2++) {

					Field field2 = fieldManager.getField(index2);

					if (field2.equals((Field) editActualizacion)) {
						indexDerecho = index3;
						fieldManagerDerecho = fieldManager;
						isDerecho = true;
					}
				}
			}
		}
	}
	
	private void borrarCamposActualizacion(){
		
		if (isDerecho){
			this.delete(fieldManagerDerecho);
			isDerecho = false;
		}
		
		this.invalidate();
	}
	
	
	private void agregarCamposActualizacion(){
		
		if (!isDerecho){
			this.insert(fieldManagerDerecho, indexDerecho);
			isDerecho = true;
		}
		
		this.invalidate();
	}
}
