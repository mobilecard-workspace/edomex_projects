package com.addcel.gdf.view.transacciones;

import java.io.IOException;
import java.util.Calendar;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;

public class VPredial extends CustomMainScreen implements FieldChangeListener, Viewable {

	
	private ObjectChoiceField editPrediales = null;
	
	private LabelField textPredial = null;
	private LabelField textConcepto = null;
	private LabelField textCuentaP = null;
	private LabelField textBimestre = null;
	private LabelField textAnio = null;
	//private LabelField textVencimiento = null;
	private LabelField textLCaptura = null;
	private LabelField textImporte = null;
	private LabelField textIVA = null;
	private LabelField textReduccion = null;
	private LabelField textTotal = null;

	private ColorEditField editPredial = null;
	private HighlightsLabelField editConcepto = null;
	private HighlightsLabelField editCuentaP = null;
	private HighlightsLabelField editBimestre = null;
	private HighlightsLabelField editAnio = null;
	//private HighlightsLabelField editVencimiento = null;
	private HighlightsLabelField editLCaptura = null;
	private HighlightsLabelField editImporte = null;
	private HighlightsLabelField editIVA = null;
	private HighlightsLabelField editReduccion = null;
	private HighlightsLabelField editTotal = null;

	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField agregar = null;
	private SlimSelectedButtonField pagar = null;
	
	private SplashScreen splashScreen;
	private String sCuentaPredial;

	private JSONObject jsData;

	public VPredial(String title) {

		super("", true);

		splashScreen = SplashScreen.getInstance();

		DataRMS placas = new DataRMS(DataRMS.PREDIAL);
		
		try {
			String sPrediales[] = placas.getData();
			editPrediales = new ObjectChoiceField("", sPrediales, 0);			
		} catch (RecordStoreFullException e) {
			Dialog.alert("Ya no se puede almacenar informaci�n");
			e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			Dialog.alert("No se encuentra en sistema de almacenamiento");
			e.printStackTrace();
		} catch (RecordStoreException e) {
			Dialog.alert("El almacenamiento no se encuentra a disposici�n");
			e.printStackTrace();
		} catch (IOException e) {
			Dialog.alert("La lectura de la informaci�n es inexacta.");
			e.printStackTrace();
		}

		textPredial = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);

		textConcepto = new ColorLabelField("Concepto: ", LabelField.NON_FOCUSABLE);
		textCuentaP = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);
		textBimestre = new ColorLabelField("Bimestre: ", LabelField.NON_FOCUSABLE);
		textAnio = new ColorLabelField("A�o: ", LabelField.NON_FOCUSABLE);
		//textVencimiento = new ColorLabelField("Vencimiento: ", LabelField.NON_FOCUSABLE);
		textLCaptura = new ColorLabelField("Captura: ", LabelField.NON_FOCUSABLE);
		textImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		textIVA = new ColorLabelField("IVA: ", LabelField.NON_FOCUSABLE);
		textReduccion = new ColorLabelField("Reducci�n: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);

		editPredial = new ColorEditField("", "");

		editConcepto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editCuentaP = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editBimestre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editAnio = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		//editVencimiento = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editImporte = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editIVA = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editReduccion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);


		consultar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		agregar = new SlimSelectedButtonField("Agregar predial", ButtonField.CONSUME_CLICK);
		pagar = new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		agregar.setChangeListener(this);
		pagar.setChangeListener(this);

		add(editPrediales);

		addHorizontalFiels(textPredial, editPredial);

		CustomButtonFieldManager manager = new CustomButtonFieldManager(consultar, agregar);
		manager.add(consultar);
		manager.add(agregar);
		add(manager);
		add(new LabelField(""));
		
		/*
		   a).-Cuenta
		   b).-A�o
		   c).-Bimestre
		   d).-Derecho
		   e).-IVA
		   f).-Linea Captura
		   g).-Total
		*/
		
		addHorizontalFiels(textCuentaP, editCuentaP);
		addHorizontalFiels(textAnio, editAnio);
		addHorizontalFiels(textBimestre, editBimestre);
		addHorizontalFiels(textImporte, editImporte);
		//addHorizontalFiels(textIVA, editIVA);
		
		addHorizontalFiels(textLCaptura, editLCaptura);
		addHorizontalFiels(textTotal, editTotal);
		
		
		//addHorizontalFiels(textConcepto, editConcepto);
		//addHorizontalFiels(textReduccion, editReduccion);

		add(pagar);
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		jsData = jsObject;

		editConcepto.setText(jsObject.optString("concepto"));
		editCuentaP.setText(jsObject.optString("cuentaP"));
		editBimestre.setText(jsObject.optString("bimestre"));
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		editAnio.setText(String.valueOf(year));
		//editVencimiento.setText(jsObject.optString("vencimiento"));
		editLCaptura.setText(jsObject.optString("linea_captura"));
		editImporte.setText(UtilNumber.formatCurrency(jsObject.optString("importe"))); //importe intImpuesto
		//editIVA.setText(UtilNumber.formatCurrency(jsObject.optString("IVA")));
		editReduccion.setText(jsObject.optString("reduccion"));
		editTotal.setText(UtilNumber.formatCurrency(jsObject.optString("totalPago")));

		//"error_cel":"",
		if (jsObject.has("error")){
			String error = jsObject.optString("error");
			
			if (error.trim().length() > 0){
				Dialog.alert(error);
			}
		}
	}

	public void sendMessage(String message) {
	}

	
	public void fieldChanged(Field field, int arg) {

		if (field == consultar){
			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				getData();
			}
		} else if(field == agregar){
			agregarPredial();
		} else if(field == pagar){
			
			try {

				//jsData.put("usuario", UserBean.idLogin);

				String imei = UtilBB.getImei();
				
				jsData.put("id_usuario", UserBean.idLogin);
				jsData.put("id_producto", DTO.STATUS_PREDIAL);
				jsData.put("imei", imei);
				jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
				jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
				jsData.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
				jsData.put("wkey", imei);
				jsData.put("cx", "0.0");
				jsData.put("cy", "0.0");
				
				String post = jsData.toString();
				
				String data = AddcelCrypto.encryptSensitive(UserBean.password, post);
				
				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(data);
				UiApplication.getUiApplication().popScreen(this);
				UiApplication.getUiApplication().pushScreen(pagoWeb);
				
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al agregar la informaci�n para mandarla.");
			}
			
			
		}
	}
	
	
	private void getData() {

		int iCuentaAgua = editPrediales.getSelectedIndex();
		
		if (iCuentaAgua == 0){
			sCuentaPredial = editPredial.getText();
			if (sCuentaPredial != null){
				sCuentaPredial = sCuentaPredial.toUpperCase();
				editPredial.setText(sCuentaPredial);
			}
		} else {
			sCuentaPredial = (String) editPrediales.getChoice(iCuentaAgua);
		}


		if (sCuentaPredial != null) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("toDO", DTO.STATUS_PREDIAL);
				jsonObject.put("cuenta", sCuentaPredial);

				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptSensitive(UserBean.password, post);

				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}	


	private void agregarPredial() {
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Predial:",
				Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {
				
				String predial = inputField.getText();
				
				if (predial != null){
					DataRMS placas = new DataRMS(DataRMS.PREDIAL);
					predial = predial.toUpperCase();
					placas.addData(predial);
					String sPlacas[] = placas.getData();
					editPrediales.setChoices(sPlacas);
				} else {
					Dialog.alert("Verificar valor de placa");
				}

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar informaci�n");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposici�n");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la informaci�n es inexacta.");
				e.printStackTrace();
			}
		}
	}
}
