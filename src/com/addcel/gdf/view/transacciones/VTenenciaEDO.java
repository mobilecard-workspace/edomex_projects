package com.addcel.gdf.view.transacciones;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.CustomSelectedSizeButton;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;

public class VTenenciaEDO extends CustomMainScreen implements FieldChangeListener, Viewable {

	private ColorLabelField textPlaca;
	private ColorLabelField textTelefono;
	
	private ColorEditField editPlaca;
	private ColorEditField editTelefono;
	
	
	private ColorLabelField titleMonto;
	private ColorLabelField titleLC;
	
	private HighlightsLabelField textMonto;
	private HighlightsLabelField textLC;
	
	private CustomSelectedSizeButton obtenerDatos = null;
	private CustomSelectedButtonField cancelar = null;
	private CustomSelectedButtonField pagar = null;
	
	private String token;
	private String monto;
	private String linea;
	private String placa;
	
	public VTenenciaEDO(String title) {
		super(title, true);

		textPlaca = new ColorLabelField("Ingresa tu placa: ", LabelField.NON_FOCUSABLE);
		textTelefono = new ColorLabelField("Ingresa tu n�mero de t�lefono: ", LabelField.NON_FOCUSABLE);
		
		editPlaca = new ColorEditField("", "");
		editTelefono = new ColorEditField("", "", 10, EditField.FILTER_INTEGER);
		
		titleMonto = new ColorLabelField("Monto a pagar:");
		titleLC = new ColorLabelField("Linea de captura:");

		textMonto = new HighlightsLabelField("", LabelField.NON_FOCUSABLE|LabelField.USE_ALL_WIDTH);
		textLC = new HighlightsLabelField("", LabelField.NON_FOCUSABLE|LabelField.USE_ALL_WIDTH);
		
		obtenerDatos = new CustomSelectedSizeButton("Obtener datos de pagos", 1);
		pagar = new CustomSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		cancelar = new CustomSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		obtenerDatos.setChangeListener(this);
		pagar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		add(textPlaca);
		add(editPlaca);
		add(new LabelField());
		add(textTelefono);
		add(editTelefono);
		add(new LabelField());
		
		add(obtenerDatos);
		
		add(new LabelField());
		add(titleMonto);
		add(textMonto);
		add(new LabelField());
		add(titleLC);
		add(textLC);
		add(new LabelField());
		
		CustomButtonFieldManager managerBtn = new CustomButtonFieldManager(pagar, cancelar);
		managerBtn.add(pagar);
		managerBtn.add(cancelar);
		add(managerBtn);
	}

	
	public void setData(int request, JSONObject jsObject) {

		System.out.println(jsObject.toString());
		
		/*
		CADENA respuesta: {"idError":1,"mensajeError":"Ocurrio un error."}
		CADENA respuesta: {"idError":0 ,"mensajeError":"null", "placa":"102EL","monto":"7876", "linea":"102002000005349701396096241" }
		{"idError":4,"linea":null,"placa":null,"mensajeError":"PLACA SIN ADEUDOS (5HRE09)","monto":null}
		 
		 {"token":"kN0YzT5PTyvyKf0g/aRhej5w3b+yMVO4F3UI2sWHWy"","idError":0,"mensajeError": null}
		 */
		
		int error = jsObject.optInt("idError", 0);
		
		if (error == 1){
			
			String mensaje = jsObject.optString("mensajeError");
			Dialog.alert(mensaje);
			
		} else if (error == 0){
			
			hacerAccion(jsObject);
			
		} else if (error == 4){
			
			String mensaje = jsObject.optString("mensajeError");
			Dialog.alert(mensaje);
		}
	}
	
	private void hacerAccion(JSONObject jsObject){
		
		placa = jsObject.optString("placa", null);
		token = jsObject.optString("token", null);
		
		if (placa != null){
		
			monto = UtilNumber.formatCurrency(jsObject.optString("monto", "0.0"));
			linea = jsObject.optString("linea");
			
			Double double1 = Double.valueOf(monto);
			
			textMonto.setText(double1.toString());
			textLC.setText(linea);
		} if (token != null){


			try {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("idUser", UserBean.idLogin);
				jsonObject.put("idProveedor", 23);
				jsonObject.put("placa", editPlaca.getText());
				jsonObject.put("monto", monto);
				jsonObject.put("linea", linea);
				jsonObject.put("token", token);
				jsonObject.put("imei", UtilBB.getImei());
				jsonObject.put("cx", 0);
				jsonObject.put("cy", 0);
				jsonObject.put("wkey", UtilBB.getImei());
				jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
				jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));

				String data = AddcelCrypto.encryptSensitive(UserBean.password, jsonObject.toString());
				
				VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
				pagoWeb.execute(data);
				UiApplication.getUiApplication().popScreen(this);
				UiApplication.getUiApplication().pushScreen(pagoWeb);
				
				/*
				PagoPlacas pagoPlacas = new PagoPlacas(this, Url.URL_EDOMEX_PAGO, jsonObject.toString());
				pagoPlacas.run();
				*/
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}


	public void sendMessage(String message) {}


	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == obtenerDatos){
			
			if ((validarEditField(editPlaca))&&(validarEditField(editTelefono))){
				
				if (UserBean.nameLogin == null){
					UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
				} else {
					
					String sPlaca = editPlaca.getText();
					String sTelefono = editTelefono.getText();
					String json = createJson(sPlaca, sTelefono);
					
					sendData(json);
				}
				
			} else {
				
			}
			
		} else if (arg0 == pagar){
			

			try {
				
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("idProveedor", 23);
				//jsonObject.put("usuario", UserBean.nameLogin);
				//jsonObject.put("password", UserBean.password);

				jsonObject.put("usuario", "userPrueba");
				jsonObject.put("password", "passwordPrueba");
				
				String data = "json=" + AddcelCrypto.encryptSensitive(UserBean.password, jsonObject.toString());
				
				PagoPlacas pagoPlacas = new PagoPlacas(this, Url.URL_EDOMEX_TOKEN, data);
				pagoPlacas.run();
				
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("La informaci�n no se pudo asignar.");
			}
			
		} else if (arg0 == cancelar){
			
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	
	
	private boolean validarEditField(EditField editField){

		boolean regreso = false;

		String string = editField.getText(); 

		string = string.toUpperCase();

		editField.setText(string);

		if ((string.length() > 0)){
			regreso = true; 
		}

		return regreso; 
	}

	
	private String createJson(String placa, String telefono){
		
		JSONObject jsonObject = new JSONObject();
		String json = null;
		
		try {
			
			jsonObject.put("placa", placa);
			jsonObject.put("telefono", telefono);

			json = jsonObject.toString();

		} catch (JSONException e) {
			e.printStackTrace();
			Dialog.alert("Error al interpretar los datos capturados.");
		} 

		return json;
	}
	
	
	private void sendData(String json){
		
		String data = "json=" + AddcelCrypto.encryptSensitive(UserBean.password, json);

		PagoPlacas pagoPlacas = new PagoPlacas(data, this, 0);
		pagoPlacas.run();
	}
}
