package com.addcel.gdf.view.util;

import net.rim.device.api.ui.Color;

public class UtilColor {

	public final static int COLOR_RED = 0xDB001B;
	public final static int COLOR_WHITE = Color.WHITE;
	
	
	public final static int BUTTON_FOCUS = 0x8CB809;
	public final static int BUTTON_SELECTED = 0x294E00;
	public final static int BUTTON_UNSELECTED = 0x294E00;
	
	public final static int SLIM_BUTTON_FOCUS = 0x8CB809;
	public final static int SLIM_BUTTON_SELECTED = 0x294E00;
	public final static int SLIM_BUTTON_UNSELECTED = 0x294E00;
	
	public final static int BUTTON_STRING = Color.WHITE; 


	public final static int LABEL_FIELD_STRING = 0X434345;
	public final static int EDIT_FIELD_STRING = 0X434345;
	public final static int EDIT_FIELD_BACKGROUND = 0xd1d3d4;
	
	public final static int BACKGROUND = Color.WHITE;
	
	public final static int TITLE_BG_TOP_LEFT = 0x434345;
	public final static int TITLE_BG_TOP_RIGHT = 0x434345;
	public final static int TITLE_BG_BOTTOM_RIGHT = 0xa3a5a8;
	public final static int TITLE_BG_BOTTOM_LEFT = 0xa3a5a8;
	
	/*
	public final static int TITLE_YE_TOP_LEFT = 0xd1d3d4;
	public final static int TITLE_YE_TOP_RIGHT = 0xd1d3d4;
	public final static int TITLE_YE_BOTTOM_RIGHT = 0xd1d3d4;
	public final static int TITLE_YE_BOTTOM_LEFT = 0xd1d3d4;
	
	
		public final static int TITLE_IMAGE_TOP_LEFT = 0xd1d3d4;
	public final static int TITLE_IMAGE_TOP_RIGHT = 0xd1d3d4;
	public final static int TITLE_IMAGE_BOTTOM_RIGHT = 0xd1d3d4;
	public final static int TITLE_IMAGE_BOTTOM_LEFT = 0xa3a5a8;
	*/
	
	public final static int BUTTON_STRING_FOCUS = COLOR_WHITE;
	public final static int BUTTON_STRING_SELECTED = COLOR_WHITE;
	public final static int BUTTON_STRING_UNSELECTED = COLOR_WHITE;
	
	public final static int TITLE_YE_TOP_LEFT = Color.WHITE;
	public final static int TITLE_YE_TOP_RIGHT = Color.WHITE;
	public final static int TITLE_YE_BOTTOM_RIGHT = Color.WHITE;
	public final static int TITLE_YE_BOTTOM_LEFT = Color.WHITE;
	
	public final static int TITLE_IMAGE_TOP_LEFT = Color.WHITE;
	public final static int TITLE_IMAGE_TOP_RIGHT = Color.WHITE;
	public final static int TITLE_IMAGE_BOTTOM_RIGHT = Color.WHITE;
	public final static int TITLE_IMAGE_BOTTOM_LEFT = Color.WHITE;

	public final static int TITLE_STRING_GDF = 0x77787b;

/*
	title
	data
	
	background = 
	focus
*/
	
	//public final static int LIST_TITLE = 0x0081c8;
	//public final static int LIST_TITLE = COLOR_RED;
	public final static int LIST_TITLE = Color.WHITE;
	public final static int LIST_DATA = 0x4d4d4f;
	//public final static int LIST_BACKGROUND = 0Xd1d3d4;
	//public final static int LIST_BACKGROUND = 0x8CB809;
	//public final static int LIST_FOCUS = 0Xc5f533;
	
	public final static int LIST_BACKGROUND = 0x769c09;
	public final static int LIST_FOCUS = 0x8CB809;
	
	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}
