package com.addcel.gdf.view.util;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.BitmapField;

public class UtilIcon {

	public static BitmapField getTitle(){

		String resolution = "enGrande" + String.valueOf(Display.getWidth())  + ".png";
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}

	
	public static BitmapField getSplash(){
		
		int width = Display.getWidth();
		int height = Display.getHeight();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("splash");
		stringBuffer.append(width);
		stringBuffer.append(".png");

		String splash = stringBuffer.toString();

		Bitmap encodedImage = Bitmap.getBitmapResource(splash);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}
}
