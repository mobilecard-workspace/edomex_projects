package com.addcel.gdf.dto;

public class DTO {

	public final static int STATUS_NOMINA = 1;
	public final static int STATUS_TENENCIA = 2;
	public final static int STATUS_INFRACCION = 3;
	public final static int STATUS_PREDIAL = 4;
	public final static int STATUS_AGUA = 5;

	public static String PAGO_TENENCIA = "101";
	public static String PAGO_INFRACCION = "102";
	public static String PAGO_NOMINA = "103";
}
