package com.addcel.edomex.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.edomex.model.mapper.BitacorasMapper;
import com.addcel.edomex.model.mapper.EdoMexMapper;
import com.addcel.edomex.model.vo.DetalleVO;
import com.addcel.edomex.model.vo.EdoMexDetalleVO;
import com.addcel.edomex.model.vo.pagos.ProcomVO;
import com.addcel.edomex.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.edomex.model.vo.pagos.TBitacoraVO;
import com.addcel.edomex.model.vo.pagos.TransactionProcomVO;
import com.addcel.edomex.model.vo.servicios.VentanillaRequest;
import com.addcel.edomex.model.vo.servicios.VentanillaResponse;
import com.addcel.edomex.utils.AddCelGenericMail;
import com.addcel.edomex.utils.AxisSSLSocketFactory;
import com.addcel.edomex.utils.UtilsService;
import com.addcel.edomex.utils.encryption.TripleDES;
import com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanillaProxy;
import com.addcel.utils.AddcelCrypto;

@Service
public class EdoMexPagosService {
	private static final Logger logger = LoggerFactory.getLogger(EdoMexPagosService.class);

	@Autowired 
	private EdoMexMapper mapper;
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private EdoMexMapper mapperEM;
		
	public ProcomVO procesaPago(String jsonEnc){
		VentanillaRequest ventRequest = null;
		String json = null;
		ProcomVO procom = null;
//			token=mapper.getFechaActual();
		json=AddcelCrypto.decryptSensitive(jsonEnc);		
		logger.debug("jsondatospago: {}",json);
		ventRequest = (VentanillaRequest) utilService.jsonToObject(json, VentanillaRequest.class);
		if(ventRequest.getToken() == null){
			json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			ventRequest.setToken(AddcelCrypto.decryptSensitive(ventRequest.getToken()));
			logger.info("token ==> {}",ventRequest.getToken());
			
				if((mapperEM.difFechaMin(ventRequest.getToken())) < 31){
					logger.info("Transaccio Valida, continuando con los insert en base datos");
	//					total=calculaTotal(comisiones,ventRequest.get() );
					long idBitacora = insertaBitacoras(ventRequest);
					procom = comercioFin(ventRequest.getIdUser(), String.valueOf(idBitacora), 
							ventRequest.getMonto(), "0", ventRequest.getEmail());				
					
				}else{
					logger.error("La Transacción ya no es válida");
				}
		}			
		return procom;
	}	
	
	private long insertaBitacoras(VentanillaRequest ventRequest ){		
		TBitacoraVO tb = new TBitacoraVO(
				ventRequest.getIdUser(), "PAGO EDOMEX TENENCIA 3DSECURE", ventRequest.getImei(), "PAGO EDOMEX TENENCIA 3DSECURE",
				null,ventRequest.getTipo(), ventRequest.getSoftware(), ventRequest.getModelo(),ventRequest.getWkey());
		bm.addBitacora(tb);
		
		TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
				ventRequest.getIdUser(), null,null,null, "PAGO EDOMEX TENENCIA 3DSECURE",
				ventRequest.getMonto(),"0.0", ventRequest.getCx(), ventRequest.getCy());
		
		EdoMexDetalleVO edomexDet = new EdoMexDetalleVO(
				tb.getIdBitacora(), ventRequest.getIdUser(),"Tenencia", 
				ventRequest.getMonto(), "1", ventRequest.getPlaca(), ventRequest.getLinea());
		
		bm.addBitacoraProsa(tbProsa);
		bm.addEDOMEXDetalle(edomexDet);
		return tb.getIdBitacora();
	}
	
	
	private static final String varMerchant = "7711336";
	private static final String varOrderId = "";
	private static final String varStore ="1234";
	private static final String varTerm = "001";
	private static final String varCurrency = "484";
	private static final String varAddress = "PROSA";    
	
	private ProcomVO comercioFin(String user,String referencia,String monto,String idTramite,String email) {    	        
        String varTotal=formatoMontoProsa(monto);      
        //cambiar por datos correctos
        String digest = digestEnvioProsa(varMerchant, varOrderId, varTerm, varTotal, varCurrency, referencia);   
        logger.info("Digest EdoMex: {}",digest);
        							//(total, currency, addres, orderId, merchant, store, term, digest, urlBack, usuario, idTramite)        
    	ProcomVO procomObj = new ProcomVO(varTotal, varCurrency, varAddress, referencia, varMerchant, varStore, varTerm, digest, "", user, idTramite,email);
    	
    	logger.info("User EdoMex: {}",user);
    	logger.info("Referencia EdoMex: {}",referencia);
    	logger.info("IdTramite EdoMex: {}",idTramite);
    	
    	return procomObj;
    }
	
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+centavos;
        }else{
            varTotal=monto.concat("00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
	private String digest(String text){
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	
	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {
		StringBuffer digestCad = new StringBuffer()
				.append(transactionProcomV.getEmTotal())
				.append(transactionProcomV.getEmOrderID())
				.append(transactionProcomV.getEmMerchant())
				.append(transactionProcomV.getEmStore())
				.append(transactionProcomV.getEmTerm())
				.append(transactionProcomV.getEmRefNum())
				.append("-")
				.append(transactionProcomV.getEmAuth());
				
		return digest(digestCad.toString());		
	}			
	
	public VentanillaResponse procesaRespuestaProsa(TransactionProcomVO tp) {
		// insertar bitacora ecommerce
		// Valida digest,
		List<DetalleVO> detalleVO = null;
		VentanillaResponse ventanillaResponse = new VentanillaResponse();
		
		String digest = digestRegresoProsa(tp);
		TBitacoraVO b = new TBitacoraVO();
		String patron = "000000";
    	String patron2 = "000000000000";
    	DecimalFormat formato = new DecimalFormat(patron);
    	DecimalFormat formato2 = new DecimalFormat(patron2);
    	
    	Random  rnd = new Random();
    	
    	int aut = (int)(rnd.nextDouble() * 900000);
    	long ref = (long)(rnd.nextDouble() * 900000000);
    	
    	tp.setEmAuth(formato.format(aut));
    	tp.setEmRefNum(formato2.format(ref));
	//	if (tp.getEmDigest() != null && tp.getEmDigest().equals(digest)) {
			if (!tp.getEmAuth().equals("000000")) {		
				//Notificacion de pago a Edo Mex
				logger.info("Inicia notificacion de pago a Edo Mex");
				bm.updateEDOMEXDetalle(Integer.parseInt(tp.getEmOrderID()), 1);
				
				detalleVO = mapperEM.getDetalle(null, null, null, tp.getEmOrderID());
				if(detalleVO != null && detalleVO.size() >0){
					
					ventanillaResponse = aproviEdoMex(detalleVO.get(0).getLinea() , detalleVO.get(0).getTotal(), tp.getEmAuth());
					//Fin
					
					ventanillaResponse.setAutorizacion(tp.getEmAuth());
					ventanillaResponse.setLinea(detalleVO.get(0).getLinea());
					ventanillaResponse.setMonto(detalleVO.get(0).getTotal());
					ventanillaResponse.setPlaca(detalleVO.get(0).getPlaca());
					ventanillaResponse.setReferencia(tp.getEmOrderID());
					detalleVO.get(0).setReferencia(tp.getEmAuth());
					
					b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
					b.setBitStatus(ventanillaResponse.getParametro() == 61 ? 1: 3);
//					b.setBitStatus(1);
					b.setBitConcepto("PAGO EDOMEX TENENCIA 3DSECURE EXITOSO");
					b.setBitNoAutorizacion(tp.getEmAuth());
					bm.updateBitacora(b);
					// idStatus
					bm.updateEDOMEXDetalle(Integer.parseInt(tp.getEmOrderID()), b.getBitStatus());
					// envio correo
					
					logger.info("Pago correcto");
				
					logger.info("Se obtuvieron datos, inicia proceso de envio de correo.");
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
				}else{
					logger.error("ID_BITACORA: " + tp.getEmOrderID() + ", No se obtuvieron datos para inicia proceso de aprovicionamiento y envio de correo.");
				}
				
				
			} else {					
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(2);
				b.setBitConcepto("PAGO EDOMEX TENENCIA 3DSECURE NO AUTORIZADA");
				b.setBitNoAutorizacion(tp.getEmAuth());
				bm.updateBitacora(b);
				// idStatus
				bm.updateEDOMEXDetalle(Integer.parseInt(tp.getEmOrderID()), 2);

				logger.error("Transacción no autorizada: {}", tp.getEmAuth());
				
				ventanillaResponse.setIdError(2);
				ventanillaResponse.setMensajeError("Transacción no autorizada: " + tp.getEmAuth());
			}
//		} else {
//			b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
//			b.setBitStatus(2);
//			b.setBitConcepto("PAGO DUTTY FREE 3DSECURE ERROR DIGEST");
//			b.setBitNoAutorizacion(tp.getEmAuth());
//			bm.updateaddBitacora(b);
//			// idStatus
//			bm.updateDfDetalle(Integer.parseInt(tp.getEmOrderID()), 2);
//			logger.error("Digest incorrecto: INFORMACIÃ“N INVÃ�LIDA ");
//			paginaRespuesta = "error";
//		}
		return ventanillaResponse;
	}	
	
	private static final String sucursal = "04112008";
	private static final String plazo = "0";
	private static final String cvmedio = "1";
	private static final String cvcentro = "34";//Clave centro
    //String cvcentro = "5";
	private static final String cuenta = "4055787410";
	private static final String forma = "2";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	private VentanillaResponse aproviEdoMex(String referencia, String importe, String autorizacion){
		WSRegresoVentanillaProxy regresoVentanillaProxy = null;
		VentanillaResponse ventanillaResponse = new VentanillaResponse();
		TripleDES enc = new TripleDES();
		
		Date dia = new Date();    
		String fecha = null;
		int respuesta = 0;
		String auth = autorizacion;
		try{
		 
		    //Encriptacion de campos       
		    importe = enc.EncryptDesarrollo(importe);
		    autorizacion = enc.EncryptDesarrollo(autorizacion);  
		    referencia = enc.EncryptDesarrollo(referencia);
		    fecha =  sdf.format(dia);
		    
		    regresoVentanillaProxy = new WSRegresoVentanillaProxy();
		
			AxisSSLSocketFactory.setKeystorePassword("edomexkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/edomex/utils/EdoMexkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.edomex.utils.AxisSSLSocketFactory");
		    
//			System.setProperty("javax.net.ssl.trustStoreType","jks");
//			System.setProperty("javax.net.ssl.trustStore","/usr/java/jboss-6.1.0.Final/server/default/certificados/EdoMexkey.jks");
//			System.setProperty("javax.net.ssl.trustStorePassword","edomexkeypass");
//			System.setProperty("javax.net.debug","ssl");

			respuesta = regresoVentanillaProxy.getRegistraPago(cvmedio, cvcentro, sucursal, cuenta, forma, referencia, importe, fecha, fecha, autorizacion, plazo);
			
			logger.info("********************************************************************");
			logger.info("Respuesta EDOMEX Pago: {}",respuesta);
			
			ventanillaResponse.setParametro(respuesta);
			ventanillaResponse.setAutorizacion(auth);
		
		}catch(Exception e){
			logger.info("Ocurrio un error en la respuesta EDOMEX Pago: {}",e.getMessage());
			e.printStackTrace();
			ventanillaResponse.setIdError(1);
			ventanillaResponse.setMensajeError("Ocurrio un error: " + e.getMessage());
		}
		
		return ventanillaResponse;
	}
	
}
