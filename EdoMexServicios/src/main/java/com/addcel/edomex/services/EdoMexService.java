package com.addcel.edomex.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.edomex.model.mapper.EdoMexMapper;
import com.addcel.edomex.model.vo.DetalleVO;
import com.addcel.edomex.model.vo.RequestBusquedaPagos;
import com.addcel.edomex.model.vo.TokenVO;
import com.addcel.edomex.model.vo.servicios.LineaRequest;
import com.addcel.edomex.model.vo.servicios.LineaResponse;
import com.addcel.edomex.utils.AddCelGenericMail;
import com.addcel.edomex.utils.AxisSSLSocketFactory;
import com.addcel.edomex.utils.Utils;
import com.addcel.edomex.utils.UtilsService;
import com.addcel.edomex.ws.clientes.getlinea.WSMensajesProxy;
import com.addcel.utils.AddcelCrypto;

@Service
public class EdoMexService {
	private static final Logger logger = LoggerFactory.getLogger(EdoMexService.class);
	
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	@Autowired
	private EdoMexMapper mapper;
	@Autowired
	private UtilsService utilService;	
	
		
	public String generaToken(String json){
		TokenVO tokenVO = null;
		try{
			tokenVO = (TokenVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(json), TokenVO.class);
			if(!"userPrueba".equals(tokenVO.getUsuario()) || !"passwordPrueba".equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este servicio.\"}";
			}else{
				json="{\"token\":\""+AddcelCrypto.encryptSensitive(formato.format(new Date()),mapper.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al generar el Token: {}", e.getMessage());
			json = "{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}finally{
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		}
		return json;
	}

	
	public String busquedaPagos(String data) {
		List<DetalleVO> resp = null;
		try{
			RequestBusquedaPagos request = 
					(RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			resp = mapper.getDetalle(request.getIdUser(), request.getAnio(), request.getMes(), null);
			if(resp.size() > 0){
				data = "{\"idError\":0,\"mensajeError\":\"\" , \"pagos\":"+ utilService.objectToJson( resp )+"}";
			}else{
				data = "{\"idError\":1,\"mensajeError\":\"No existen registro para mostrar.\" , \"pagos\":[]}";
			}
		}catch(Exception e){
			data = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al obtener la lista.\" , \"pagos\":[]}";
		}
		return AddcelCrypto.encryptHard(data);
	}
	
	public String reenvioRecibo(String data) {
		List<DetalleVO> detalleVO = null;
		RequestBusquedaPagos request = null;
		String json = null;
		try{
			request = (RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			if(request.getIdUser() == null){
				json = "{\"idError\":2,\"mensajeError\":\"Falta el parametro idUsuario.\"}";
			}else if(request.getIdBitacora() == null){
				json = "{\"idError\":3,\"mensajeError\":\"Falta el parametro idBitacora.\"}";
			}else{
				
				detalleVO =  mapper.getDetalle(request.getIdUser(), null, null, request.getIdBitacora());
				
				if(detalleVO != null && detalleVO.size() >0){
					logger.info("Se obtuvieron datos, inicia proceso de reenvio de correo.");
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
					json = "{\"idError\":0,\"mensajeError\":\"Recibo reenviado correctamente.\"}";
				}else{
					logger.error("ID_BITACORA: " + request.getIdBitacora() + ", No se obtuvieron datos para inicia proceso de reenvio del recibo.");
					json = "{\"idError\":1,\"mensajeError\":\"No se obtuvieron datos para inicia el proceso de reenvio del recibo.\"}";
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error al reenviar el recibo: {}", e.getMessage());
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al reenviar el recibo.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;	
	}
	
	public String getLinea(String json) {
		LineaRequest lineaRequest = null;
		LineaResponse lineaResponse = new LineaResponse();
		WSMensajesProxy  mensajesProxy = null;
		String respuesta = null;
		int idResponse = 0;
		try{
			lineaRequest = (LineaRequest) utilService.jsonToObject(
					AddcelCrypto.decryptSensitive(json), LineaRequest.class);
			
			mensajesProxy = new WSMensajesProxy();
			
//			AxisSSLSocketFactory.setKeystorePassword("edomexkeypass");
//			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/edomex/utils/EdoMexKey.jks");
//			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.edomex.utils.AxisSSLSocketFactory");

			respuesta = mensajesProxy.getLinea(
					"12", String.valueOf(new Date().getTime()), lineaRequest.getTelefono(), "TENENCIA " + lineaRequest.getPlaca());
					
			idResponse = Utils.ValidateLine(respuesta, lineaRequest.getPlaca());

			if(idResponse == 0){
				//lineaResponse.setMonto("1234.56");
		        lineaResponse.setLineParams(respuesta);
		    } else {
		    	lineaResponse.setMensajeError(respuesta);
		    	lineaResponse.setIdError(idResponse);
		    }
		}catch(Exception e){
			logger.error("Ocurrio un error al obtenr la Linea de Captura: {}", e.getMessage());
			lineaResponse.setMensajeError("Ocurrio un error: " + e.getMessage());
	    	lineaResponse.setIdError(-1);
		}
		
		
		return AddcelCrypto.encryptSensitive(formato.format(new Date()),utilService.objectToJson(lineaResponse));
	}
	
	
}
