package com.addcel.edomex.ws.clientes.ventanilla;

public class WSRegresoVentanillaProxy implements com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType {
  private String _endpoint = null;
  private com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType wSRegresoVentanilla_PortType = null;
  
  public WSRegresoVentanillaProxy() {
    _initWSRegresoVentanillaProxy();
  }
  
  public WSRegresoVentanillaProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSRegresoVentanillaProxy();
  }
  
  private void _initWSRegresoVentanillaProxy() {
    try {
      wSRegresoVentanilla_PortType = (new com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_ServiceLocator()).getWSRegresoVentanillaPort();
      if (wSRegresoVentanilla_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSRegresoVentanilla_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSRegresoVentanilla_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSRegresoVentanilla_PortType != null)
      ((javax.xml.rpc.Stub)wSRegresoVentanilla_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType getWSRegresoVentanilla_PortType() {
    if (wSRegresoVentanilla_PortType == null)
      _initWSRegresoVentanillaProxy();
    return wSRegresoVentanilla_PortType;
  }
  
  public int getRegistraPago(java.lang.String cvmedio, java.lang.String cvcentro, java.lang.String sucursal, java.lang.String cuenta, java.lang.String forma, java.lang.String referencia, java.lang.String importe, java.lang.String fechapago, java.lang.String fechaaplic, java.lang.String autorizacion, java.lang.String plazo) throws java.rmi.RemoteException{
    if (wSRegresoVentanilla_PortType == null)
      _initWSRegresoVentanillaProxy();
    return wSRegresoVentanilla_PortType.getRegistraPago(cvmedio, cvcentro, sucursal, cuenta, forma, referencia, importe, fechapago, fechaaplic, autorizacion, plazo);
  }
  
  
}