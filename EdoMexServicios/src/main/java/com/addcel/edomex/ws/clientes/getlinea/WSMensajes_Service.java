/**
 * WSMensajes_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.getlinea;

public interface WSMensajes_Service extends javax.xml.rpc.Service {
    public java.lang.String getWSMensajesPortAddress();

    public com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType getWSMensajesPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType getWSMensajesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
