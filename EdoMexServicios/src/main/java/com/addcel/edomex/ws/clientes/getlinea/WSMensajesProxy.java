package com.addcel.edomex.ws.clientes.getlinea;

public class WSMensajesProxy implements com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType {
  private String _endpoint = null;
  private com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType wSMensajes_PortType = null;
  
  public WSMensajesProxy() {
    _initWSMensajesProxy();
  }
  
  public WSMensajesProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSMensajesProxy();
  }
  
  private void _initWSMensajesProxy() {
    try {
      wSMensajes_PortType = (new com.addcel.edomex.ws.clientes.getlinea.WSMensajes_ServiceLocator()).getWSMensajesPort();
      if (wSMensajes_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSMensajes_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSMensajes_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSMensajes_PortType != null)
      ((javax.xml.rpc.Stub)wSMensajes_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType getWSMensajes_PortType() {
    if (wSMensajes_PortType == null)
      _initWSMensajesProxy();
    return wSMensajes_PortType;
  }
  
  public java.lang.String getLinea(java.lang.String cea, java.lang.String transaccion, java.lang.String telefono, java.lang.String texto) throws java.rmi.RemoteException{
    if (wSMensajes_PortType == null)
      _initWSMensajesProxy();
    return wSMensajes_PortType.getLinea(cea, transaccion, telefono, texto);
  }
  
  
}