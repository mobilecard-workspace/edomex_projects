/**
 * WSRegresoVentanilla_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.ventanilla;

public interface WSRegresoVentanilla_Service extends javax.xml.rpc.Service {
    public java.lang.String getWSRegresoVentanillaPortAddress();

    public com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType getWSRegresoVentanillaPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType getWSRegresoVentanillaPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
