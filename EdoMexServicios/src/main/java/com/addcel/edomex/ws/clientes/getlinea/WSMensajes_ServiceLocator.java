/**
 * WSMensajes_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.getlinea;

public class WSMensajes_ServiceLocator extends org.apache.axis.client.Service implements com.addcel.edomex.ws.clientes.getlinea.WSMensajes_Service {

    public WSMensajes_ServiceLocator() {
    }


    public WSMensajes_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSMensajes_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSMensajesPort
    private java.lang.String WSMensajesPort_address = "http://pilotodgr.edomex.gob.mx/WSMensajes/WSMensajesPort";

    public java.lang.String getWSMensajesPortAddress() {
        return WSMensajesPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSMensajesPortWSDDServiceName = "WSMensajesPort";

    public java.lang.String getWSMensajesPortWSDDServiceName() {
        return WSMensajesPortWSDDServiceName;
    }

    public void setWSMensajesPortWSDDServiceName(java.lang.String name) {
        WSMensajesPortWSDDServiceName = name;
    }

    public com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType getWSMensajesPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSMensajesPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSMensajesPort(endpoint);
    }

    public com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType getWSMensajesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.edomex.ws.clientes.getlinea.WSMensajes_BindingStub _stub = new com.addcel.edomex.ws.clientes.getlinea.WSMensajes_BindingStub(portAddress, this);
            _stub.setPortName(getWSMensajesPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSMensajesPortEndpointAddress(java.lang.String address) {
        WSMensajesPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.edomex.ws.clientes.getlinea.WSMensajes_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.edomex.ws.clientes.getlinea.WSMensajes_BindingStub _stub = new com.addcel.edomex.ws.clientes.getlinea.WSMensajes_BindingStub(new java.net.URL(WSMensajesPort_address), this);
                _stub.setPortName(getWSMensajesPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSMensajesPort".equals(inputPortName)) {
            return getWSMensajesPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://wslinea/", "WSMensajes");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://wslinea/", "WSMensajesPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSMensajesPort".equals(portName)) {
            setWSMensajesPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
