/**
 * WSMensajes_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.getlinea;

public interface WSMensajes_PortType extends java.rmi.Remote {
    public java.lang.String getLinea(java.lang.String cea, java.lang.String transaccion, java.lang.String telefono, java.lang.String texto) throws java.rmi.RemoteException;
}
