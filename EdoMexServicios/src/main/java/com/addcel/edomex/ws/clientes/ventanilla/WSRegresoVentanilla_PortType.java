/**
 * WSRegresoVentanilla_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.ventanilla;

public interface WSRegresoVentanilla_PortType extends java.rmi.Remote {
    public int getRegistraPago(java.lang.String cvmedio, java.lang.String cvcentro, java.lang.String sucursal, java.lang.String cuenta, java.lang.String forma, java.lang.String referencia, java.lang.String importe, java.lang.String fechapago, java.lang.String fechaaplic, java.lang.String autorizacion, java.lang.String plazo) throws java.rmi.RemoteException;
}
