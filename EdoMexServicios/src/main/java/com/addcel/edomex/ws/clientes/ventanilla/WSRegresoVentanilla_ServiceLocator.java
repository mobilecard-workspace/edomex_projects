/**
 * WSRegresoVentanilla_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.edomex.ws.clientes.ventanilla;

public class WSRegresoVentanilla_ServiceLocator extends org.apache.axis.client.Service implements com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_Service {

    public WSRegresoVentanilla_ServiceLocator() {
    }


    public WSRegresoVentanilla_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSRegresoVentanilla_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSRegresoVentanillaPort
    private java.lang.String WSRegresoVentanillaPort_address = "https://pilotodgr.edomex.gob.mx/WSRegresoVentanilla/WSRegresoVentanillaPort";

    public java.lang.String getWSRegresoVentanillaPortAddress() {
        return WSRegresoVentanillaPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSRegresoVentanillaPortWSDDServiceName = "WSRegresoVentanillaPort";

    public java.lang.String getWSRegresoVentanillaPortWSDDServiceName() {
        return WSRegresoVentanillaPortWSDDServiceName;
    }

    public void setWSRegresoVentanillaPortWSDDServiceName(java.lang.String name) {
        WSRegresoVentanillaPortWSDDServiceName = name;
    }

    public com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType getWSRegresoVentanillaPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSRegresoVentanillaPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSRegresoVentanillaPort(endpoint);
    }

    public com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType getWSRegresoVentanillaPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_BindingStub _stub = new com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_BindingStub(portAddress, this);
            _stub.setPortName(getWSRegresoVentanillaPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSRegresoVentanillaPortEndpointAddress(java.lang.String address) {
        WSRegresoVentanillaPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_BindingStub _stub = new com.addcel.edomex.ws.clientes.ventanilla.WSRegresoVentanilla_BindingStub(new java.net.URL(WSRegresoVentanillaPort_address), this);
                _stub.setPortName(getWSRegresoVentanillaPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSRegresoVentanillaPort".equals(inputPortName)) {
            return getWSRegresoVentanillaPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://wsventanilla/", "WSRegresoVentanilla");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://wsventanilla/", "WSRegresoVentanillaPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSRegresoVentanillaPort".equals(portName)) {
            setWSRegresoVentanillaPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
