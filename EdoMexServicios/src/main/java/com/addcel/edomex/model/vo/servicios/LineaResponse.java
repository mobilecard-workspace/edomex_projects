package com.addcel.edomex.model.vo.servicios;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class LineaResponse {
    private String placa;
    private String monto;
    private String linea;
    private int idError;
	private String mensajeError;

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    /**
     * Método que asigna parametros a partir del String devuelto por WSGetLinea
     * @param wsResponse String devuelto por WSGetLinea
     */
    public void setLineParams(String wsResponse){
        if(!"".equals(wsResponse) && null != wsResponse){
            String [] wsResponseStrings = wsResponse.split(" ");
            this.placa = wsResponseStrings[0];
            this.monto = wsResponseStrings[1].replace("$", "");
            this.linea = wsResponseStrings[3];
        }
    }

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}
