package com.addcel.edomex.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RequestBusquedaPagos  {

	private String mes;
	private String anio;
	private String idProveedor;
	private String idUser;
	private String idBitacora;
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	
}