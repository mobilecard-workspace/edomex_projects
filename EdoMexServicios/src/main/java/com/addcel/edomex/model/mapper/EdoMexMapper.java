package com.addcel.edomex.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.edomex.model.vo.DetalleVO;

public interface EdoMexMapper {

	String getFechaActual();

	int difFechaMin(String fechaToken);

	List<DetalleVO> getDetalle(
			@Param(value = "idUser") String idUser,
			@Param(value = "anio") String anio,
			@Param(value = "mes") String mes,
			@Param(value = "idBitacora") String idBitacora);

}
