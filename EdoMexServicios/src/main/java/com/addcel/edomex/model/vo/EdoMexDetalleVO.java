package com.addcel.edomex.model.vo;

import java.io.Serializable;

public class EdoMexDetalleVO implements Serializable{
	
	private static final long serialVersionUID = 6587661250744212275L;
	
	private long idBitacora;
	private String idUsuario;
	private String fecha;
	private int status;
	private String producto;
	private String total;
	private String moneda;
	private String placa;
    private String linea;
	

	public EdoMexDetalleVO() {}
	
	public EdoMexDetalleVO(long idBitacora, String idUsuario,
			String producto, String total,
			String moneda, String placa,
			String linea) {
		super();		
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;				
		this.producto = producto;
		this.total = total;
		this.moneda = moneda;
		this.placa = placa;
		this.linea = linea;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}
}
