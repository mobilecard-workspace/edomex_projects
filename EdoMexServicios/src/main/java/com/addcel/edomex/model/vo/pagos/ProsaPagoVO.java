package com.addcel.edomex.model.vo.pagos;

public class ProsaPagoVO {
	private String toDo;
	private Object detallePago;
	private TBitacoraVO tBitacoraVO;
	
	public String getToDo() {
		return toDo;
	}
	public void setToDo(String toDo) {
		this.toDo = toDo;
	}
	public Object getDetallePago() {
		return detallePago;
	}
	public void setDetallePago(Object detallePago) {
		this.detallePago = detallePago;
	}
	public TBitacoraVO gettBitacoraVO() {
		return tBitacoraVO;
	}
	public void settBitacoraVO(TBitacoraVO tBitacoraVO) {
		this.tBitacoraVO = tBitacoraVO;
	}
	
	

}
