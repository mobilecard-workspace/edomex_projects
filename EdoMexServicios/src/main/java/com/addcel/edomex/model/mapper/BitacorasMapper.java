package com.addcel.edomex.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.edomex.model.vo.EdoMexDetalleVO;
import com.addcel.edomex.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.edomex.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	int addBitacoraProsa(TBitacoraProsaVO b);
	int addBitacora(TBitacoraVO b);
	int updateBitacoraProsa(TBitacoraProsaVO b);
	int updateBitacora(TBitacoraVO b);
	int addEDOMEXDetalle(EdoMexDetalleVO edomexDet);
	int updateEDOMEXDetalle(@Param(value="idBitacora")int idBitacora,@Param(value="status")int status);
}
