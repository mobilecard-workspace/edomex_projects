/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.edomex.utils.encryption;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import sun.misc.BASE64Encoder;

/**
 *
 * @author mans
 */
public class TripleDES {
    private  String keyProduccion = "CA6F345E0EC7573B39E9247EDEE01956AE78C1A8268DB516";
    private  String ivProduccion = "BA4881F43847A630";  
    
    private String keyDesarrollo = "DEE01956AE78C1A8268DB516CA6F345E0EC7573B39E9247E";
    private String ivDesarrollo = "3847A630BA4881F4";
    
    
    /*IV  = "BA4881F43847A630";
    KEY = "CA6F345E0EC7573B39E9247EDEE01956AE78C1A8268DB516";*/
            
    /**
     * Metodo que cifra texto con algoritmo 3DES
     * @param TextoIn String a cifrar
     * @return String cifrada con algoritmo 3DES
     */
    public String EncryptProduccion(String TextoIn){
        try{
            byte [] KEYBytes = toBytes(keyProduccion);
            byte [] IVBytes = toBytes(ivProduccion);
            SecretKeyFactory SKF = SecretKeyFactory.getInstance("DESede");
            DESedeKeySpec DEKS = new DESedeKeySpec(KEYBytes);
            javax.crypto.SecretKey SK = SKF.generateSecret(DEKS);
            IvParameterSpec IVParam = new IvParameterSpec(IVBytes);
            Cipher Encripta = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            Encripta.init(1, SK, IVParam);
            byte TextoUTF[] = TextoIn.getBytes("UTF8");
            byte Salida[] = Encripta.doFinal(TextoUTF);
      
            return (new BASE64Encoder()).encode(Salida);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return "";
        }
    }
    
    /**
     * Metodo para convertir un String en un arreglo de 8 bytes
     * @param s String a convertir
     * @return Un arreglo de 8 bytes
     */
    private byte[] toBytes(String s){
        byte Resultado[] = null;
        try {
            Resultado = new byte[s.length() / 2];
            for (int i = 0; i < Resultado.length; i++){
                Resultado[i] = (byte)Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
            }
            return Resultado;
        }catch(Exception e){
            return Resultado;
        }
    }
    
    
    public String EncryptDesarrollo(String TextoIn){
        try{
            byte [] KEYBytes = toBytes(keyDesarrollo);
            byte [] IVBytes = toBytes(ivDesarrollo);
            SecretKeyFactory SKF = SecretKeyFactory.getInstance("DESede");
            DESedeKeySpec DEKS = new DESedeKeySpec(KEYBytes);
            javax.crypto.SecretKey SK = SKF.generateSecret(DEKS);
            IvParameterSpec IVParam = new IvParameterSpec(IVBytes);
            Cipher Encripta = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            Encripta.init(1, SK, IVParam);
            byte TextoUTF[] = TextoIn.getBytes("UTF8");
            byte Salida[] = Encripta.doFinal(TextoUTF);
      
            return (new BASE64Encoder()).encode(Salida);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return "";
        }
    }                  
}
