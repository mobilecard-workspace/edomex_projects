
package com.addcel.edomex.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
    public static int ValidateLine(String linea, String placa){
        
        if(!linea.equals("")){
        
            logger.info("::::REGRESO LINEA DE PAGO:::" + linea);

            String lineaCaps = linea.toUpperCase();

            if(lineaCaps.contains(ErroresEdoMex.ERROR_10)){
                    return 10;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_1)){
                    return 1;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_44)){
                    return 44;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_45)){
                    return 45;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_2)){
                    return 2;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_3)){
                    return 3;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_4)){
                    return 4;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_5)){
                    return 5;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_6)){
                    return 6;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_7)){
                    return 7;  		
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_8)){
                    return 8;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_62)){
                    return 62;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_63)){
                    return 63;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_64)){
                    return 64;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_65)){
                    return 65;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_67)){
                    return 67;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_68)){
                    return 68;
            }
            else if(lineaCaps.contains(ErroresEdoMex.ERROR_69)){
                    return 69;
            }
             else if(lineaCaps.contains(ErroresEdoMex.ERROR_70)){
                    return 70;
            } 
             else if(lineaCaps.contains(ErroresEdoMex.ERROR_71)){
            	 	return 71;
            }
            else {
                return 0;
            }
        }
        else{
            return -1;
        }
    }
    
    public static String GetMensaje(int resultado){
        switch(resultado){
            case 10: return "Error en WebServices";
            case 46: return "Parámetro Clavemedio no enviado";
            case 47: return "Parámetro Formapago no enviado";
            case 48: return "Parámetro Referencia no enviado";
            case 49: return "Longitud de parámetro Referencia incorrecto";
            case 50: return "Parámetro Importe no enviado";
            case 51: return "Formato de parámetro Importe incorrecto";
            case 52: return "Parámetro Fechapago no enviado";
            case 53: return "Formato de parámetro Fechapago incorrecto";
            case 54: return "Parámetro Fechaaplic no enviado";
            case 55: return "Formato de parámetro Fechaaplic incorrecto";
            case 56: return "Parámetro Autorizacion no enviado";
            case 57: return "Parámetro Plazo no enviado";
            case 58: return "Parámetro Sucursal no enviado";
            case 59: return "Parámetro Clavecentro no enviado";
            case 60: return "Parámetro Cuentadeposito no enviado";
            case 66: return "La línea de referencia ya estaba registrada";
            
            default: return "Error de Conexión";
        }
    }
    
    public static String random() {
        
        long number = (long) Math.floor(Math.random() * 9000000L) + 1000000L;

        logger.info(":::RANDOM GENERADO");
        logger.info("" + number);
        logger.info(":::");
        return Long.toString(number);
    }
    
}
