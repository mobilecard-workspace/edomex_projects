/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.edomex.utils;

/**
 *
 * @author Carlos
 */
public class ErroresEdoMex {
    public static final String PLACA = "";
    public static final String ERROR_10 = "NULO";
    public static final String ERROR_1  = "CENTRO DE EMISOR AUTORIZADO INCORRECTO";
    public static final String ERROR_44 = "SERVICIO NO DEFINIDO";
    public static final String ERROR_45 = "CEA INACTIVO";
    public static final String ERROR_2  = "ACUDA AL CSF O MAC MAS CERCANO";
    public static final String ERROR_3  = "NO EXISTE LA PLACA";
    public static final String ERROR_4  = "PLACA SIN ADEUDOS";
    public static final String ERROR_5  = "PLACA INVALIDA";
    public static final String ERROR_6  = "LONGITUD INVALIDA DEL SMS";
    public static final String ERROR_7  = "ACUDIR C/PAGOS A ATENCION AL CONTRIBUYENTE";
    public static final String ERROR_8  = "LA CONSULTA NO SE CONCLUYO, INTENTE MAS TARDE";
    public static final String ERROR_62 = "NO SE PUEDE GENERAR EL TALON";
    public static final String ERROR_63 = "NO SE PUEDE ACTUALIZAR LA DECLARACION";
    public static final String ERROR_64 = "NO EXISTE DECLARACION CON EL RFC SOLICITADO";
    public static final String ERROR_65 = "FALTAN PARAMETROS (REPECO RFC BIMESTRE EJERCICIO)";
    public static final String ERROR_67 = "EXISTE PLACA EN PADRON VERIFICA";
    public static final String ERROR_68 = "EXISTE SERIE EN PADRON VERIFICA";
    public static final String ERROR_69 = "NO EXISTE PLACA EN PADRON VERIFICA";
    public static final String ERROR_70 = "NO EXISTE SERIE EN PADRON VERIFICA";
    public static final String ERROR_71 = "ACUDA AL CSF O MAC MÁS CERCANO";
}

