package com.addcel.edomex.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.edomex.model.vo.CorreoVO;
import com.addcel.edomex.model.vo.DetalleVO;
import com.addcel.edomex.model.vo.pagos.TransactionProcomVO;
import com.addcel.utils.Utilerias;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://127.0.0.1:8080/MailSenderAddcel/enviaCorreoAddcel";
//	private static final String urlString = "http://50.57.192.213:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	private static final String HTML_DOBY = 
			"<!DOCTYPE html> <html><head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">  <style type=\"text/css\">  body { background: none repeat scroll 0 0 " +
			"#FFFFFF; font-family: 'Open Sans', sans-serif;  } table.info_description{ height: auto; width: 620px; position: relative; } table.info_description td{ text-align: left; " +
			"display: inline-block; vertical-align: top; *display: inline; } section.main_info{ background: none repeat scroll 0 0 #CBCBCB; width: 100%; height: auto; margin: 0 auto; " +
			"background:#CBCBCB; }  table.donate{ width: 620px; }  table.donate p.text_donation{ color: #000000; font-size: 14px; font-style:italic; line-height: 20px; text-align: left; } " +
			"table.donate p.text_donation span.highlights{ font-weight: bold; }  table.block{ width: 400px; height: auto; color: #232323; } table.block td.title_rigth{ font-weight: normal;" +
			" text-transform:uppercase; font-size: 12px; line-height: 16px; text-align: right; width: 170px; } table.block td.address_center{ margin: 0 0 18px; font-weight: bold; " +
			"font-style:italic; font-size: 14px; line-height: 14px; text-align: center; } section.title_tamaulipas{ width: 100%; height: auto; position: relative; } " +
			"section.title_tamaulipas p.state_tamaulipas{ margin: 12px 0 18px 0; color: #FF000E; font-size: 14px; font-weight: bold; text-transform:uppercase; line-height: 16px; " +
			"text-align: center; } section.title_tamaulipas div.line_tamaulipas{ width: 220px; height: 1px; padding: 6px 0; position: absolute; top: 8px; left: 50%; margin-left:-300px; " +
			"border-top: 1px #9E9E9E solid; } section.title_tamaulipas div.line_tamaulipas_right{ width: 220px; height: 1px; padding: 6px 0; position: absolute; top: 8px; left: 50%; " +
			"margin-left: 80px; border-top: 1px #9E9E9E solid; } section.helpers{ background: none repeat scroll 0 0 #FFFFFF; width: 940px; height: auto; position: relative; padding: " +
			"26px 0 0; margin: 0 auto; vertical-align: center; color: #000000; font-weight: bold; text-transform:uppercase; font-size: 12px; line-height: 14px; text-align: center; } " +
			"section.helpers div.extra_text_helpers{ margin: 44px auto 20px;  } section.helpers div.footer_text_helpers{ margin: 0 0 0 0;  } </style> </head> " +
			"<body data-twttr-rendered=\"true\">  <table class=\"info_description\"  align=\"center\"> <tbody> <tr > <td colspan=\"2\"><img src=\"cid:identifierCID00\"></td> </tr> " +
			"<tr> <td><img src=\"cid:identifierCID01\"></td> <td><img src=\"cid:identifierCID02\"></td> </tr> </tbody> </table>  <section class=\"main_info\">  " +
			"<table class=\"donate\"  align=\"center\"> <tbody> <tr> <td >&nbsp </td> </tr> <tr> <td > <p class=\"text_donation\">  </p> " +
			"<p class=\"text_donation\"> <span class=\"highlights\" >Estimado Contribuyente.</span></p> " +
			"<p class=\"text_donation\">Usted ha realizado una transacción a través de la aplicación para teléfonos móviles del Gobierno del Estado de México.</p> " +
			"<p class=\"text_donation\">Acontinuacion encontrará el recibo de pago correspondiente.</p> " +
			"<p class=\"text_donation\">Gracias por pagar oportunamente sus contribuciones.</p>  </td> </tr> <tr> <td >&nbsp </td> </tr> </tbody> </table> " +
			"<section class=\"title_tamaulipas\" align= \"center\" > <div class=\"line_tamaulipas\"></div> <p class=\"state_tamaulipas\">Pago de Tenencia</p> " +
			"<div class=\"line_tamaulipas_right\"></div> </section>  <table class=\"block\"  align=\"center\" cellpadding=\"2\"> <tbody> " +
			"<tr> <td class=\"title_rigth\"> Placa: </td> <td class=\"address_center\"> <#PLACA#> </td> <td class=\"address_center\"> </td> </tr> " +
			"<tr> <td class=\"title_rigth\"> Linea captura: </td> <td class=\"address_center\"> <#LINEA#> </td> <td class=\"address_center\"> </td> </tr>" +
			"<tr> <td class=\"title_rigth\"> Monto: </td> <td class=\"address_center\"> <#MONTO#> </td> <td class=\"address_center\"> </td> </tr> " +
			"<tr> <td class=\"title_rigth\"> Moneda: </td> <td class=\"address_center\"> <#MONEDA#> </td> <td width=\"50\"> </td> </tr> " +
			"<tr> <td class=\"title_rigth\"> Fecha: </td> <td class=\"address_center\"> <#FECHA#> </td> <td > </td> </tr> " +
			"<tr> <td class=\"title_rigth\"> Autorizacion Bancaria: </td> <td class=\"address_center\"> <#AUTBAN#> </td> <td > </td> " +
			"</tr> <tr> <td class=\"title_rigth\"> Referencia: </td> <td class=\"address_center\"> <#REFE#> </td> <td > </td> </tr> " +
			"<tr> <td >&nbsp </td> <td >&nbsp </td> <td >&nbsp </td> </tr> </tbody> </table> </section>  <section class=\"helpers\"> " +
			"<div class=\"extra_text_helpers\"> Nota. Este correo es de caráter informativo, no es necesario que responda al mismo. </div> " +
			"<div class=\"footer_text_helpers\"> Gracias por usar: <img src=\"cid:identifierCID03\"> </div> </section>  </body> </html>";
			

	public static CorreoVO generatedMail(DetalleVO detalleVO){
		logger.info("Genera objeto para envio de mail: " + detalleVO.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("<#PRODUCTO#>", detalleVO.getProducto() != null ? detalleVO.getProducto() : "");
			body = body.replaceAll("<#PLACA#>", detalleVO.getPlaca());
			body = body.replaceAll("<#LINEA#>", detalleVO.getLinea());
			body = body.replaceAll("<#MONTO#>", Utilerias.formatoImporteMon(detalleVO.getTotal()));
			body = body.replaceAll("<#MONEDA#>", (detalleVO.getMoneda()!= null && "1".equalsIgnoreCase(detalleVO.getMoneda())?"MXN":"DLL"));
			body = body.replaceAll("<#FECHA#>", detalleVO.getFecha()!= null ? detalleVO.getFecha().substring(0, 19) : "");
			body = body.replaceAll("<#AUTBAN#>", detalleVO.getReferencia()!= null ? detalleVO.getReferencia() : "");
			body = body.replaceAll("<#REFE#>", detalleVO.getIdBitacora()!= null ? detalleVO.getIdBitacora() : "");
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago Gobierno Estado de Mexico - Referencia: " + detalleVO.getReferencia();
			String[] to = {detalleVO.getEmail()};
//			String[] to = {"jesus.lopez@addcel.com"};
			String[] cid = {
					"/usr/java/resources/images/EdoMex/EdoMex_logo_2.png",
					"/usr/java/resources/images/EdoMex/Gobierno_logo.png",
					"/usr/java/resources/images/EdoMex/logo-compromiso.png",
					"/usr/java/resources/images/EdoMex/MobileC_logo.png"
					};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
