package com.addcel.edomex.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.edomex.model.vo.pagos.ProcomVO;
import com.addcel.edomex.model.vo.pagos.TransactionProcomVO;
import com.addcel.edomex.model.vo.servicios.VentanillaResponse;
import com.addcel.edomex.services.EdoMexPagosService;
import com.addcel.edomex.services.EdoMexService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class EdoMexController {
	
	private static final Logger logger = LoggerFactory.getLogger(EdoMexController.class);
	@Autowired
	private EdoMexService dfService;
	@Autowired
	private EdoMexPagosService empService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}
	
	@RequestMapping(value = "/getToken")
	public @ResponseBody String getToken(@RequestParam("json") String jsonEnc) {
		logger.info("Dentro del servicio: /getToken");
		return dfService.generaToken(jsonEnc);
	
	}
	
	@RequestMapping(value = "/getLinea")
	public @ResponseBody String getLinea(@RequestParam("json") String jsonEnc) {
		logger.info("Dentro del servicio: getLinea");
		return dfService.getLinea(jsonEnc);	
	}
			
		
	@RequestMapping(value = "/pago-ventanilla")
	public ModelAndView datosPago(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-ventanilla");
		ModelAndView mav=new ModelAndView("error");
		ProcomVO procom = empService.procesaPago(jsonEnc);
		if(procom!=null){
			mav=new ModelAndView("comerciofin");
			mav.addObject("prosa", procom);
		}
		return mav;	
	}
	
	@RequestMapping(value = "/pagina-prosa")
	public String paginaTestProsa(Model model) {
		logger.info("Dentro del servicio: /pagina-prosa");
		//empService.comercioFin(user, referencia, monto, idTramite)
		//model.addAttribute("prosa", arg1);
		return "pagina-prosa";	
	}
	
	@RequestMapping(value = "/comercio-con")
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest) {
		logger.info("Dentro del servicio: /comercio-con");
		ModelAndView mav=new ModelAndView("error");
		
		TransactionProcomVO tp= new TransactionProcomVO();
		tp.setEmAuth(EM_Auth);
		tp.setEmMerchant(EM_Merchant);
		tp.setEmOrderID(EM_OrderID);
		tp.setEmRefNum(EM_RefNum);
		tp.setEmResponse(EM_Response);
		tp.setEmStore(EM_Store);
		tp.setEmTerm(EM_Term);
		tp.setEmTotal(EM_Total);		
			
		VentanillaResponse ventanillaResponse = empService.procesaRespuestaProsa(tp);
		if(ventanillaResponse != null && ventanillaResponse.getIdError() == 0){
			mav=new ModelAndView("comerciocon");
			mav.addObject("prosa", ventanillaResponse);
		}
		return mav;	
	}
	
	@RequestMapping(value = "/busquedaPagos") 
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /busquedaPagos");
		return dfService.busquedaPagos(data);
	}
	
	@RequestMapping(value = "/reenvioRec") 
	public @ResponseBody String reenvioRec(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /reenvioRec");
		return dfService.reenvioRecibo(data);
	}
}